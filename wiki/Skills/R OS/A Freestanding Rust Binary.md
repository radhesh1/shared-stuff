The first step in making of our own rust based OS kernel is to create a executable that doesn't link the standard library. This makes it possible to run the code bare metal without an OS.

## Introduction
To write an OS kernel , we need code that is independent of OS features. This means that we can’t use the fun and easy features <span style="color:#0070c0">(threads, files, heap memory, the network, random numbers, standard output, or any other features)</span> requiring OS abstractions or any specific hardware. which makes complete sense as we are trying to crabify everything.

This also means we can't use most of rust stamdard library, but there's a lot of Rust features that we can use. For example, we could use [iterators](https://doc.rust-lang.org/book/ch13-02-iterators.html), [closures](https://doc.rust-lang.org/book/ch13-01-closures.html), [pattern matching](https://doc.rust-lang.org/book/ch06-00-enums.html), [option](https://doc.rust-lang.org/core/option/) and [result](https://doc.rust-lang.org/core/result/), [string formatting](https://doc.rust-lang.org/core/macro.write.html), and Of Course! the [ownership system](https://doc.rust-lang.org/book/ch04-00-understanding-ownership.html). These features make it possible to write a kernel in a very expressive, high level way without worrying about [undefined behavior](https://www.nayuki.io/page/undefined-behavior-in-c-and-cplusplus-programs) or [memory safety](https://tonyarcieri.com/it-s-time-for-a-memory-safety-intervention)i.e. the C/C++ issues. 

In order to crabify our kernel we need to make it have "*bare-metal*" or "*freestanding*" executable.

Below explains this in a  fun way;

### Disabling the Standard Library
By default, all rust crates link the standard library, which is OS-dependent for fun and easy features. It also depends on the [[`libc` library| C standard library]] , which closely interacts with other OS services. Since we are making the whole OS, we can't include any 


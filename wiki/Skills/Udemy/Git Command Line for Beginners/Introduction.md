---
dg-publish: true
---
Git is a [[https://en.wikipedia.org/wiki/Distributed_version_control|distributed version control]]  system that tracks changes in [computer files](https://en.wikipedia.org/wiki/Computer_file "Computer file"), usually used for coordinating work among programmers who are collaboratively developing [source code](https://en.wikipedia.org/wiki/Source_code "Source code") during [software development](https://en.wikipedia.org/wiki/Software_development "Software development"). Its goals include speed, [data integrity](https://en.wikipedia.org/wiki/Data_integrity "Data integrity"), and support for [distributed](https://en.wikipedia.org/wiki/Distributed_computing "Distributed computing"), non-linear workflows (thousands of parallel [branches](https://en.wikipedia.org/wiki/Branching_(version_control) "Branching (version control)") running on different computers).
Git was originally mde by [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds "Linus Torvalds") in 2005 for development of the [Linux kernel](https://en.wikipedia.org/wiki/Linux_kernel "Linux kernel"), with other kernel developers contributing to its initial development. Since 2005, Junio Hamano has been the core maintainer. As with most other distributed version control systems, and unlike most [client–server](https://en.wikipedia.org/wiki/Client%E2%80%93server "Client–server") systems, every Git [directory](https://en.wikipedia.org/wiki/Directory_(computing) "Directory (computing)") on every computer is a full-fledged [repository](https://en.wikipedia.org/wiki/Repository_(version_control) "Repository (version control)") with complete history and full version-tracking abilities, independent of [network](https://en.wikipedia.org/wiki/Computer_network "Computer network") access or a central [server](https://en.wikipedia.org/wiki/Server_(computing) "Server (computing)"). Git is [free and open-source software](https://en.wikipedia.org/wiki/Free_and_open-source_software "Free and open-source software") shared under the [GPL-2.0-only license](https://en.wikipedia.org/wiki/GNU_General_Public_License "GNU General Public License").
Since its creation, Git has become the most popular distributed version control system, with nearly 95% of developers reporting it as their primary version control system as of 2022. There are many popular offerings of Git repository services, including [GitHub](https://en.wikipedia.org/wiki/GitHub "GitHub"), [SourceForge](https://en.wikipedia.org/wiki/SourceForge "SourceForge"), [Bitbucket](https://en.wikipedia.org/wiki/Bitbucket "Bitbucket") and [GitLab](https://en.wikipedia.org/wiki/GitLab "GitLab").
## Setting up git

[<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Apple_logo_black.svg/800px-Apple_logo_black.svg.png" style="width:50px" alt="Apple Logo">](http://git-scm.com/download/mac)

[<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Microsoft_logo.svg/512px-Microsoft_logo.svg.png?20210729021049" style="width: 50px" alt="windows logo">](https://gitforwindows.org/)

 [<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Tux.svg/265px-Tux.svg.png" style="width:50px" alt="linux logo">](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Initialization
### Creation of a new Repository
1. `mkdir <folder_name>`  Make the project directory
2. `cd <folder_name>`  Open the directory
3. `git init`  To initalize the git repository

### Adding files to the Repository
-  `git add <filename>`  Add a specific file
	   or
- `git add *`  Add all files except . files
	 or
- `git add .`  Adds all files in current directory

### Saving Changes to the Repository
- `git commit -m "<some_message>"` Save changes with message 
> Saves changes to local copy

### Remote Repository
#### Online Git Services
You can put your project online in a "remote repository" by doing this multiple people can use same project.
These are several free services available for this including [[https://github.com|Github]], [[https://gitlab.com|Gitlab]], and [[https://bitbucket.org|Bitbucket]]. You can also choose to set up your own Git Service using [[https://gogs.io|Gogs]] 

#### How to do it?
- `git remote add origin <server>`  To connect to a remote repository
- `git push origin master` To send to remote repository
#### Checkout online Repository
- `git clone username@host:/path/to/repository` To get a local copy
  > using ssh as inbuilt protocol
- `git clone /path/to/repository` To get local copy, if it's on a network drive

## Exploration
### To view Repository history
- `git log` Shows detailed history

### Undo in Repository
1. `git commit --amend`

### Rollbacking a Repository
1. `git reset --hard <prev_commit>`

### Branching
It lets you isolate repository into different copies which don't affect the entire project
The _master_ branch is the default branch. Git lets you switch between the branches and merge them

#### Commands
- `git checkout -b <branch_name>` To Create a branch
- `git checkout <brnach_name>` To switch branch
- `git branch -d <branch_name>` To delete branch
- `git push origin <branch_name>` To put your branch online 
- `git merge <branch_name>` To merge branch


### Syncing and Merging a Repository
- `git pull` To sync project to latest
- `git merge <branch_name>` To merge changes to master from  _<branch_name>_

### Tagging
- `git tag <name> <commit_id>` Naming Commits
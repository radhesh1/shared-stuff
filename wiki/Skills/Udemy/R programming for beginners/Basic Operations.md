## Calculator & Vectors
### Basic calculations
- 1+2 on console is 3
- 100-50.6 is 49.4
- etc...
### Mathematical Functions

1. sqrt()  - square root
2. exp()  - exponential
3. log()   - logarithm

## R objects
Most of the stuff we use in R are ==objects== 
These objects have their:
- Own set of characteristics<span style="color:#92d050">(size, dimensions, data types, etc)</span>
- Own set of functions
> Basically like C structures  

Examples:
- Vector
- Matrix
- List
- DataFrame
### Vectors
- Characterized by their size and element type.
- All element must have to be of the same type
- index-based and accessible using index

### **Making Vectors** 
- made using `c()` 
	E.g 
	```r
		apples <- c(3.4, 3.1, 3, 4.5)
		#or
		apples = c(3.4, 3.1, 3, 4,5)
    ```
- To access:
	`example[1]`

### Operations
```r
#scalar
apples/2 #divides every element by 2
apples*3 #multiplies every element by 3
#vector
adjust_weight <- c(0.4, 0.2, 0.4, 0.3)  #each element is individually increased
new_apples <- melons+adjust_weight
#functions
sqrt(apples) #finds square root
sum(apples) # finds sum
mean(apples) #finds average
#boolean
apples>4 #both produce a boolean vector
apples==3
#exceptions
sum(c(3.4, 3.1, 3, 4.5/0)) #Inf(Infinity)
sum(c(3.4, 3.1, 3, sqrt(-1))) #NaN(NotaNumber)
sum(c(3.4, 3.1, 3, NA)) #NA(NotAvailable)
```

### Excercise
1. **Use R to extract the square root of 85.**
> sqrt(85) 
2. **Compute the following equation in R:**
    ${15}/{10^2}$
> 15/10^2 
3. **Compute the following equation in R:**
    $(215+log(100))/e^{100} )$
> (215+log(100)/exp(100))
4. **Use R to compute the natural logarithm of 24**
> log(2)
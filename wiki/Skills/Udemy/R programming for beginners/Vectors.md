### Creation
#### syntax
```r
cities_population<-c(10000, 20000, 4000, 3000) #To create a vector
rm(cities_population) #To remove variable
char_cities_population<-c(10000,20000,4000,"3000") #Everything is converted as characters
```
They appear as arrays in global enviornment area
+ By Default, every element is converted to character
### Indexing and slicing
```r
cities_population<-c(10000, 20000, 4000, 3000)
melons <- c(1.2, 1.4, 2.4, 3.4)

#Indexing
melons[1] #Positiom
melons[3:4] #Slcing
melons[c(1,4)] #Vectors
```
### Operations
```r
 melons[1]+melons[2]+melons[3]+melons[4]
 #or
 sum(melons)

 melons*2 #Scalar multiplication of vector
 #or
 melons+melons

 melons/2

 sqrt(melons)
 exp(melons)
 log(melons)

 adjusted_weight <- c(1.2, 1.2, 1.4, 1.4)
 new_melons <- melons*adjusted_weight #vector multiplication 
 ```

### Functions and Unexpected Values
```r
mean(melons) #Finds mean
length(melons) #Finds length of vector
median(melons) #Finds median of vector
sd(melons) #Finds standard division of vector
sort(melons #, decreasing=TRUE) #Sorts the vector

bad_melons <- c(1.2, 1.4, 2.4, 3.4, NA)
sum(melons, na.rm+TRUE)
corrupt_melons <- c(1.2, 1.4, 2.4, 3.4, 1/0)
mean(melons, na.rm=TRUE)
```

+ `na.rm=TRUE` removes the NA value and finds the answer
### Comparison Operators
```r
melons > 2 #Logical vector
melons[melons > 2] #vector based on comparison
which(melons == 1.4) #equality
```

+ `which()` : finds the index
### Naming
```r
countries <-  c(10276617, 67545757, 67020000)
names(countries) <- c('Portugal', 'United Kingdom', 'France')
countries['Portugal']

#countries['Portugal','France'] Dimensions Issue
countries[c('Portugal','France')]
which(countries > 20000000)
names(which(countries > 20000000))
```

+ Be careful of the return type of our object
### Modifications
```r
melons[1] <- 2
melons[2:4] <- c(3,4,5)
melons[5] <- 10 #Increases object length
melons <- c(1.2, 1.4, 2.4, 3.4, 1.2)
melons[melons<2]<- 2

#Deletion
new_melons <- melons[-c(2,4)]
```

### Comparision with SQL and Excel
| *Point* | Excel | SQL | R |  |
| ---- | ---- | ---- | ---- | ---- |
| Style | Uses GUI based interaction | Uses a table to do anything | Uses Obects for manipulation of data |  |
| freedom in use | minimal | medium | high |  |
|  |  |  |  |  |

### Excercise
1. **Create a vector called** `kids_ages` **with the following elements:**  
    
    `12, 11, 12, 13, 14, 13, 12, 10, 12, 12, 14, 13`
	  >kids_ages <-  c(12, 11, 12, 13, 14, 13, 12, 10, 12, 12, 14, 13)
	  
    1. **Calculate the mean of the kids_ages vector.**
	    > mean(kids_ages)
	      
    2. **Calculate the median of the kids_ages vector.**
        >median(kids_age)
        
    3. 🔥**Create a new logical (TRUE/FALSE) vector called** `subset_ages` **that stores with TRUE the kids that are less than 11 years old or more than 13 years old.**
	    >subset_ages <- kid_ages  < 11 | kids_ages > 13
2. **Give the following names to the** `kids_ages` **vector:** 
    
    `"John", "Rachel", "Joe", "Anne", "Theresa", "Samuel", "Marcus", "Andrew", "Kate", "Jane", "Martha", "David"`
    > 	names(kids_ages) <- c("John", "Rachel", "Joe", "Anne", "Theresa", "Samuel", "Marcus", "Andrew", "Kate", "Jane", "Martha", "David")
1. **Subset the ages of** `"Rachel"` **and** `"Anne"`
	>ages[c(2,4)]
1. **Change the age of the fifth student to** `13`
    >kid_ages[5] <- 13
5. **Return a vector named** `under_12` **with the names of the students under 12 years old.**
    >under_12 <- names(which(kids_ages<12))
6. **Create a new vector called** `product_pricing` **with the following elements:** 
    `5.6, 11.2, 4.5, 0.25, 1, 23`
    >product_pricing <- c(5.6, 11.2, 4.5, 0.25, 1, 23)
7. **Create a new vector based on** `product_pricing` **called** `half_price` **with half the original price.**
    > half_price <- product_pricing/2
8. 🔥 **Create a new vector based on** `product_pricing` **called** `promotion` **with promotions applied to the first product(20%), third product(40%) and fifth product(50%)**
    >promotion <- product_pricing - (1-c(0.2,0,0.4,0,0.5)) 
9. 🔥**Create a new vector based on** `product_pricing` **called** `squared_prices`  **with the prices squared.**
    >square_prices <- product_pricing^2
10. **Subset the first three products of the** `product_pricing` **vector.**
    >product_pricing[1:3]
11. **Select the maximum price on the** `product_pricing` **vector and store it in the** `maximum_price` **object.**
    >maximum_price <- max(product_pricing) 
12. **Return the number of products in your vector and store it in a object called** `num_products`**.**
    >num_products <- length(product_pricing)
13. **Add 5 to the product's value and divide them by the original** `product_price`**. Store the result in an object called** `transform_price`**.**
	>transform_price <- (product_pricing + 5)/product_pricing
## R Language
It is a language and environment for statistical computing and graphics. It Provides a wide variety of statistical  <span title="linear and non-linear modelling, classical statistical tests, time-series analysis, classification, clustering" style="border-bottom: 1px dashed #999; cursor: help;">statistical</span> and graphing techniques, and is is highly extensible.
### Stuff
[Resources](https://drive.google.com/drive/folders/1KC5NxvCoRjYumlHoFehjicvQYbA4E01M) and [Answers](https://github.com/ivopbernardo/r-programming-absolute-beginners)
## Chapters
1. [[Installing R and R studio]]
2. [[Basic Operations]]
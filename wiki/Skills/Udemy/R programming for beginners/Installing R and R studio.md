## Installing R
1. Get the instructions from [R-Project](https://www.r-project.org/)
	<span style="color:#ff0000;">Since I am using Ubuntu 23.04 I follow</span>  [this](https://cloud.r-project.org/bin/linux/ubuntu/)
		or
	Run these lines
	```sh
	sudo apt update -qq #update indices
	sudo apt install --no-install-recommends software-properties-common dirmngr #install two helper packages we need
	#add the signing key(Michael Rutter's) for these repos
		#To verify, run gpg --show-keys /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc 
		#Fingerprint: E298A3A825C0D65DFD57CBB651716619E084DAB9
	wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | sudo tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
	sudo add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"
	sudo apt install --no-install-recommends r-base #installing r package
	```
### To get CRAN packages
```sh
sudo add-apt-repository ppa:c2d4u.team/c2d4u4.0+ #c2d4u - cran to deb for ubuntu for 4.0+
#example packages:
sudo apt install --no-install-recommends r-cran-rstan
sudo apt install --no-install-recommends r-cran-tidyverse
```

## Installing RStudio IDE
1. Get the files from [Downloads Page](https://posit.co/download/rstudio-desktop/)
   <span style="color:#ff0000;">Since I am using Ubuntu 23.04 I use</span>
```sh
sudo apt install ./`cd Downloads && ls|grep "rstudio"`
```

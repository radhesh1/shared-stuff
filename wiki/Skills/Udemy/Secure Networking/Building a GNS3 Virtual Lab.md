## Installing Setup
1. [[https://www.virtualbox.org|Oracle VirtualBox]]
2. [[https://www.gns3.com/|GNS3]]

### Create custom VM in GNS, in case a Link does not work...

+ The links used in this course might sometimes get expired or be removed by the people managing those links.

	In case a download Link for a virtual machine does not work, there are two options for you.
	1. The first option is a [[http://docs.gns3.com/docs/getting-started/installation/linux|Docker]], for example, an Ubuntu Docker, if you require a lightweight machine.
	2. The other option is creating a [[https://www.gns3.com/community/support/how-to-add-qemu-machines-in-gns3|QEMU VM]]. 
	> It is a custom virtual machine you can create on your GNS3. That way, you can have ANY virtual machine with ANY version you want on your GNS3.

## GNS3 VM & Server, templates for Linux nodes, pfSense, Cumulus & VBox Integration
1. Install GNS3 if you haven't already.
   
2. Go to `View > Docks > Server Summary` to be on.
 
3. To edit Server Preferences `Edit > Preferences > Server` .
	* Local server should be on
	* The firewall doesn't block the connection to the server

4. To edit GNS3 VM Preferences `Edit > Preferences > GNS3 VM` .
   Choose a relevant Virtualization Engine:
	1. VMware Workstation : [[https://github.com/GNS3/gns3-gui/releases/download/v2.2.44.1/GNS3.VM.VMware.Workstation.2.2.44.1.zip|Install image]]
	2. VirtualBox : [[https://github.com/GNS3/gns3-gui/releases/download/v2.2.44.1/GNS3.VM.VirtualBox.2.2.44.1.zip|Install image]]
	3. Remote : refer to [[https://docs.gns3.com/docs/getting-started/installation/remote-server|this]]
	+ Hyper-V <span style="color:#ff0000">(recommended for windows)</span> : [[https://github.com/GNS3/gns3-gui/releases/download/v2.2.44.1/GNS3.VM.Hyper-V.2.2.44.1.zip|Install Image]]
5. Go to <img src=https://i.imgur.com/EqY0QyN.png alt="GNS3 browse all devices" align="center" height="50%"> button  then, add a new template choose any one that you like.
   > Eg.
   > 1. [[https://d2cd9e7ca6hntp.cloudfront.net/public/CumulusLinux-5.4.0/cumulus-linux-5.4.0-vx-amd64-qemu.qcow2|cumulusVX from switches]]
   > 	<span style="color:#0070c0">user/password:</span> cumulus/cumulus
   > 2. Alpine Linux from Guests
   > 3. [[https://www.osboxes.org/opensuse/#opensuse-42-3-leap-vmware|OpenSUSE 42.3 Leap]]
   > 4. [[https://atxfiles.netgate.com/mirror/downloads/pfSense-CE-2.7.2-RELEASE-amd64.iso.gz?_gl=1*tth7cg*_ga*MTA0MzI2Mjk1MS4xNzAzMzc2MTky*_ga_TM99KBGXCB*MTcwMzM3NjE5MS4xLjAuMTcwMzM3NjIwNy40NC4wLjA|pfSense 2.7.2]]
   > 5. Ubuntu from Guests
   > 6. Kali Linux from Guests

6. Make sure all the files are downloaded and everything is loading correctly

Layer 2
### Intro to VLAN
It is a network segmentation technique that allows network administrators to logically divide a physical network into multiple isolated broadcast domains. This segmentation is achieved virtually, without the need for physically separate network infrastructure. VLANs provide several benefits, including improved network performance, enhanced security, and simplified network management.

Each VLAN operates as if it is a separate and independent network, even though devices in the same VLAN may physically share the same network infrastructure. VLANs are commonly used to group devices logically based on factors such as department, function, or security requirements.

#### **Port-based VLAN:**
Port-based VLANs/port VLANs/static VLANs, involve assigning specific switch ports to a particular VLAN. Devices connected to those ports become members of the assigned VLAN. Port-based VLANs are straightforward and easy to configure.

Key points about port-based VLANs:
- **Configuration:** VLAN membership is manually assigned to specific switch ports by the network administrator.
- **Isolation:** Devices in different VLANs cannot communicate directly with each other, even if they are connected to the same physical switch.
- **Flexibility:** This approach is commonly used in environments where devices need to be grouped based on physical location or departmental boundaries.
- **Simplicity:** Port-based VLANs are relatively easy to set up and manage.
#### **Protocol-Based VLAN:**
Protocol-based VLANs/dynamic VLANs/VLAN tagging, involve classifying traffic into VLANs based on the type of network protocol or protocol type. This method is often associated with IEEE 802.1Q, which is a standard for VLAN tagging in Ethernet frames.

Key points about protocol-based VLANs:
- **Dynamic Assignment:** VLAN membership is dynamically assigned based on the protocol information found in the Ethernet frame.
- **IEEE 802.1Q:** This standard adds a VLAN tag to the Ethernet frame, indicating the VLAN to which the frame belongs. Switches use these tags to forward frames to the correct VLANs.
- **Flexibility:** Protocol-based VLANs are more dynamic and allow for more flexible VLAN assignments based on the type of traffic.
- **Interoperability:** IEEE 802.1Q is widely supported in network equipment, enabling interoperability between devices from different vendors.
### Access Mode vs Trunk Mode
**Access Mode:**
In access mode:

- The switch port is typically connected to an end device, such as a computer or printer.
- The port is assigned to a specific VLAN, and frames entering or leaving the port are untagged with VLAN information.
- Frames sent from the device attached to an access port are assumed to belong to the VLAN assigned to that port.

**Trunk Mode:**

In trunk mode:

- The switch port is configured to carry traffic for multiple VLANs.
- Frames are tagged with VLAN information as they traverse the trunk link.
- Trunk links are often used to interconnect switches, allowing the passage of traffic for multiple VLANs between them.

| **Type**                | Access Mode                                                                                                                          | Trunk Mode                                                                                                                                                              |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **number of VLANs**     | Connects to end devices and is associated with a single VLAN<br>Frames are untagged.                                                 | Connected other switches and is configured to carry traffic for multiple VLANs.<br>Frames are tagged.                                                                   |
| **VLAN <br>Assignment** | Each Access Port is assigned to a specific VLAN. <br>Frames Entering and leaving the port are assumed to belong to the VLAN.         | Carries traffic for multiple VLANs.<br>VLAN information is included in the frame tags.                                                                                  |
| **Tagging**             | Frames are untagged.<br>The End device connected to the access port is unware of the VLAN information                                | Frames are tagged with VLAN information.<br>This allows switched at each end of the trunk link to understand understand and properly handle traffic for multiple VLANs. |
| **Configuration**       | Simple to Configure.<br>The port is assigned to a specific VLAN using commands like `switchport mode access` and `switchport access vlan <VLAN_ID>`<br> | Requires Additional Configuration.<br>The port needs to be explicitly set to trunk mode using commands like `switchport mode trunk`, and additional settings such as allowed VLANs may need to be specified                                                                                                                                                                         |
| **Typical Use Cases**   | Used for Connecting end devices                                                                                                                                     | Used for interconnecting switches, routers, or other network devices where traffic for multiple VLANs needs to traverse the link.                                                                                                                                                                        |
### Spanning Tree Protocol(STP)
It is a network protocol that ensures a loop-free topology for Ethernet networks. Its primary purpose is to prevent and eliminate loops in a network, which can cause broadcast storms and lead to network instability. It operates by identifying redundant paths in a network and blocking those paths, leaving only a single active path for data transmission.
#### **Spanning Tree Algorithm (STA):**
It is the underlying algorithm used by STP to determine the active and blocking paths in a network. The algorithm is designed to select the most efficient and loop-free path, ensuring network stability.
#### **STP Port States:**
STP operates by transitioning ports through different states to establish a loop-free topology. The main port states are:
##### **Blocking:**
   - In this state, the port does not forward data frames but listens to BPDU (Bridge Protocol Data Unit) messages from other switches. The purpose is to prevent loops while still allowing the switch to gather information about the network.
##### **Listening:**
   - The port transitions to the listening state after the blocking state. During this state, the port prepares to forward data by populating its MAC address table and still listens to BPDU messages.
##### **Learning:**
   - In the learning state, the port continues to listen to BPDU messages and starts populating its MAC address table. However, it still does not forward data frames.
##### **Forwarding:**
   - This is the active state where the port forwards data frames. The switch has determined that it can safely forward traffic on this port without causing a loop.
##### **Disabled:**
   - A port in the disabled state is effectively shut down. It does not participate in the spanning tree process and does not forward any frames.
### [ARP(Address Resolution Protocol)]([[Network Protocols and Services#[ARP(Address Resolution Protocol)](https //en.wikipedia.org/wiki/Address_Resolution_Protocol)]])
### LAG, Port-Channel, MLAG, vPC, LACP
#### **LAG ( Link Aggregator):**
   - It is a logical entity that groups multiple physical links into a single logical link. The goal is to increase bandwidth, provide fault tolerance, and enhance network efficiency. Aggregation is often achieved using protocols like LACP (Link Aggregation Control Protocol).
#### **Port-Channel:**
   - Port-Channel refers to a technology that enables the grouping of multiple physical network interfaces into a single logical interface. It's a general term that is often used interchangeably with link aggregation or EtherChannel. The technology is commonly employed in switches to combine multiple links between switches or between a switch and a server to form a high-bandwidth, resilient link.
#### **MLAG (Multi-Chassis Link Aggregation):**
   - MLAG is a high-availability and load-balancing technique that allows multiple switches to be connected in a loop-free manner while presenting a single logical link to connected devices. MLAG enables the use of link aggregation across multiple switches, enhancing network redundancy and avoiding the creation of a single point of failure.
#### **vPC (Virtual Port Channel):**
   - vPC is a specific implementation of MLAG technology in Cisco's Nexus switches. It stands for Virtual Port Channel and allows a device (typically a server or another switch) to create a port channel across two separate Nexus switches. This provides redundancy, load balancing, and improved network resiliency.
#### **LACP (Link Aggregation Control Protocol):**
   - LACP is a protocol used for negotiating and managing link aggregation groups. It operates at the data link layer (Layer 2) and allows network devices to automatically detect and configure the aggregation of multiple links. LACP helps ensure that all links in an aggregation group are properly configured and operational, promoting efficient load balancing and fault tolerance.


IP subnetting is the process of dividing an IP network into sub-networks to improve performance and security. Here's a concise overview of the IP subnetting process:

1. **Understand IP Addresses:**
   - IP addresses are composed of two parts: the network portion and the host portion.
   - IP addresses are written in dotted-decimal format (e.g., 192.168.1.1).

2. **Choose a Subnetting Mask:**
   - The subnetting mask defines the boundary between the network and host portions of an IP address.
   - Common subnetting masks include 255.255.255.0 (CIDR /24) or 255.255.0.0 (CIDR /16).

3. **Determine the Number of Subnets and Hosts:**
   - Decide how many subnets and hosts per subnet are needed.
   - Use this information to choose an appropriate subnet mask.

4. **Convert Subnet Mask to Binary:**
   - Convert the chosen subnet mask from dotted-decimal to binary form.
   - This helps visualize the network and host portions.

5. **Identify Subnet Ranges:**
   - Divide the available IP address range into subnets according to the chosen subnet mask.
   - Determine the range of IP addresses for each subnet.

6. **Assign Subnet Addresses:**
   - Assign each subnet a unique subnet address from the identified ranges.
   - These addresses serve as the network identifiers.

7. **Allocate Host Addresses:**
   - For each subnet, allocate individual addresses to hosts.
   - Ensure that the number of hosts accommodates the required devices in each subnet.

8. **Record Subnet Information:**
   - Document the subnet addresses, subnet masks, and ranges for future reference.
   - This information is crucial for network administration.

9. **Implement Subnetting on Network Devices:**
   - Configure routers and switches with the appropriate subnetting information.
   - Ensure that devices within each subnet are aware of their subnet masks.

10. **Verify Subnet Connectivity:**
    - Test connectivity within and between subnets.
    - Confirm that routing is properly configured to allow communication between subnets.

### IP Block
An IP block, or IP address block, refers to a range of consecutive IP addresses within the same network. It is typically specified by a starting IP address and an ending IP address, defining a range of usable IP addresses. IP blocks are commonly used for various purposes, such as network administration, security, and routing. They are often represented in CIDR (Classless Inter-Domain Routing) notation, which specifies both the network address and the number of significant bits used for the host portion of the address.
### How many subnets are needed?
The number of subnets needed depends on the specific requirements of the network and its design. To determine the number of subnets required, you should consider factors such as the size of the network, the number of devices or subnetworks needed, and any future scalability requirements.
> Number of Subnets=$2^n$ 
	where n is the number of additional bits borrowed for subnetting.
### How many host bits can be borrowed?
> Number of Hosts=$2^n−2$
	where n is the number of bits borrowed for hosts.
		where $n=Total Length - Prefix Length$
### Calculate decimal value and prefix value of the new subnet mask.
 The subnet mask is represented as a series of binary 1s followed by binary 0s. The number of consecutive 1s in the binary representation determines the prefix length.

For example, let's say you have a subnet mask with 6 consecutive 1s followed by 2 consecutive 0s:

	Subnet mask: 11111100

To calculate the decimal value, convert each octet (8 bits) from binary to decimal:

```
11111100 -> 252
```

So, the decimal value of the subnet mask is 252. The prefix length is the number of consecutive 1s, which in this case is 6. Therefore, the subnet mask can be represented as /6.

In summary:
- **Decimal value**: 252
- **Prefix length**: /6
### Use the subnet mask to calculate the network address, broadcast address, and the range of usable IP addresses in each subnet,You can start using IP addresses. Assign first or last IP in each subnet to a firewall/router interface, facing that subnet, Document your IPs, ranges and subnets.
To calculate the network address, broadcast address, and the range of usable IP addresses in a subnet, you need to know the subnet mask and the IP address range assigned to that subnet. Let's go through the steps:

#### Example:
Let's say you have the following information:
- **Subnet Mask**: 255.255.255.192 (/26 in CIDR notation)
- **IP Range**: 192.168.1.0 to 192.168.1.63
##### 1. Calculate Subnet Details:
- **Subnet Mask**: 255.255.255.192 (or /26 in CIDR notation)
- **Number of Host Bits**: 32 - 26 = 6
- **Number of Subnets**: 2^(6) = 64
##### 2. Calculate Subnet Size:
- Each subnet has 2^(6) = 64 IP addresses.
##### 3. Identify Subnet Range:
- **Subnet 1**: 192.168.1.0 to 192.168.1.63
- **Subnet 2**: 192.168.1.64 to 192.168.1.127
- ...
##### 4. Calculate Network and Broadcast Addresses for Subnet 1:
- **Network Address**: 192.168.1.0
- **Broadcast Address**: 192.168.1.63
##### 5. Calculate Usable IP Range for Subnet 1:
- **Usable IP Range**: 192.168.1.1 to 192.168.1.62 (excluding network and broadcast addresses)

Repeat the steps for subsequent subnets.
#### Summary:
- **Subnet Mask**: 255.255.255.192 (/26)
- **Subnet Size**: 64 IP addresses
- **Subnet 1**:
  - **Network Address**: 192.168.1.0
  - **Broadcast Address**: 192.168.1.63
  - **Usable IP Rang**e**: 192.168.1.1 to 192.168.1.62




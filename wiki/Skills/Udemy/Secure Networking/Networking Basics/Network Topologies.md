## What is topology?
[Network topology](https://wikipedia.org/wiki/Network_Topology) is the arrangement of elements <span style="color:#92d050">(nodes,links etc.)</span> of a communication network or the way we connect network elements.

## Types

### [Bus](https://service.snom.com/display/wiki/Bus+topology)
```mermaid
graph TD

  A1[Node 1] -->|Data| B1[Central Bus]
  A2[Node 2] -->|Data| B2[Central Bus]
  A3[Node 3] -->|Data| B3[Central Bus]
  B0[Central Bus] --> B1 --> B2 --> B3

  style B0 B1 B2 B3 fill:#f9f,stroke:#333,stroke-width:2px;
  style A1 A2 A3 fill:#fff,stroke:#333,stroke-width:2px;
```
In this all the nodes are connected to a central backbone or trunk.
+ Typically, these are connected by DT connector to the ethernet.
+ At the end of the trunk, there is a terminator which helps to avoid signal reflection.
### [Ring](https://service.snom.com/display/wiki/Ring+topology)
```mermaid
graph TB
  A[Node 1] -->|Data| B[Node 2]
  B -->|Data| C[Node 3]
  C -->|Data| A[Node 1]
```
In this Topology, all the nodes are connected in a style of a ring.
+ a hop/switch is not needed as each node is connected to its neighbouring nodes.
+ Expansion is very hard as the data from $node_{1}$ to $node_{n}$ goes through n-1 nodes first before reaching $node_{n}$.
### [Star](https://service.snom.com/display/wiki/Star+topology) 
```mermaid
graph TB
	A1[Node 1] --> C[Node 4]
	A2[Node 2] --> C[Node 4]
	A3[Node 3] --> C[Node 4]
```
In this Topology, all the nodes are connected to a central switch which transfer data. It is also called hub and spoke topology.
+ Load on the centre switch will increase with increase in number of connected nodes.
+ It has a single point of failure.
### [Mesh](https://service.snom.com/display/wiki/Mesh+topology)
```mermaid
graph TB
	A1 --> A2
	A1 --> A3
	A1 --> A4
	A2 --> A1
	A2 --> A3
	A2 --> A4
	A3 --> A1
	A3 --> A2
	A3 --> A4
	A4 --> A1
	A4 --> A2
	A4 --> A3
```
This is also called *Fully connected topology*, In this all nodes are connected to each other.
+ Cable required is $n(n-1)$.
+ Each node can send data to any other node.
### [Hybrid (Tree)](https://service.snom.com/display/wiki/Hybrid+topologies)
#### 1. Star-Bus(Tree)
```mermaid
graph TD

	A1 --> A
	A2 --> A
	A3 --> A 
	A4 --> A
	B1 --> B
	B2 --> B
	B3 --> B
	B4 --> B
	C1 --> C
	C2 --> C
	C3 --> C 
	C4 --> C
	D1 --> D
	D2 --> D
	D3 --> D
	D4 --> D
	A -->|Data| S0[Central Bus]
	B -->|Data| S1[Central Bus]
    C -->|Data| S2[Central Bus]
    D -->|Data| S3[Central Bus]
    S0 --> S1 --> S2 --> S3
	
```
In this all the Star Topologies are connected by a single Trunk
+ Single Point of failure
+ Popular Topology
#### 2. Star-Ring
```mermaid
graph TD

  A1 --> A
  A2 --> A
  A3 --> A 
  A4 --> A
  B1 --> B
  B2 --> B
  B3 --> B
  B4 --> B
  C1 --> C
  C2 --> C
  C3 --> C 
  C4 --> C
  D1 --> D
  D2 --> D
  D3 --> D
  D4 --> D

  A --> B --> C --> D --> A

  style A padding:20px;
  style B padding:20px;
  style C padding:20px;
  style D padding:20px;

```

In this Star Topologies are connected by a Ring by using each central hub of each star
+ If one hub fails, the entire network fails.
+ this allows higher traffic through segments.
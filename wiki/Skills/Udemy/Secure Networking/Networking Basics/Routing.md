Layer 3 material
### How does routing work? (Binary ANDing)
RRouting involves the process of directing data packets from their source to their destination across a network. One method used in routing is binary ANDing, particularly in the context of IP (Internet Protocol) addressing and subnetting. Let's break down the process step by step:

1. **Subnetting:**
   - Subnetting involves dividing a larger network into smaller, more manageable sub-networks (subnets). This helps in efficient utilization of IP addresses and allows for better organization and management.

2. **Subnet Mask:**
   - A subnet mask is a 32-bit number used to divide an IP address into network and host portions. It consists of consecutive 1s followed by consecutive 0s. For example, in the subnet mask 255.255.255.0 (or in binary, 11111111.11111111.11111111.00000000), the first 24 bits represent the network, and the last 8 bits represent hosts within that network.

3. **Binary ANDing:**
   - Binary ANDing is a bitwise operation that involves comparing each bit of two binary numbers. The result is obtained by performing AND operations on corresponding bits.

   - In the context of routing, the router performs binary ANDing between the destination IP address of a packet and its subnet mask. This operation helps identify the network portion of the destination address.

   - For example, if the destination IP address is 192.168.1.15 and the subnet mask is 255.255.255.0 (or in binary, 11111111.11111111.11111111.00000000), the router would perform the following binary AND operation:

     ```binary
     11000000.10101000.00000001.00001111   (192.168.1.15 - Destination IP)
     AND
     11111111.11111111.11111111.00000000   (255.255.255.0 - Subnet Mask)
     ------------------------------------
     11000000.10101000.00000001.00000000   (192.168.1.0 - Network Address)
     ```

5. **Routing Decision:**
   - The result of the binary AND operation gives the network address. The router uses this information to make routing decisions. It looks up its routing table to determine the next hop or outgoing interface for the packet.

### Default Route vs Static Route
|  | Default Route | Static Route |
| ---- | ---- | ---- |
| 1. | It is often represented as 0.0.0.0/0, is a route which matches all possible IPs. It is a catch-all route used when specific destination is not found. | It is manually configured in a routing table normally by a network administrator. |
| 2. | Commonly used as a gateway of last resort. When a device needs to send a packet to a destination for which it doesn't have a specified route, It forwards to the default route. | These specify the path to reach a specific network/destination. <br>Configuration of next-hop router or exit interface for the specified destination is needed. |
| E.g. | Router might be configured with a default route to its upstream ISP for traffic that doesn't match a specific route.  |  Used when the network topology is relatively simple and dynamic routing protocols are not needed. |
### Dynamic Routing Protocols, e.g., RIP, OSPF, BGP etc.
These are used in computer networks to automate the process of updating and exchanging routing information among routers. These protocols enable routers to dynamically learn about network topology changes and make informed decisions on the best paths for routing data:
1. **RIP (Routing Information Protocol):**
   - one of the oldest dynamic routing protocols. It uses a distance-vector algorithm to determine the best path to a destination based on the number of hops. RIP routers periodically broadcast their routing tables to neighboring routers.
   - limitations in terms of scalability and convergence speed and is often used in small to medium-sized networks.

2. **OSPF (Open Shortest Path First):**
   - **a link-state routing protocol that uses the Shortest Path First (SPF) algorithm** to calculate the best path to a destination. OSPF routers exchange information about the state of their links and build a topology database.
   - more scalable than RIP and is commonly used in larger enterprise networks. It supports variable-length subnet masks and provides faster convergence.

3. **BGP (Border Gateway Protocol):**
   - **a path-vector protocol** primarily used in the Internet to exchange routing and reachability information between autonomous systems (ASes). BGP is different from interior gateway protocols like RIP and OSPF because it focuses on inter-domain routing.
   - makes routing decisions based on policies, path attributes, and network policies. It is crucial for ensuring the stability and scalability of the global Internet.

4. **EIGRP (Enhanced Interior Gateway Routing Protocol):**
   - a Cisco proprietary routing protocol. It **combines characteristics of both distance-vector and link-state routing protocols**. EIGRP routers exchange information about their routes and can dynamically adapt to changes in the network.
   - suitable for both small and large networks, offering features like rapid convergence, efficient bandwidth usage, and support for multiple network protocols.

5. **IS-IS (Intermediate System to Intermediate System):**
   - another **link-state routing protocol**, similar to OSPF, but it is commonly used in larger service provider networks. It operates by exchanging information about routers and links, allowing routers to build a complete topological map of the network.
   - supports IPv4 and IPv6 and is often used in telecom and Internet Service Provider (ISP) environments.
### Routing Metrics
Routing metrics are criteria or parameters used by routers to determine the best path to a destination when making routing decisions. Different routing protocols and algorithms use various metrics to evaluate the quality of paths:

#### **Hop Counts:**
   - Hop count is a simple metric that counts the number of routers or network devices a packet must traverse to reach its destination. In protocols like RIP (Routing Information Protocol), routers prefer paths with fewer hops.
#### **MTU (Maximum Transmission Unit):**
   - MTU represents the maximum size of a packet that can be transmitted over a network. Routing decisions based on MTU aim to avoid sending packets that exceed the maximum size of a link. This is important to prevent fragmentation and potential performance issues.
#### **Bandwidth:**
   - Bandwidth is the capacity of a network link to transmit data. Some routing protocols, like OSPF (Open Shortest Path First), consider bandwidth as a metric for path selection. Higher bandwidth paths are often preferred.
#### **Costs:**
   - Cost is a generalized metric that may include various factors such as bandwidth, delay, and reliability. Routing protocols like EIGRP (Enhanced Interior Gateway Routing Protocol) use a composite metric that considers multiple factors when calculating the cost of a path.
#### **Administrative Distance:**
   - Administrative distance is a measure of the trustworthiness of a routing information source. It is used when multiple routing protocols provide information about the same route. The router considers the source with the lowest administrative distance as more reliable. For example, a directly connected route has a lower administrative distance than a route learned from an exterior routing protocol.
#### **Latency:**
   - Latency is the time delay introduced as data travels from the source to the destination. It is an essential metric in real-time applications. Routers may consider the latency of a path when making routing decisions, and protocols like BGP (Border Gateway Protocol) can take latency into account.

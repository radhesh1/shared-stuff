
## By scale
https://en.wikipedia.org/wiki/File:Data_Networks_classification_by_spatial_scope.
<img src=https://upload.wikimedia.org/wikipedia/commons/6/6e/Data_Networks_classification_by_spatial_scope.svg alt="Network Types by Spatial Scope" height="150px" > 
### 0. [Nanoscale](https://en.wikipedia.org/wiki/Nanonetwork)
A set of interconnected <span title="devices a few hundred nanometers or a few micrometers at most in size" style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;">nanomachines</span> which are able to perform to only very <span title="computing, data storing, sensing and actuation" style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;">simple tasks</span>.
- 📍typically ranging from 1 to 100 nanometers.

### 1. [Body Area Networks](https://en.wikipedia.org/wiki/Body_area_network)
A set of devices connected on a body <span style="color:#92d050">(mostly human) </span>. These Systems can be communicating with proximity to or placed on a body.
+ wearable objects which may have medical or other purposes like <span title="wireless variant of a body area network which uses any kind of mechanism {which doesn't require wires} to communicate." style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;">WBAN</span> /<span title="A body area network which are used to sense various parameters of a body" style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;">BSN</span> /<span title="Body area network machines used for medical purpose (temperature monitors, sugar regulator etc)" style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;">MBAN</span>
   - 📍 typically ranging from on body to within a few centimeters.
   
### 2. [PAN(Personal Area Networks)](https://en.wikipedia.org/wiki/Personal_area_network)
A set of interconnected devices within a individuals person's workspace
- 📍typically ranging from a room to a floor.
#### 2.1 Variants of PAN:
##### 2.1.1 WPAN (Wireless Personal Area Network)
A Personal Area Network over <span title="such as IrDA, Wireless USB, Bluetooth or Zigbee" style="border-bottom: 1px dashed #999; cursor: help;color:#92d050;">a low-powered, short-distance wireless network</span> .
- 📍typically ranging from a few centimeters to a few meters.
###### 2.1.1.1 Variants of WPAN:
1. <span title="WPAN for low-power operation of the sensors" style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;">LPPAN</span>

### 3.  [LAN (Local Area Networks)](https://en.wikipedia.org/wiki/Local_area_network)
A wired data network restricted to a limited geographic location such as a building.
- 📍typically a building wide network.
#### 3.1 Variants of LAN:
##### 3.1.1 [WLAN  (Wireless Local Area Network)](https://en.wikipedia.org/wiki/Wireless_LAN)
A secure and flexible communication mechanism, usually complementing or replacing [[#1. [LAN (Local Area Networks)](https //en.wikipedia.org/wiki/Local_area_network)|LAN]]. 
Data is being sent and received using Radio Frequency(RF Signals).
##### 3.1.2 [SAN (Storage Area Network)](https://en.wikipedia.org/wiki/Storage_area_network)
A network used to communicate between array of disks and storage devices which offers block level data storage which connected as a locally attached device. 

### 4. [WAN(Wide Area Network)](https://en.wikipedia.org/wiki/Wide_area_network)
A network communication happening over a wide area such as a country,world-wide etc. WAN requires expensive harware <span style="color:#92d050">(Routers, leased lines etc)</span>.
- 📍typically ranging from State to World-Wide.
#### 4.1 Variant of WAN
##### 4.1.1 [SD-WAN(Software-Defined Wide Area Network)](https://en.wikipedia.org/wiki/SD-WAN)
They are virtual [[#4. [WAN(Wide Area Network)](https //en.wikipedia.org/wiki/Wide_area_network|WAN]] architecture used to manage WAN. It allows enterprises to use the best path to reach an application while having QoS and security as well.

These are designed to address these <span title="network congestion, packet delay variation, packet loss and service outages" style="border-bottom: 1px dashed #999; cursor: help;color:#92d050;">network problems</span>. By enhancing or even replacing traditional branch routers with virtualization appliances that can control application-level policies and offer a network overlay, less expensive consumer-grade Internet links which act like a dedicated circuit. This simplifies the setup processes for branches.

Its products can be physical appliances or software based only.
###### Components
The [MEF Forum](https://en.wikipedia.org/wiki/MEF_Forum "MEF Forum") has defined an SD-WAN architecture consisting of an SD-WAN edge, SD-WAN gateway, SD-WAN controller and SD-WAN orchestrator.
1. **SD-WAN edge**: It is where the network endpoints reside or It is a physical/virtual network function placed at an <span title="organization's branch/regional/central office site, data center, and in public or private cloud platforms" style="border-bottom: 1px dashed #999; cursor: help;color:#92d050;">organization's parts and others</span>. It also classifies the incoming IP packets at SD-WAN UNI(user network interface)  and determines which application flow does the packet belongs to as instructed by the SD-WAN controller.
2. **SD-WAN gateway**: It provides access to the service in order to reduce the distance cloud-based services/user and service interruptions. 
3. **SD-WAN orchestrator**: It is a cloud hosted/on-premises web management tool that allows you configuration, provisioning and other functions to an SD-WAN. It simplifies the application traffic management by allowing implementation of organization's policies. It also handels dynamic routing and integration with external systems.
4. **SD-WAN controller**: It can be placed on gateway/controller, is used to make forwarding decisions for <span title="These are IP packets which have been classified to determine their user application or grouping of application to which they are related" style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;">Application flows</span>. This helps ensure that application performance meets [SLAs](https://en.wikipedia.org/wiki/Service-level_agreement "Service-level agreement"). It also sets up and configures SD-WAN edges and their path selection.
#### 4.2 Protocols
##### 4.2.1 [MPLS(Multiprotocol Label Switching)](https://en.wikipedia.org/wiki/Multiprotocol_Label_Switching)
A very high-performance WAN technology used by providers. MPLS uses short path labels to direct data between nodes. The endpoints are identified by network address and the the labels help to identify established path between endpoint. It can also encapsulate multiple protocols.
+ Uses Technologies like,[T1](https://en.wikipedia.org/wiki/Digital_Signal_1)/[E1](https://en.wikipedia.org/wiki/E-carrier "E-carrier"),  [ATM](https://en.wikipedia.org/wiki/Asynchronous_Transfer_Mode "Asynchronous Transfer Mode"),  [[#4.2.4 [Frame Relay](https //en.wikipedia.org/wiki/Frame_Relay "Frame Relay")|Frame Relay]], and [DSL](https://en.wikipedia.org/wiki/Digital_subscriber_line "Digital subscriber line").
+ It is secure and private but it is costly.
##### 4.2.2 [Packet over SONET/SDH (POS)](https://en.wikipedia.org/wiki/Packet_over_SONET/SDH "Packet over SONET/SDH")
This used for transmitting packets in the form of [Point to Point Protocol (PPP)](https://en.wikipedia.org/wiki/Point_to_Point_Protocol "Point to Point Protocol") over [SDH](https://en.wikipedia.org/wiki/Synchronous_optical_networking "Synchronous optical networking") or [SONET](https://en.wikipedia.org/wiki/SONET "SONET"), which are both standard protocols for communicating digital information using lasers or [light emitting diodes](https://en.wikipedia.org/wiki/Light_emitting_diodes "Light emitting diodes") (LEDs) over optical fibre at high line rates as SONET/SDH use Point-to-Point Circuits so, PPP is well suited. It is defined by RFC 2615 as PPP over SONET/SDH.

Scrambling is performed during insertion of the PPP packets into the SONET/SDH frame to solve <span title="denial-of-service attacks and the imitation of SONET/SDH alarms" style="border-bottom: 1px dashed #999; cursor: help;color:#92d050;">security attacks</span>. Scrambling modification is cost-effective as it is already used  to transport [ATM](https://en.wikipedia.org/wiki/Asynchronous_Transfer_Mode "Asynchronous Transfer Mode") cells over SONET/SDH. Scrambling can be disabled to allow node to be compatible with older RFC 1619 which lacks the scrambler.
##### 4.2.3 [ATM(Asynchronous Transfer Mode)](https://en.wikipedia.org/wiki/Asynchronous_Transfer_Mode "Asynchronous Transfer Mode")
In ATM,frames are of fixed length (53 octets) called cells.  It uses a [connection-oriented model](https://en.wikipedia.org/wiki/Connection-oriented_model "Connection-oriented model") in which a [virtual circuit](https://en.wikipedia.org/wiki/Virtual_circuit "Virtual circuit") must be established between two endpoints before the data exchange begins. These can either be permanent<span title="dedicated connections that are preconfigured by the service provider" style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;">Permanent</span> or <span title="set up on on a per-call basis  using signalling and disconnected when the call is terminated" style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;">Switched</span>. It is used by [SONET/SDH](https://en.wikipedia.org/wiki/SONET/SDH "SONET/SDH") backbone of the public switched telephone network and in [ISDN](https://en.wikipedia.org/wiki/Integrated_Services_Digital_Network "Integrated Services Digital Network"). 
##### 4.2.4 [Frame Relay](https://en.wikipedia.org/wiki/Frame_Relay "Frame Relay")
It puts data in  <span title="variable-size units" style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;">frames</span> and leaves any necessary <span title="retransmission of data" style="border-bottom: 1px dashed #999; cursor: help;color:#92d050;">error-connections</span> up to the end-points which speeds up the overall data transmission speeds. For most services, the network provides a [PVC](https://en.wikipedia.org/wiki/Permanent_virtual_circuit "Permanent virtual circuit"), which means that the customer sees a devoted connection without having to pay for a full-time [leased line](https://en.wikipedia.org/wiki/Leased_line "Leased line"), while the [service-provider](https://en.wikipedia.org/wiki/Service_provider "Service provider") figures out the route for the frames and can charge on usage basis.

These can run fractional [T-1](https://en.wikipedia.org/wiki/Digital_Signal_1 "Digital Signal 1") or full [T-carrier](https://en.wikipedia.org/wiki/T-carrier "T-carrier") system carriers (outside the Americas, [E1](https://en.wikipedia.org/wiki/E-carrier#E1 "E-carrier") or full [E-carrier](https://en.wikipedia.org/wiki/E-carrier "E-carrier")). It complements and provides a mid-range service between basic rate [ISDN](https://en.wikipedia.org/wiki/ISDN "ISDN"), which has a bandwidth 128 Kb/s, and [ATM](https://en.wikipedia.org/wiki/Asynchronous_Transfer_Mode "Asynchronous Transfer Mod")  which operates in somewhat similar fashion to Frame Relay but at speeds from 155.520 Mb/s to 622.080 Mb/s.
###### Protocol data unit

1. **Flag Field**. The flag is used to perform high-level data link synchronization which indicates the beginning and end of the frame with the unique pattern, its uniqueness within a frame is insured by [bit stuffing and destuffing](https://en.wikipedia.org/wiki/Bit_stuffing "Bit stuffing") procedures are used.
2. **Address Field**. Each address field may occupy either octet 2 to 3, octet 2 to 4, or octet 2 to 5, depending on the range of the address in use. A two-octet address field comprises the EA=ADDRESS FIELD EXTENSION BITS and the C/R=COMMAND/RESPONSE BIT.
    <img src=https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/En-t%C3%AAte_Relais_de_Trame.png/440px-En-t%C3%AAte_Relais_de_Trame.png alt="two-octet address field" height="120%" style="margin-left: 50px;margin-top: 20px" > 
    1. **DLCI**-Data Link Connection Identifier Bits. The [DLCI](https://en.wikipedia.org/wiki/DLCI "DLCI") serves to identify the virtual connection so that the receiving end knows which information connection a frame belongs to. A single physical channel can [multiplex](https://en.wikipedia.org/wiki/Multiplexing "Multiplexing") several different virtual connections.
    2. **FECN, BECN, DE** bits. These bits report congestion:
        - **FECN**=Forward Explicit Congestion Notification bit
        - **BECN**=Backward Explicit Congestion Notification bit
        - **DE**=Discard Eligibility bit
    
3. **Information Field**. A system parameter defines the maximum number of data bytes that a host can pack into a frame. Hosts may negotiate the actual maximum frame length at call set-up time. The standard specifies the maximum information field size as at least 262 octets. For end-to-end protocols, Frame Relay recommends that the network support the maximum value of at least 1600 octets in order to avoid the need for segmentation and reassembling by end-users.
4. **Frame Check Sequence (FCS) Field** : each switching node needs to implement error detection to avoid wasting bandwidth due to the transmission of _err_ed frames. The error detection mechanism used in Frame Relay uses the [CRC](https://en.wikipedia.org/wiki/Cyclic_redundancy_check "Cyclic redundancy check") as its basis.

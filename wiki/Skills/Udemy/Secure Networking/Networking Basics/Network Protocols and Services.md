## Connection-Oriented Protocol vs Connectionless Protocol

```sheet
{
    classes: { 
        class1: { 
            "color": "cyan",
            "align": "center",
            "padding-top": "78px",
            "padding-bottom": "78px",
        },
        class2: {
            "align": "center",
            "padding-top": "85px",
            
        }
    }
}
---
|   Comparision parameter   |   Connection-Oriented Protocol   |   Conectionless Protocol   |
|   ---------------------   |   ----------------------------   |   ----------------------   |
|   Related System   |   Designed and developed based on the telephone system   |    Service based on the postal system    |
|   Definition   |    Used to create an end-to-end connection between the senders to the reciever before transmitting the data over same/different network   |   Used to transfer the data packets between senders to the reciever without creating any connection   |
|   Virtual Path   | Creates a virtual path between the sender and reciever   |   Does not create any virtual connection or path between the sender and the receiver   |
|   Authentication   |    Requires authentication before transmitting the data packets to the reciever   |   Does not requires authentication before transmitting the data packets to the reciever   |
|   Data Packets path   |   All data packets are recieved in the same order as those sent by the sender   |Not all data packets are recieved in same order as the those sent bythe reciever    |
|   Bandwidth Requirement   |   Requires a higher bandwidth to transfer the packets   |   Requires low bandwidth to transfer the packets   |
|   Data Reliability   |   More reliable connection service as it guarantees data packets transfer from  one end to the other end with a connection   |   Not a reliable connection service as it does not guarantee transfer of data packets from one end to another for establishing a connection   |
|   Congestion    |   No congestion as it provides an end-to-end connection between the send and reciever during transmission of data   |   May be congestion due to not providing an end-to-end connection between the source and reciever to transmit the data packets   |
|   Examples   |   TCP,MPLS,ATM   |   UDP,IP,ICMP   |
```
 

## IP Protocols
### [TCP(Transmission Control Protocol)](https://en.wikipedia.org/wiki/Transmission_Control_Protocol)
It provides a communication service for application program and IP. It provides host-to-host connectivity at the transport layer. An Application does not need to know the detailed mechanism for sending data via a link to another host. It needs IP fragmentation to accommodate the maximum transmission unit of the transmission medium.

At transport layer, it handles all the handshaking and transmission details and shows abstraction of the connection to the application by a socket interface. It also detects problems at lower level, requests re-transmission of lost data, rearranges out-of-order data and even minimizes the congestion. It focuses on accuracy instead of timely delivery.

### [UDP(User Datagram Protocol)](https://en.wikipedia.org/wiki/User_Datagram_Protocol)
It uses a simple connectionless model and provides [checksums](https://en.wikipedia.org/wiki/Checksum "Checksum") for data integrity, and [port numbers](https://en.wikipedia.org/wiki/Port_numbers "Port numbers") for addressing different functions at the source and destination of the datagram. It has no handshaking talks and exposes unreliability of the network; It doesn't guarantee of delivery, ordering,or duplication protection. It provides no error-correction facilities.

<img src=https://i.imgur.com/IWSlYUy.png alt ="UDP packet" >
A number of UDP's attributes make it especially suited for certain applications.
- It is _transaction-oriented_, suitable for <span title="domain name system or the network time protocol" style="border-bottom: 1px dashed #999; cursor: help;color:#92d050;">simple query-response protocols</span>.
- It provides _[datagrams](https://en.wikipedia.org/wiki/Datagram)_, suitable for modeling other protocols such as [IP tunneling](https://en.wikipedia.org/wiki/IP_tunneling "IP tunneling") or [remote procedure call](https://en.wikipedia.org/wiki/Remote_procedure_call "Remote procedure call") and the [Network File System](https://en.wikipedia.org/wiki/Network_File_System "Network File System").
- It is _simple_, suitable for [bootstrapping](https://en.wikipedia.org/wiki/Bootstrapping "Bootstrapping") or other purposes without a full [protocol stack](https://en.wikipedia.org/wiki/Protocol_stack "Protocol stack"), such as the [DHCP](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol "Dynamic Host Configuration Protocol") and [Trivial File Transfer Protocol](https://en.wikipedia.org/wiki/Trivial_File_Transfer_Protocol "Trivial File Transfer Protocol").
- It is _stateless_ and the _lack of re-transmission delays_ makes it suitable for  <span title="VoIP,online games and many protocols using real time streaming protocol" style="border-bottom: 1px dashed #999; cursor: help;color:#92d050;">real-time applications</span>.
- Because it supports [multicast](https://en.wikipedia.org/wiki/Multicast "Multicast"), it is suitable for broadcast information such as in many kinds of [service discovery](https://en.wikipedia.org/wiki/Service_discovery "Service discovery") and shared information such as [Precision Time Protocol](https://en.wikipedia.org/wiki/Precision_Time_Protocol "Precision Time Protocol") and [Routing Information Protocol](https://en.wikipedia.org/wiki/Routing_Information_Protocol "Routing Information Protocol")
### [ICMP(Internet Control Message Protocol)](https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol)
It is a protocol designed to report error messages and operational information indicating success/failure when communicating with another address. 

the **traceroute** command can be implemented  by transmitting IP datagram  with specially set IP TTL headers field ,and looking for ICMP time exceeded in transit and  Destination unreachable messages generated in response. the **ping** utility is implemented using the ICMP _echo request_ and _echo reply_.
It lacks a TCP/UDP port number as its packets are associated with the transport layer.
#### Datagram structure
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/ICMP_header_-_General-en.svg/350px-ICMP_header_-_General-en.svg.png" alt =" Genral header for ICMPv4" style="background-color: #ffffff;">
The ICMP packet is encapsulated in an IPv4 packet. The packet consists of header and data sections.
##### Header
The ICMP header starts after the [IPv4 header](https://en.wikipedia.org/wiki/IPv4#Header "IPv4") and is identified by [IP protocol number](https://en.wikipedia.org/wiki/List_of_IP_protocol_numbers "List of IP protocol numbers") . All ICMP packets have an 8-byte header and variable-sized data section. The first 4 bytes of the header have fixed format, while the last 4 bytes depend on the type/code of that ICMP packet.

```sheet
{
	"classes":{
	  "class1":{
	    "align": "center",
	    "padding-left": "250px"
	 },
	  "class2":{
	    "align": "center",
	    "padding-left": "750px"
	 }
	}
}
---
| Offsets  | Octet | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 |
|---------|-------|---|---|---|---|---|---|---|---|---|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
| 0       | 0     | Type ~.class1 | < | < | < | < | < | < | < | Code ~.class1 | < | < | < | < | < | < | < | Checksum ~.class1 |  < |  < |  < |  < |  < |  < |  < | < |  < |  < |  < |  < | < |  < |  < | < |
| 4       | 32    | Rest of the Header ~.class2 | < | < | < | < | < | < | < | < | < | < | < | < | < | < |  < | < |  < |  < |  < |  < |  < |  < |  < | < | < | < | < |
```
##### Type
ICMP type, see [§ Control messages](https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol#Control_messages).
##### Code
ICMP subtype, see [§ Control messages](https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol#Control_messages).
##### Checksum
[Internet checksum](https://en.wikipedia.org/wiki/Internet_checksum "Internet checksum") (RFC 1071) for error checking, calculated from the ICMP header and data with value 0 substituted for this field.
##### Rest of header
Four-byte field, contents vary based on the ICMP type and code.
#### Data
ICMP error messages contain a data section that includes a copy of the entire IPv4 header, plus at least the first eight bytes of data from the IPv4 packet that caused the error message. The length of ICMP error messages should not exceed 576 bytes. This data is used by the host to match the message to the appropriate process. If a higher level protocol uses port numbers, they are assumed to be in the first eight bytes of the original datagram's data.

The variable size of the ICMP packet data section has been [exploited](https://en.wikipedia.org/wiki/Exploit_(computer_security) "Exploit (computer security)"). In the "[Ping of death](https://en.wikipedia.org/wiki/Ping_of_death "Ping of death")", large or fragmented ICMP packets are used for [denial-of-service attacks](https://en.wikipedia.org/wiki/Denial-of-service_attacks "Denial-of-service attacks"). ICMP data can also be used to create [covert channels](https://en.wikipedia.org/wiki/Covert_channels "Covert channels") for communication called [ICMP tunnels](https://en.wikipedia.org/wiki/ICMP_tunnel "ICMP tunnel").
### [IPSec(**Internet Protocol Security**)](https://en.wikipedia.org/wiki/IPsec)
It is security protocol suite which authenticates and encrypts packets of data.
It is a part of IPv4 suite and uses following protocols to perform various functions:
#### **AH(Authenticator Header)** 
It gives connectionless data  integrity by using a [hash function](https://en.wikipedia.org/wiki/Hash_function "Hash function") ,and secret key in the Algorithm ,and data origin authentication for IP datagrams  by authenticating packets ,and provides protection against [replay attacks](https://en.wikipedia.org/wiki/Replay_attack "Replay attack") using [sliding window](https://en.wikipedia.org/wiki/Sliding_window "Sliding window") technique and discarding old packets.
It operates on IP, using [IP protocol number 51](https://en.wikipedia.org/wiki/List_of_IP_protocol_numbers "List of IP protocol numbers")
```sheet
{
	"classes": {
		"class1": {
			"align": "center"
		}
	}
}
---
| IPv4 | IPv6 |
| ---- | ---- |
| Prevents option-insertion attacks | Protects against both header insertion and option-insertion attacks |
| Protects the payload and all header fields of the datagram except for mutable fields(DSCP/ToS, ECN, Flags, Fragment Offset, TTL and header Checksum), and also IP Security Options(RFC 1108) | Protects most of the basic header, non-mutable extensions  headers after the AH, and the payload. Protection excludes the mutable fields: DSCP, ECN, Flow Label, and Hop Limit|
```
##### AH packet 
<img src=https://i.imgur.com/CnbreT1.png alt="AH packet" >
###### _Next Header_ (8 bits)
Type of the next header, indicating what upper-layer protocol was protected. The value is taken from the [list of IP protocol numbers](https://en.wikipedia.org/wiki/List_of_IP_protocol_numbers "List of IP protocol numbers").
###### _Payload Len_ (8 bits)
The length of this _Authentication Header_ in 4-octet units, minus 2. For example, an AH value of 4 equals 3×(32-bit fixed-length AH fields) + 3×(32-bit ICV fields) − 2 and thus an AH value of 4 means 24 octets. Although the size is measured in 4-octet units, the length of this header needs to be a multiple of 8 octets if carried in an IPv6 packet. This restriction does not apply to an _Authentication Header_ carried in an IPv4 packet.
###### _Reserved_ (16 bits)

Reserved for future use (all zeroes until then).
###### _Security Parameters Index_ (32 bits)
Arbitrary value which is used (together with the destination IP address) to identify the [security association](https://en.wikipedia.org/wiki/Security_association "Security association") of the receiving party.
###### _Sequence Number_ (32 bits)
A [monotonic](https://en.wikipedia.org/wiki/Monotonic "Monotonic") strictly increasing sequence number (incremented by 1 for every packet sent) to prevent [replay attacks](https://en.wikipedia.org/wiki/Replay_attack "Replay attack"). When replay detection is enabled, sequence numbers are never reused, because a new security association must be renegotiated before an attempt to increment the sequence number beyond its maximum value.
###### _Integrity Check Value_ (multiple of 32 bits)
Variable length check value. It may contain padding to align the field to an 8-octet boundary for [IPv6](https://en.wikipedia.org/wiki/IPv6 "IPv6"), or a 4-octet boundary for [IPv4](https://en.wikipedia.org/wiki/IPv4 "IPv4").

#### Encapsulating Security Payload
Encapsulating Security Payload (ESP) is a member of the IPsec protocol suite. It provides origin [authenticity](https://en.wikipedia.org/wiki/Information_security#Authenticity "Information security") through source [authentication](https://en.wikipedia.org/wiki/Authentication "Authentication"), [data integrity](https://en.wikipedia.org/wiki/Data_integrity "Data integrity") through hash functions and [confidentiality](https://en.wikipedia.org/wiki/Confidentiality "Confidentiality") through [encryption](https://en.wikipedia.org/wiki/Encryption "Encryption") protection for IP [packets](https://en.wikipedia.org/wiki/Packet_(information_technology) "Packet (information technology)"). ESP also supports [encryption](https://en.wikipedia.org/wiki/Encryption "Encryption")-only and [authentication](https://en.wikipedia.org/wiki/Authentication "Authentication")-only configurations, but using encryption without authentication is strongly discouraged because it is insecure.ESP in transport mode does not provide integrity and authentication for the entire [IP packet](https://en.wikipedia.org/wiki/IP_packet_(disambiguation) "IP packet (disambiguation)"). However, in [tunnel mode](https://en.wikipedia.org/wiki/Tunneling_protocol "Tunneling protocol"), where the entire original IP packet is [encapsulated](https://en.wikipedia.org/wiki/Information_hiding "Information hiding") with a new packet header added, ESP protection is afforded to the whole inner IP packet (including the inner header) while the outer header (including any outer IPv4 options or IPv6 extension headers) remains unprotected.
 
ESP operates directly on top of IP, using IP protocol number 50.
##### ESP Packet
<img src=https://i.imgur.com/g9ZabNJ.png alt="ESP packet" align="center" >
###### _Security Parameters Index_ (32 bits)
Arbitrary value used (together with the destination IP address) to identify the [security association](https://en.wikipedia.org/wiki/Security_association "Security association") of the receiving party.
###### _Sequence Number_ (32 bits)
A [monotonically](https://en.wikipedia.org/wiki/Monotonic "Monotonic") increasing sequence number (incremented by 1 for every packet sent) to protect against [replay attacks](https://en.wikipedia.org/wiki/Replay_attack "Replay attack"). There is a separate counter kept for every security association.
###### _Payload data_ (variable)
The protected contents of the original IP packet, including any data used to protect the contents (e.g. an Initialization Vector for the cryptographic algorithm). The type of content that was protected is indicated by the _Next Header_ field.
###### _Padding_ (0-255 octets)
Padding for encryption, to extend the payload data to a size that fits the encryption's [cipher](https://en.wikipedia.org/wiki/Block_cipher "Block cipher") [block size](https://en.wikipedia.org/wiki/Block_size_(cryptography) "Block size (cryptography)"), and to align the next field.
###### _Pad Length_ (8 bits)
Size of the padding (in octets).
###### _Next Header_ (8 bits)
Type of the next header. The value is taken from the [list of IP protocol numbers](https://en.wikipedia.org/wiki/List_of_IP_protocol_numbers "List of IP protocol numbers").
###### _Integrity Check Value_ (multiple of 32 bits)
Variable length check value. It may contain padding to align the field to an 8-octet boundary for [IPv6](https://en.wikipedia.org/wiki/IPv6 "IPv6"), or a 4-octet boundary for [IPv4](https://en.wikipedia.org/wiki/IPv4 "IPv4").
#### Security Association
The IPsec protocols use a [security association](https://en.wikipedia.org/wiki/Security_association "Security association"), where the communicating parties establish shared security attributes such as [algorithms](https://en.wikipedia.org/wiki/Algorithms "Algorithms") and keys. As such, IPsec provides a range of options once it has been determined whether AH or ESP is used. Before exchanging data, the two hosts agree on which [symmetric encryption algorithm](https://en.wikipedia.org/wiki/Symmetric-key_algorithm "Symmetric-key algorithm") is used to encrypt the IP packet, for example [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard "Advanced Encryption Standard") or [ChaCha20](https://en.wikipedia.org/wiki/ChaCha20 "ChaCha20"), and which <span title="BLAKE2 or SHA256" style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;">hash function</span> is used to ensure the integrity of the data. These parameters are agreed for the particular session, for which a lifetime must be agreed and a [session key](https://en.wikipedia.org/wiki/Session_key "Session key").

The algorithm for authentication is also agreed before the data transfer takes place and IPsec supports a range of methods. Authentication is possible through [pre-shared key](https://en.wikipedia.org/wiki/Pre-shared_key "Pre-shared key"), where a [symmetric key](https://en.wikipedia.org/wiki/Symmetric_key "Symmetric key") is already in the possession of both hosts, and the hosts send each other hashes of the shared key to prove that they are in possession of the same key. IPsec also https://en.wikipedia.org/wiki/Security_Parameter_Indexsupports [public key encryption](https://en.wikipedia.org/wiki/Public_key_encryption "Public key encryption"), where each host has a public and a private key, they exchange their public keys and each host sends the other a [nonce](https://en.wikipedia.org/wiki/Cryptographic_nonce "Cryptographic nonce") encrypted with the other host's public key. Alternatively if both hosts hold a [public key certificate](https://en.wikipedia.org/wiki/Public_key_certificate "Public key certificate") from a [certificate authority](https://en.wikipedia.org/wiki/Certificate_authority "Certificate authority"), this can be used for IPsec authentication. In order to decide what protection is to be provided for an outgoing packet, IPsec uses the [Security Parameter Index](https://en.wikipedia.org/wiki/Security_Parameter_Index "Security Parameter Index") (SPI), an index to the security association database (SADB), along with the destination address in a packet header, which together uniquely identifies a security association for that packet. A similar procedure is performed for an incoming packet, where IPsec gathers decryption and verification keys from the security association database.

For [IP multicast](https://en.wikipedia.org/wiki/IP_multicast "IP multicast") a security association is provided for the group, and is duplicated across all authorized receivers of the group. There may be more than one security association for a group, using different SPIs, thereby allowing multiple levels and sets of security within a group. Indeed, each sender can have multiple security associations, allowing authentication, since a receiver can only know that someone knowing the keys sent the data. Note that the relevant standard does not describe how the association is chosen and duplicated across the group; it is assumed that a responsible party will have made the choice.

### [ARP(Address Resolution Protocol)](https://en.wikipedia.org/wiki/Address_Resolution_Protocol)
It used for discovering the  <span title="MAC address" style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;">link layer address</span>  associated with a given<span title="typically IPv4 address" style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;"> internet layer address</span>.It has been implemented with <span title="IPv4, Chaosnet, DECnet and Xerox PUP(PARC Universal Packet) using IEEE 802, X.25, Frame Relay and ATM(Asynchronous Transfer Mode)" style="border-bottom: 1px dashed #999; cursor: help;color:#0070c0;">many combinations of network and data link layer technologies</span>.In IPv6 networks, the functionality of ARP is provided by the [NDP(Neighbor Discovery Protocol)](https://en.wikipedia.org/wiki/Neighbor_Discovery_Protocol "Neighbor Discovery Protocol").
#### ARP Packet
<img src=https://i.imgur.com/QN6otXP.png alt="ARP packet" style="width:100%">
##### Hardware type (HTYPE)
This field specifies the network link protocol type. <span style="color:#ff0000">(Ethernet is 1)</span>
##### Protocol type (PTYPE)
This field specifies the internetwork protocol for which the ARP request is intended. For IPv4, this has the value 0x0800. The permitted PTYPE values share a numbering space with those for [EtherType](https://en.wikipedia.org/wiki/EtherType "EtherType").[
##### Hardware length (HLEN)
Length (in [octets](https://en.wikipedia.org/wiki/Octet_(computing) "Octet (computing)")) of a hardware address. <span style="color:#ff0000">Ethernet address length is 6</span>
##### Protocol length (PLEN)
Length (in octets) of internetwork addresses. The internetwork protocol is specified in PTYPE. <span style="color:#ff0000">IPv4 address length is 4.</span>
##### Operation
Specifies the operation that the sender is performing: 1 for request, 2 for reply.
##### Sender hardware address (SHA)
Media address of the sender. In an ARP request this field is used to indicate the address of the host sending the request. In an ARP reply this field is used to indicate the address of the host that the request was looking for.
##### Sender protocol address (SPA)
Internetwork address of the sender.
##### Target hardware address (THA)
Media address of the intended receiver. In an ARP request this field is ignored. In an ARP reply this field is used to indicate the address of the host that originated the ARP request.
##### Target protocol address (TPA)
Internetwork address of the intended receiver.

The [EtherType](https://en.wikipedia.org/wiki/EtherType "EtherType") for ARP is 0x0806. This appears in the Ethernet frame header when the payload is an ARP packet and is not to be confused with PTYPE, which appears within this encapsulated ARP packet.

### [DNS(Domain Name System)](https://en.wikipedia.org/wiki/Domain_Name_System)
The **Domain Name System** (**DNS**) is a hierarchical and distributed naming system. It associates various information with _[domain names](https://en.wikipedia.org/wiki/Domain_name "Domain name")_ (identification [strings](https://en.wikipedia.org/wiki/String_(computer_science) "String (computer science)")) assigned to each of the associated entities. It translates readily memorized domain names to the numerical [IP addresses](https://en.wikipedia.org/wiki/IP_address "IP address") needed for locating and identifying computer services and devices with the underlying [network protocols](https://en.wikipedia.org/wiki/Network_protocol "Network protocol").

The Domain Name System delegates the responsibility of assigning domain names and mapping those names to Internet resources by designating [authoritative name servers](https://en.wikipedia.org/wiki/Authoritative_name_server "Authoritative name server") for each domain. Network administrators may delegate authority over [sub-domains](https://en.wikipedia.org/wiki/Sub-domain "Sub-domain") of their allocated name space to other name servers. This mechanism provides distributed and [fault-tolerant](https://en.wikipedia.org/wiki/Fault-tolerant "Fault-tolerant") service and was designed to avoid a single large central database.

The Internet maintains two principal [namespaces](https://en.wikipedia.org/wiki/Namespace "Namespace"), the domain name hierarchy and the IP [address spaces](https://en.wikipedia.org/wiki/Address_space "Address space").It maintains the domain name hierarchy and provides translation services between it and the address spaces.the  [name servers](https://en.wikipedia.org/wiki/Name_server "Name server") and a [communication protocol](https://en.wikipedia.org/wiki/Communication_protocol "Communication protocol") implement the Domain Name System. A DNS name server is a server that stores the DNS records for a domain; a DNS name server responds with answers to queries against its database.
<iframe src="https://en.wikipedia.org/wiki/List_of_DNS_record_types" style="width: 1600px; height: 500px"></iframe>
#### DNS message format
It has two types of messages:
+ Queries
+ Responses
>Each message has a header and four sections(question,answer,authority,additional space)  

```sheet
{
	"classes":{
		"class1":{
		}
	}
}
---
|Field|Description|Length ([bits](https://en.wikipedia.org/wiki/Bit "Bit"))|
|---|---|---|
|QR|Indicates if the message is a query (0) or a reply (1)|1|
|OPCODE|The type can be QUERY (standard query, 0), IQUERY (inverse query, 1), or STATUS (server status request, 2)|4|
|AA|Authoritative Answer, in a response, indicates if the DNS server is authoritative for the queried hostname|1|
|TC|TrunCation, indicates that this message was truncated due to excessive length|1|
|RD|Recursion Desired, indicates if the client means a recursive query|1|
|RA|Recursion Available, in a response, indicates if the replying DNS server supports recursion|1|
|Z|Zero, reserved for future use|3|
|RCODE|Response code, can be NOERROR (0), FORMERR (1, Format error), SERVFAIL (2), NXDOMAIN (3, Nonexistent domain), etc.|4|
```

#### Resource Records
Each records has a type(name and number), an expiry time, a class and type specific data
```sheet
{
	"classes":{
		"class1":{
		}
	}
}
---
|Field|Description|Length ([octets](https://en.wikipedia.org/wiki/Octet_(computing) "Octet (computing)"))|
|---|---|---|
|NAME|Name of the node to which this record pertains|Variable|
|TYPE|Type of RR in numeric form (e.g., 15 for MX RRs)|2|
|CLASS|Class code|2|
|[TTL](https://en.wikipedia.org/wiki/Time_to_live "Time to live")|Count of seconds that the RR stays valid (The maximum is 231−1, which is about 68 years)|4|
|RDLENGTH|Length of RDATA field (specified in octets)|2|
|RDATA|Additional RR-specific data|Variable, as per RDLENGTH|
```

### [DHCP(Dynamic Host Communication Protocol)](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol)
**Dynamic Host Configuration Protocol** (**DHCP**) is a network management protocol used on IP networks to automatically assign IP addresses and communication parameters. It operates in a client-server architecture, eliminating manual configuration. DHCP consists of a centrally installed server and client instances on devices. Clients request parameters from the server when connecting to the network. DHCP is used in networks of various sizes, from residential to large campus and ISP networks. Routers and residential gateways often have DHCP server capability, assigning unique IP addresses within the network. DHCP supports both IPv4 and IPv6, with the latter referred to as DHCPv6.
#### Overview
[Internet Protocol](https://en.wikipedia.org/wiki/Internet_Protocol "Internet Protocol") (IP) facilitates communication within and across networks. DHCP (Dynamic Host Configuration Protocol) operates in a client-server model, automatically assigning IP addresses and configuration parameters like default gateways and DNS servers. DHCP clients send broadcast queries, and servers respond with specific information. DHCP can use dynamic allocation with lease periods, automatic allocation with permanent assignments, or manual allocation with reserved addresses. Large networks may employ DHCP relay agents.

DHCP services are utilized for both [IPv4](https://en.wikipedia.org/wiki/Internet_Protocol_version_4 "IPv4") and [IPv6](https://en.wikipedia.org/wiki/IPv6 "IPv6"). IPv6 hosts can also employ stateless address autoconfiguration or link-local addressing for local network operations.
#### Operation

<img src=https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/DHCP_session.svg/260px-DHCP_session.svg.png style="background-color:white;"alt="An illustration of a typical non-renewing DHCP session; each message may be either a broadcast or a unicast, depending on the DHCP client capabilities.">

DHCP operates on a connectionless service model using UDP. It utilizes UDP port 67 for the server and port 68 for the client. The DHCP process involves four phases: Discovery, Offer, Request, and Acknowledgement (DORA). Clients broadcast a request, and if in different Broadcast Domains, a DHCP Helper or Relay Agent may be used. Renewing clients can communicate via UDP unicast. A BROADCAST flag indicates whether DHCPOFFER should be sent as broadcast or unicast, often using unicast by default.
##### Discovery
The DHCP client broadcasts a DHCPDISCOVER message on the network subnet, using either the destination address 255.255.255.255 (limited broadcast) or the specific subnet broadcast address (directed broadcast). The DHCPDISCOVER may include a request for a specific IP address, which the server considers when offering an address. For instance, when using Ethernet (HTYPE = 1), with a 6-octet MAC address (HLEN = 6), CHADDR is set to the client's MAC address, along with additional configured options.

```sheet
{
	"classes": {
		"class1": {
			"padding-left": "00px",
			"text-align": "center",
			"font-weight": "bold"
		},
		"class2": {
			"text-align": "center",
			"font-weight": "bold"
		},
		"class3": {
			"text-align": "center",
			"font-weight": "bold"
		}
	}
}
---
|Ethernet: source=sender's MAC; destination=_FF:FF:FF:FF:FF:FF_| < | < | < |
| ------------------------------------------------------------ | - | - | - |
|IP: source=_0.0.0.0_; destination=_255.255.255.255_  <br>[UDP](https://en.wikipedia.org/wiki/User_Datagram_Protocol "User Datagram Protocol"): source port=68; destination port=67| < | < | < |
|Octet 0~.class1|Octet 1~.class1|Octet 2~.class1|Octet 3~.class1|
|  OP ~.class1  | HTYPE~.class1 |  HLEN~.class1 |  HOPS~.class1 |
| 0x01  | 0x01  |  0x06 |  0x00 |
|  XID  |   <   |   <   |   <   |
|   0x3903F326  |   <   |   <   |   <   |
| SECS~.class1  |   <   | FLAGS~.class1 |   <   |
|0x0000 |   <   | 0x0000|   <   |
|CIADDR (Client IP address)~.class1     |   <   |   <   |   <   |
|0x00000000     |   <   |   <   |   <   |
|YIADDR (Your IP address)~.class1       |   <   |   <   |   <   |
|0x00000000     |   <   |   <   |   <   |
|SIADDR (Server IP address)~.class1     |   <   |   <   |   <   |
|0x00000000     |   <   |   <   |   <   |
|GIADDR (Gateway IP address)~.class1    |   <   |   <   |   <   |
|0x00000000     |   <   |   <   |   <   |
|CHADDR (Client hardware address)~.class1       |   <   |   <   |   <   |
|0x00053C04     |   <   |   <   |   <   |
|0x8D590000     |   <   |   <   |   <   |
|0x00000000     |   <   |   <   |   <   |
|0x00000000     |   <   |   <   |   <   |
|192 octets of 0s, or overflow space for additional options; [BOOTP](https://en.wikipedia.org/wiki/BOOTP "BOOTP") legacy.|   <   |   <   |   <   |
|[Magic cookie](https://en.wikipedia.org/wiki/Magic_cookie "Magic cookie")~.class1|   <   |   <   |   <   |
|0x63825363|   <   |   <   |   <   |
|DHCP options~.class1      |   <   |   <   |   <   |
|0x350101 53: 1 (DHCP Discover)|   <   |   <   |   <   |
|0x3204c0a80164 50: _192.168.1.100_ requested|   <   |   <   |   <   |
|0x370401030f06 55 (Parameter Request List):<br><br>- 1 (Request Subnet Mask),<br>- 3 (Router),<br>- 15 (Domain Name),<br>- 6 (Domain Name Server)|   <   |   <   |   <   |
|0xff 255 (Endmark)|   <   |   <   |   <   |
```

##### Offer
When a DHCP server receives a DHCPDISCOVER message from a client, which is an IP address lease request, the DHCP server reserves an IP address for the client and makes a lease offer by sending a DHCPOFFER message to the client. This message contains the client's client id (traditionally a MAC address), the IP address that the server is offering, the subnet mask, the lease duration, and the IP address of the DHCP server making the offer. The DHCP server may also take notice of the hardware-level MAC address in the underlying transport layer: according to current [RFCs](https://en.wikipedia.org/wiki/Request_for_Comments "Request for Comments") the transport layer MAC address may be used if no client ID is provided in the DHCP packet.

The DHCP server determines the configuration based on the client's hardware address as specified in the CHADDR (client hardware address) field. In the following example the server (_192.168.1.1_) specifies the client's IP address in the YIADDR (your IP address) field.

```sheet
{
	"classes": {
		"class1": {
			"padding-left": "00px",
			"text-align": "center",
			"font-weight": "bold"
		}
	}
}
---
|Ethernet: source=sender's MAC; destination=client mac address|   <   |   <   |   <   |
| ----------------------------------------------------------- |  ---  |  ---  |  ---  |
|IP: source=_192.168.1.1_; destination=_192.168.1.100_  <br>UDP: source port=67; destination port=68|   <   |   <   |   <   |
|Octet 0~.class1|Octet 1~.class1|Octet 2~.class1|Octet 3~.class1|
|OP~.class1|HTYPE~.class1|HLEN~.class1|HOPS~.class1|
|   0x02   |   0x01   |   0x06   |   0x00   |
|   XID~.class1    |   <   |   <   |   <   |
|0x3903F326|   <   |   <   |   <   |
|   SECS~.class1   |   <   | FLAGS~.class1 |   <   |
|  0x0000  |   <   | 0x0000|   <   |
|CIADDR (Client IP address)~.class1|   <   |   <   |   <   |
|0x00000000|   <   |   <   |   <   |
| YIADDR (Your IP address)~.class1 |   <   |   <   |   <   |
|0xC0A80164 (_192.168.1.100_)|   <   |   <   |   <   |
|SIADDR (Server IP address)~.class1|   <   |   <   |   <   |
|0xC0A80101 (_192.168.1.1_)|   <   |   <   |   <   |
|GIADDR (Gateway IP address)~.class1|   <   |   <   |   <   |
|0x00000000|   <   |   <   |   <   |
|CHADDR (Client hardware address)~.class1|   <   |   <   |   <   |
|0x00053C04|   <   |   <   |   <   |
|0x8D590000|   <   |   <   |   <   |
|0x00000000|   <   |   <   |   <   |
|0x00000000|   <   |   <   |   <   |
|192 octets of 0s; [BOOTP](https://en.wikipedia.org/wiki/BOOTP "BOOTP") legacy.|   <   |   <   |   <   |
|[Magic cookie](https://en.wikipedia.org/wiki/Magic_cookie "Magic cookie")~.class1|   <   |   <   |   <   |
|0x63825363|   <   |   <   |   <   |
|DHCP options~.class1|   <   |   <   |   <   |
|53: 2 (DHCP Offer)|   <   |   <   |   <   |
|1 (subnet mask): _255.255.255.0_|   <   |   <   |   <   |
|3 (Router): _192.168.1.1_|   <   |   <   |   <   |
|51 (IP address lease time): 86400s (1 day)|   <   |   <   |   <   |
|54 (DHCP server): _192.168.1.1_|   <   |   <   |   <   |
|6 (DNS servers):<br><br>- 9.7.10.15,<br>- 9.7.10.16,<br>- 9.7.10.18|   <   |   <   |   <   |
```
##### Request
Upon receiving the DHCP offer, the client broadcasts a DHCPREQUEST message to the server, confirming the requested address. If multiple offers are received, the client accepts only one. Before claiming the IP address, the client broadcasts an ARP request to check for conflicts. If there's no reply, the address is considered available.

The DHCPREQUEST message includes the "server identification" option, specifying the chosen server's offer. Other DHCP servers, upon receiving this message, withdraw their offers to the client and return the offered IP address to the available pool.
```sheet
{
	"classes": {
		"class1": {
			"padding-left": "00px",
			"text-align": "center",
			"font-weight": "bold"
		}
	}
}
---
|Ethernet: source=sender's MAC; destination=_FF:FF:FF:FF:FF:FF_|   <   |   <   |   <   |
|---|---|---|---|
|IP: source=_0.0.0.0_; destination=_255.255.255.255_;[[a]](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol)  <br>[UDP](https://en.wikipedia.org/wiki/User_Datagram_Protocol "User Datagram Protocol"): source port=68; destination port=67|   <   |   <   |   <   |
|  Octet 0~.class1 |Octet 1~.class1|Octet 2~.class1|Octet 3~.class1|
|    OP~.class1    | HTYPE~.class1 |  HLEN~.class1 |  HOPS~.class1 |
|   0x01   |  0x01 | 0x06  |  0x00 |
|    XID~.class1   |   <   |   <   |   <   |
|0x3903F326|   <   |   <   |   <   |
|   SECS~.class1   |   <   | FLAGS~.class1 |   <   |
|0x0000    |   <   |0x0000 |   <   |
|CIADDR (Client IP address)~.class1        |   <   |   <   |   <   |
|0x00000000|   <   |   <   |   <   |
|YIADDR (Your IP address)~.class1          |   <   |   <   |   <   |
|0x00000000|   <   |   <   |   <   |
|SIADDR (Server IP address)~.class1        |   <   |   <   |   <   |
|0xC0A80101 (_192.168.1.1_)|   <   |   <   |   <   |
|GIADDR (Gateway IP address)~.class1       |   <   |   <   |   <   |
|0x00000000|   <   |   <   |   <   |
|CHADDR (Client hardware address)~.class1  |   <   |   <   |   <   |
|0x00053C04|   <   |   <   |   <   |
|0x8D590000|   <   |   <   |   <   |
|0x00000000|   <   |   <   |   <   |
|0x00000000|   <   |   <   |   <   |
|192 octets of 0s; [BOOTP](https://en.wikipedia.org/wiki/BOOTP "BOOTP") legacy.|   <   |   <   |   <   |
|[Magic cookie](https://en.wikipedia.org/wiki/Magic_cookie "Magic cookie")~.class1     |   <   |   <   |   <   |
|0x63825363                     |   <   |   <   |   <   |
|DHCP options~.class1                   |   <   |   <   |   <   |
|53: 3 (DHCP Request)           |   <   |   <   |   <   |
|50: _192.168.1.100_ requested  |   <   |   <   |   <   |
|54 (DHCP server): _192.168.1.1_|   <   |   <   |   <   |
```

##### Acknowledgement
Upon receiving the **DHCPREQUEST** from the client, the DHCP server enters the final phase, acknowledging with a DHCPACK packet containing lease duration and configuration details. The IP configuration process concludes, and the client is expected to configure its network interface accordingly.

To avoid conflicts, the client probes the assigned IP address post-configuration. If conflicts arise, the client broadcasts a DHCPDECLINE message to notify the server.
```sheet
{
	"classes": {
		"class1": {
			"padding-left": "00px",
			"text-align": "center",
			"font-weight": "bold"
		},
		"class2": {
			"text-align": "center",
			"font-weight": "bold"
		},
		"class3": {
			"text-align": "center",
			"font-weight": "bold"
		}
	}
}
---
| Ethernet: source=sender's MAC; destination=client's MAC~.class1 |            <         |        <     |        <     |
|--------------------------------------------------------|---------------------|-------------|-------------|
| IP: source=_192.168.1.1_; destination=_192.168.1.100_   | UDP: source port=67; destination port=68 |       <      |         <    |
| Octet 0~.class2           | Octet 1~.class2 | Octet 2~.class2 | Octet 3~.class2  |
| OP~.class2                | HTYPE~.class2   | HLEN~.class2    | HOPS~.class2    |
| 0x02              | 0x01    | 0x06    | 0x00     |
| XID ~.class1              |    <    |  <      |   <      |
| 0x3903F326        |    <    |  <      |   <      |
| SECS~.class2      |    <    | FLAGS~.class3  |   <      |
| 0x0000            |    <    | 0x0000  |   <      |
| CIADDR (Client IP address)~.class1  |  <      |   <      |  <    |
| 0x00000000        |    <    |  <      |   <      |
| YIADDR (Your IP address)~.class1    |  <      |   <      |  <    |
| 0xC0A80164 (_192.168.1.100_)|  <      |   <      |  <    |
| SIADDR (Server IP address)~.class1  |  <      |   <      |  <    |
| 0xC0A80101 (_192.168.1.1_)  |  <      |   <      |  <    |
| GIADDR (Gateway IP address switched by relay)~.class1 |  <      |  <       |  <    |
| 0x00000000        |    <    |  <      |   <      |
| CHADDR (Client hardware address) ~.class1|  <      |   <      |  <    |   
| 0x00053C04        |    <    |  <      |   <      |
| 0x8D590000        |    <    |  <      |   <      |
| 0x00000000        |    <    |  <      |   <      |
| 0x00000000        |    <    |  <      |   <      |
| 192 octets of 0s. [BOOTP](https://en.wikipedia.org/wiki/BOOTP "BOOTP") legacy |  <      |   <      |  <    |
| [Magic cookie](https://en.wikipedia.org/wiki/Magic_cookie "Magic cookie") ~.class1 |  <      |   <      |  <    |
| 0x63825363        |    <    |  <      |   <      |
| DHCP options ~.class1     |    <    |  <      |   <      |
| 53: 5 (DHCP ACK)   |   <    |  <      |   <      |
| 1 (subnet mask): _255.255.255.0_      |  <      |   <      |  <    |
| 3 (Router): _192.168.1.1_             |  <      |   <      |  <    |
| 51 (IP address lease time): 86400s (1 day)      |  <      |   <      |  <    |
| 54 (DHCP server): _192.168.1.1_       |  <      |   <      |  <    |
| 6 (DNS servers):<br><br>- 9.7.10.15,<br>- 9.7.10.16,<br>- 9.7.10.18 |  <      |   <      |  <    |

```


##### Information
A DHCP client may request more information than the server sent with the original DHCPOFFER. The client may also request repeat data for a particular application. For example, browsers use _DHCP Inform_ to obtain web proxy settings via [WPAD](https://en.wikipedia.org/wiki/Web_Proxy_Auto-Discovery_Protocol "Web Proxy Auto-Discovery Protocol").

##### Releasing
The client sends a request to the DHCP server to release the DHCP information and the client deactivates its IP address. As client devices usually do not know when users may unplug them from the network, the protocol does not mandate the sending of _DHCP Release_.

#### Client configuration parameters
A DHCP server, as defined in RFC 2132 by IANA, offers optional configuration parameters to clients. Clients, particularly in Unix-like systems, can customize and overwrite these settings using the _/etc/dhclient.conf_ configuration file.
#### Options
Options are octet strings of varying length. This is called [Type–length–value](https://en.wikipedia.org/wiki/Type%E2%80%93length%E2%80%93value "Type–length–value") encoding. The first octet is the option code, the second octet is the number of following octets and the remaining octets are code dependent. For example, the DHCP message-type option for an offer would appear as 0x35, 0x01, 0x02, where 0x35 is code 53 for "DHCP message type", 0x01 means one octet follows and 0x02 is the value of "offer".

The following tables list the available DHCP options.
**RFC 1497 (BOOTP Vendor Information Extensions) vendor extensions**
```sheet
{
	classes:{
		class1:{
			"font-weight": "bold",
			"text-align": "center"
		}
	}
}
---
| Code | Name                     | Length                 | Notes |
| ---- | ------------------------ | ---------------------- | ----- |
| 0    | Pad                      | 0 octets               | Can be used to pad other options so that they are aligned to the word boundary; is not followed by length byte |
| 1    | Subnet mask              | 4 octets               | Client's subnet mask as per [RFC 950](https://datatracker.ietf.org/doc/html/rfc950). If both the subnet mask and the router option (option 3) are included, the subnet mask option must be first. |
| 2    | Time offset              | 4 octets               | Offset of the client's subnet in seconds from Coordinated Universal Time (UTC). The offset is expressed as a two's complement 32-bit integer. A positive offset indicates a location east of the zero meridian, and a negative offset indicates a location west of the zero meridian. |
| 3    | Router                   | Multiples of 4 octets  | Available routers, should be listed in order of preference |
| 4    | Time server              | Multiples of 4 octets  | Available [Time Protocol](https://en.wikipedia.org/wiki/Time_Protocol "Time Protocol") servers to synchronize with, should be listed in order of preference |
| 5    | Name server              | Multiples of 4 octets  | Available [IEN 116](https://en.wikipedia.org/wiki/IEN_116 "IEN 116") name servers, should be listed in order of preference |
| 6    | Domain name server        | Multiples of 4 octets  | Available [DNS](https://en.wikipedia.org/wiki/Domain_Name_System "Domain Name System") servers, should be listed in order of preference |
| 7    | Log server                | Multiples of 4 octets  | Available log servers, should be listed in order of preference |
| 8    | Cookie server             | Multiples of 4 octets  | "Cookie" in this case means "fortune cookie" or "quote of the day," a pithy or humorous anecdote often sent as part of a logon process on large computers; it has nothing to do with [cookies sent by websites](https://en.wikipedia.org/wiki/HTTP_cookie "HTTP cookie"). |
| 9    | LPR Server                | Multiples of 4 octets  | A list of [Line Printer Daemon protocol](https://en.wikipedia.org/wiki/Line_Printer_Daemon_protocol "Line Printer Daemon protocol") servers available to the client, should be listed in order of preference |
| 10   | Impress server            | Multiples of 4 octets  | A list of Imagen Impress servers available to the client, should be listed in order of preference |
| 11   | Resource location server  | Multiples of 4 octets  | A list of [Resource Location Protocol](https://en.wikipedia.org/w/index.php?title=Resource_Location_Protocol&action=edit&redlink=1 "Resource Location Protocol (page does not exist)") servers available to the client, should be listed in order of preference |
| 12   | Host name                 | Minimum of 1 octet     | Name of the client. The name may be qualified with the local domain name. |
| 13   | Boot file size            | 2 octets               | Length of the boot image in 512B blocks |
| 14   | [Merit](https://en.wikipedia.org/wiki/Merit_Network "Merit Network") dump file | Minimum of 1 octet | Path where crash dumps should be stored |
| 15   | Domain name               | Minimum of 1 octet     | |
| 16   | Swap server               | 4 octets               | |
| 17   | Root path                 | Minimum of 1 octet     | |
| 18   | Extensions path           | Minimum of 1 octet     | |
| 255  | End                      | 0 octets               | Used to mark the end of the vendor option field |

```

**IP layer parameters per host**
```sheet
| **Code** | **Name**                                | **Length**            | **Notes** | 
| -------- | --------------------------------------- | --------------------- | --------- |
| 19       | IP forwarding enable/disable            | 1 octet               |           |
| 20       | Non-local source routing enable/disable | 1 octet               |           |
| 21       | Policy filter                           | Multiples of 8 octets |           |
| 22       | Maximum datagram reassembly size        | 2 octets              |           |
| 23       | Default IP time-to-live                 | 1 octet               |           |
| 24       | Path MTU aging timeout                  | 4 octets              |           |
| 25       | Path MTU plateau table                  | Multiples of 2 octets |           |
```

**IP Layer Parameters per Interface**
```sheet
| **Code** | **Name**                    | **Length**            | **Notes**                          |
| -------- | --------------------------- | --------------------- | ---------------------------------- |
| 26       | Interface MTU               | 2 octets              |                                    |
| 27       | All subnets are local       | 1 octet               |                                    |
| 28       | Broadcast address           | 4 octets              |                                    |
| 29       | Perform mask discovery      | 1 octet               |                                    |
| 30       | Mask supplier               | 1 octet               |                                    |
| 31       | Perform router discovery    | 1 octet               |                                    |
| 32       | Router solicitation address | 4 octets              |                                    | 
| 33       | Static route                | Multiples of 8 octets | A list of destination/router pairs |
```


**Link layer parameters per interface**
```sheet
| **Code** | **Name**                     | **Length**   | **Notes** |
| ------ | ---------------------------- | ------------ | ----- |
| 34     | Trailer encapsulation option | 1 octet      |       |
| 35     | ARP cache timeout            | 4 octets     |       |
| 36     | Ethernet encapsulation       | 1 octet      |       | 
```

**TCP parameters**
```sheet
|Code|Name|Length|Notes|
|---|---|---|---|
|37|TCP default TTL|1 octet||
|38|TCP keepalive interval|4 octets||
|39|TCP keepalive garbage|1 octet||
```

**Application and service parameters**
```sheet
| **Code** | **Name**                                                                                                                                      | **Length**            | **Notes** |
| -------- | --------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- | --------- |
| 40       | Network information service domain                                                                                                            | Minimum of 1 octet    |           |
| 41       | Network information servers                                                                                                                   | Multiples of 4 octets |           |
| 42       | [Network Time Protocol](https://en.wikipedia.org/wiki/Network_Time_Protocol "Network Time Protocol") (NTP) servers                            | Multiples of 4 octets |           |
| 43       | Vendor-specific information                                                                                                                   | Minimum of 1 octets   |           |
| 44       | NetBIOS over TCP/IP name server                                                                                                               | Multiples of 4 octets |           |
| 45       | NetBIOS over TCP/IP datagram Distribution Server                                                                                              | Multiples of 4 octets |           |
| 46       | NetBIOS over TCP/IP node type                                                                                                                 | 1 octet               |           |
| 47       | NetBIOS over TCP/IP scope                                                                                                                     | Minimum of 1 octet    |           |
| 48       | [X Window System](https://en.wikipedia.org/wiki/X_Window_System "X Window System") font server                                                | Multiples of 4 octets |           |
| 49       | X Window System display manager                                                                                                               | Multiples of 4 octets |           |
| 64       | [Network Information Service](https://en.wikipedia.org/wiki/Network_Information_Service "Network Information Service")+ domain                | Minimum of 1 octet    |           |
| 65       | Network Information Service+ servers                                                                                                          | Multiples of 4 octets |           |
| 68       | Mobile IP home agent                                                                                                                          | Multiples of 4 octets |           |
| 69       | [Simple Mail Transfer Protocol](https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol "Simple Mail Transfer Protocol") (SMTP) server    | Multiples of 4 octets |           |
| 70       | [Post Office Protocol](https://en.wikipedia.org/wiki/Post_Office_Protocol "Post Office Protocol") (POP3) server                               | Multiples of 4 octets |           |
| 71       | [Network News Transfer Protocol](https://en.wikipedia.org/wiki/Network_News_Transfer_Protocol "Network News Transfer Protocol") (NNTP) server | Multiples of 4 octets |           |
| 72       | Default [World Wide Web](https://en.wikipedia.org/wiki/World_Wide_Web "World Wide Web") (WWW) server                                          | Multiples of 4 octets |           |
| 73       | Default [Finger protocol](https://en.wikipedia.org/wiki/Finger_protocol "Finger protocol") server                                             | Multiples of 4 octets |           |
| 74       | Default [Internet Relay Chat](https://en.wikipedia.org/wiki/Internet_Relay_Chat "Internet Relay Chat") (IRC) server                           | Multiples of 4 octets |           |
| 75       | [StreetTalk](https://en.wikipedia.org/wiki/StreetTalk "StreetTalk") server                                                                    | Multiples of 4 octets |           |
| 76       | StreetTalk Directory Assistance (STDA) server                                                                                                 | Multiples of 4 octets |           | 
```

**DHCP extensions**
```sheet
| **Code** | **Name**                  | **Length**          | **Notes** |
| -------- | ------------------------- | ------------------- | --------- |
| 50       | Requested IP address      | 4 octets            |           |
| 51       | IP address lease time     | 4 octets            |           |
| 52       | Option overload           | 1 octet             |           |
| 53       | DHCP message type         | 1 octet             |           |
| 54       | Server identifier         | 4 octets            |           |
| 55       | Parameter request list    | Minimum of 1 octet  |           |
| 56       | Message                   | Minimum of 1 octet  |           |
| 57       | Maximum DHCP message size | 2 octets            |           |
| 58       | Renewal (T1) time value   | 4 octets            |           |
| 59       | Rebinding (T2) time value | 4 octets            |           |
| 60       | Vendor class identifier   | Minimum of 1 octet  |           |
| 61       | Client-identifier         | Minimum of 2 octets |           |
| 66       | TFTP server name          | Minimum of 1 octet  |           |
| 67       | Bootfile name             | Minimum of 1 octet  |           | 
```

##### DHCP message types
This table lists the DHCP message types, documented in RFC 2132, RFC 3203, RFC 6926 and RFC 7724.These codes are the value in the DHCP extension 53, shown in the table above.

**DHCP message types**
```sheet
| **Code** | **Name**             | **Length** | **RFC** | 
| -------- | -------------------- | ---------- | ------- |
| 1        | DHCPDISCOVER         | 1 octet    | rfc2132 |
| 2        | DHCPOFFER            | 1 octet    | rfc2132 |
| 3        | DHCPREQUEST          | 1 octet    | rfc2132 |
| 4        | DHCPDECLINE          | 1 octet    | rfc2132 |
| 5        | DHCPACK              | 1 octet    | rfc2132 |
| 6        | DHCPNAK              | 1 octet    | rfc2132 |
| 7        | DHCPRELEASE          | 1 octet    | rfc2132 |
| 8        | DHCPINFORM           | 1 octet    | rfc2132 |
| 9        | DHCPFORCERENEW       | 1 octet    | rfc32   |
| 10       | DHCPLEASEQUERY       | 1 octet    | rfc4388 |
| 11       | DHCPLEASEUNASSIGNED  | 1 octet    | rfc4388 |
| 12       | DHCPLEASEUNKNOWN     | 1 octet    | rfc4388 |
| 13       | DHCPLEASEACTIVE      | 1 octet    | rfc4388 |
| 14       | DHCPBULKLEASEQUERY   | 1 octet    | rfc6926 |
| 15       | DHCPLEASEQUERYDONE   | 1 octet    | rfc6926 |
| 16       | DHCPACTIVELEASEQUERY | 1 octet    | rfc7724 |
| 17       | DHCPLEASEQUERYSTATUS | 1 octet    | rfc7724 |
| 18       | DHCPTLS              | 1 octet    | rfc7724 |
```

######  Client vendor identification
An option exists to identify the vendor and functionality of a DHCP client. The information is a [variable-length string](https://en.wikipedia.org/wiki/Variable-length_code "Variable-length code") of characters or octets which has a meaning specified by the vendor of the DHCP client. One method by which a DHCP client can communicate to the server that it is using a certain type of hardware or firmware is to set a value in its DHCP requests called the Vendor Class Identifier (VCI) (Option 60).

The value to which this option is set gives the DHCP server a hint about any required extra information that this client needs in a DHCP response. Some types of [set-top boxes](https://en.wikipedia.org/wiki/Set-top_boxes "Set-top boxes") set the VCI to inform the DHCP server about the hardware type and functionality of the device. An [Aruba](https://en.wikipedia.org/wiki/Aruba_Networks "Aruba Networks") campus [wireless access point](https://en.wikipedia.org/wiki/Wireless_access_point "Wireless access point"), for example, supplies value 'ArubaAP' as option 60 in its DHCPDISCOVER message. The DHCP server can then augment its DHCPOFFER with an IP address of an Aruba [wireless controller](https://en.wikipedia.org/wiki/Wireless_controller "Wireless controller") in option 43, so the access point knows where to register itself.

Setting a VCI by the client allows a DHCP server to differentiate between client machines and process the requests from them appropriately.

##### Other extensions
**Documented DHCP options**
```sheet
| **Code** | **Name**                                                                                                                                       | **Length**                                | **RFC**  |
| -------- | ---------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------- | -------- |
| 82       | [Relay agent information](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol#Relay_agent_information_sub-options)               | Minimum of 2 octets                       | RFC      |
| 85       | [Novell Directory Service](https://en.wikipedia.org/wiki/Novell_Directory_Service "Novell Directory Service") (NDS) servers                    | Minimum of 4 octets, multiple of 4 octets | RFC 2241 |
| 86       | NDS tree name                                                                                                                                  | Variable                                  | RFC 2241 |
| 87       | NDS context                                                                                                                                    | Variable                                  | RFC 2241 |
| 100      | [Time zone](https://en.wikipedia.org/wiki/Time_zone "Time zone"), POSIX style                                                                  | Variable                                  | RFC 4833 |
| 101      | [Time zone](https://en.wikipedia.org/wiki/Time_zone "Time zone"), [tz database](https://en.wikipedia.org/wiki/Tz_database "Tz database") style | Variable                                  | RFC 4833 |
| 114      | DHCP Captive-Portal                                                                                                                            | Variable                                  | RFC 8910 |
| 119      | [Domain search](https://en.wikipedia.org/wiki/Search_domain "Search domain")                                                                   | Variable                                  | RFC 3397 |
| 121      | Classless static route                                                                                                                         | Variable                                  | RFC 3442 |
| 209      | Configuration File                                                                                                                             | Variable                                  | RFC 5071 |
| 210      | Path Prefix                                                                                                                                    | Variable                                  | RFC 5071 |
| 211      | Reboot Time                                                                                                                                    | Variable                                  | RFC 5071 | 
```

###### Relay agent information sub-options
The relay agent information option (option 82) specifies container for attaching sub-options to DHCP requests transmitted between a DHCP relay and a DHCP server.
**Relay agent sub-options**
```sheet
| **Code** | **Name**                                                               | **Length**         | **RFC**   |
| -------- | ---------------------------------------------------------------------- | ------------------ | --------- |
| 1        | Agent Circuit ID                                                       | Minimum of 1 octet | RFC 3046  |
| 2        | Agent Remote ID                                                        | Minimum of 1 octet | RFC 3046  |
| 4        | Data-Over-Cable Service Interface Specifications (DOCSIS) device class | 4 octets           | RFC 3256  | 
```

#### Relaying
DHCP clients in a network communicate with DHCP servers to obtain IP addresses. In scenarios with multiple subnets, DHCP relay agents facilitate communication. These agents, installed on subnets without direct DHCP server access, forward client broadcasts to DHCP servers using unicast. The relay agent's IP address and the DHCP server addresses are manually configured. The DHCP server determines the subnet and allocates an IP address, sending the reply to the relay agent's IP address. The relay agent then transmits the response locally. If a client's IP stack doesn't accept unicast without an IP address, the broadcast bit is set in DHCPDISCOVER packets. Communication between the relay agent and DHCP server uses UDP ports 67 as both source and destination.
#### Client states

<img src=https://upload.wikimedia.org/wikipedia/commons/a/af/Dhcp-client-state-diagram.svg style="background-color: white;" alt="A simplified DHCP client state-transition diagram based on figure 5 of RFC 2131">
A DHCP client can receive these messages from a server:

- DHCPOFFER
- DHCPACK
- DHCPNAK

The client moves through DHCP states depending on how the server responds to the messages that the client sends.

#### Reliability
DHCP ensures reliability through periodic renewal, rebinding, and failover. Clients attempt lease renewal by sending a unicast DHCPREQUEST to the server. If the server is unreachable, the client repeats the DHCPREQUEST until successful contact is made. In extended server unavailability, rebinding occurs through broadcasting DHCPREQUEST to all servers. If another server can renew the lease, it does so.

Rebinding requires accurate client binding information on backup servers, posing challenges in maintaining consistency. A proposed fault-tolerant DHCP server implementation was discussed but not formalized.

If rebinding fails, the lease eventually expires, prompting the client to restart the DHCP process with a DHCPDISCOVER broadcast, accepting any offered IP address. Ongoing connections are broken due to the changed IP address.

#### IPv6 networks
The basic methodology of DHCP was developed for networks based on [Internet Protocol version 4](https://en.wikipedia.org/wiki/IPv4 "IPv4") (IPv4). Since the development and deployment of [IPv6](https://en.wikipedia.org/wiki/IPv6 "IPv6") networks, DHCP has also been used for assigning parameters in such networks, despite the inherent features of IPv6 for [stateless address autoconfiguration](https://en.wikipedia.org/wiki/Stateless_address_autoconfiguration "Stateless address autoconfiguration"). The IPv6 version of the protocol is designated as [DHCPv6](https://en.wikipedia.org/wiki/DHCPv6 "DHCPv6").

#### Security
The base DHCP lacks authentication, making it susceptible to attacks like rogue DHCP servers providing false information, unauthorized clients gaining access, and resource exhaustion attacks. Rogue DHCP servers can lead to denial-of-service or man-in-the-middle attacks by providing incorrect information to clients. Additionally, without client authentication, unauthorized clients can gain access to IP addresses and exhaust the DHCP server's pool.

DHCP offers some mitigation mechanisms, including the Relay Agent Information Option (Option 82) for authorization control. Authentication for DHCP Messages (RFC 3118) provides a way to authenticate DHCP messages but faces challenges in key management. Despite attempts, widespread adoption has been limited due to identified vulnerabilities and deployment challenges. Proposed solutions involve using 802.1x, PANA, and EAP for DHCP authentication, but progress has been slow, and some proposals have not advanced beyond draft stages. For more details on DHCP snooping.
### [FTP(File Transfer Protocol)](https://en.wikipedia.org/wiki/File_Transfer_Protocol)
It is a standard [communication protocol](https://en.wikipedia.org/wiki/Communication_protocol "Communication protocol") used for the transfer of [computer files](https://en.wikipedia.org/wiki/Computer_file "Computer file") from a server to a client on a [computer network](https://en.wikipedia.org/wiki/Computer_network "Computer network"). Ir is built on a client–server model architecture using separate control and data connections between the client and the server. FTP users may authenticate themselves with a [plain-text](https://en.wikipedia.org/wiki/Plaintext "Plaintext") sign-in protocol, normally in the form of a username and password, but can connect anonymously if the server is configured to allow it. 

> For secure transmission that protects the username and password, and encrypts the content, It is often [secured](https://en.wikipedia.org/wiki/File_Transfer_Protocol#Security) with [SSL/TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security "Transport Layer Security") ([FTPS](https://en.wikipedia.org/wiki/FTPS "FTPS")) or replaced with [SFTP(SSH File Transfer Protocol)](https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol "SSH File Transfer Protocol").

The first FTP client applications were [command-line programs](https://en.wikipedia.org/wiki/Command-line_interface "Command-line interface") developed before [operating systems](https://en.wikipedia.org/wiki/Operating_system "Operating system") had [graphical user interfaces](https://en.wikipedia.org/wiki/Graphical_user_interface "Graphical user interface"), and are still shipped with most [Windows](https://en.wikipedia.org/wiki/Windows "Windows"), [Unix](https://en.wikipedia.org/wiki/Unix "Unix"), and [Linux](https://en.wikipedia.org/wiki/Linux "Linux") operating systems. Many dedicated FTP [clients](https://en.wikipedia.org/wiki/Client_(computing) "Client (computing)") and automation utilities have since been developed for [desktops](https://en.wikipedia.org/wiki/Desktop_computer "Desktop computer"), servers, mobile devices, and hardware, and FTP has been incorporated into productivity applications such as [HTML editors](https://en.wikipedia.org/wiki/HTML_editor "HTML editor") and [file managers](https://en.wikipedia.org/wiki/File_managers "File managers").
    An FTP client used to be commonly integrated in [web browsers](https://en.wikipedia.org/wiki/Web_browser "Web browser"), where file servers are browsed with the [URI](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier "Uniform Resource Identifier") prefix "`ftp://`". In 2021, FTP support was dropped by Google Chrome and Firefox,two major web browser vendors, due to it being superseded by the more secure SFTP and FTPS; although neither of them have implemented the newer protocols.
#### Protocol overview
##### Communication and data transfer

<img style="background-color: #ffffff;border-left=100px"src=https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Passive_FTP_Verbindung.svg/220px-Passive_FTP_Verbindung.svg.png alt="Illustration of starting a passive connection using port 21">
FTP may run in _active_ or _passive_ mode, which determines how the data connection is established. (This sense of "mode" is different from that of the MODE command in the FTP protocol.)

- In active mode, the client starts listening for incoming data connections from the server on port M. It sends the FTP command PORT M to inform the server on which port it is listening. The server then initiates a data channel to the client from its port 20, the FTP server data port.
- In situations where the client is behind a [firewall](https://en.wikipedia.org/wiki/Firewall_(computing) "Firewall (computing)") and unable to accept incoming TCP connections, _passive mode_ may be used. In this mode, the client uses the control connection to send a PASV command to the server and then receives a server IP address and server port number from the server, which the client then uses to open a data connection from an arbitrary client port to the server IP address and server port number received.

The server responds over the control connection with [three-digit status codes](https://en.wikipedia.org/wiki/List_of_FTP_server_return_codes "List of FTP server return codes") in ASCII with an optional text message. For example, "200" (or "200 OK") means that the last command was successful. The numbers represent the code for the response and the optional text represents a human-readable explanation or request (e.g. `Need account for storing file`). An ongoing transfer of file data over the data connection can be aborted using an interrupt message sent over the control connection.

FTP needs two ports (one for sending and one for receiving) because it was originally designed to operate on top of [NCP(Network Control Protocol)](https://en.wikipedia.org/wiki/Network_Control_Protocol_(ARPANET) "Network Control Protocol (ARPANET)") , which was a [simplex protocol](https://en.wikipedia.org/wiki/Simplex_communication "Simplex communication") that utilized two [port addresses](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers "List of TCP and UDP port numbers"), establishing two connections, for two-way communications. An odd and an even port were reserved for each [application layer](https://en.wikipedia.org/wiki/Application_layer "Application layer") application or protocol. The standardization of TCP and UDP reduced the need for the use of two simplex ports for each application down to one duplex port,  but the FTP protocol was never altered to only use one port, and continued using two for backwards compatibility.

##### NAT and firewall traversal
FTP normally transfers data by having the server connect back to the client, after the PORT command is sent by the client. This is problematic for both [NATs](https://en.wikipedia.org/wiki/Network_address_translation "Network address translation") and firewalls, which do not allow connections from the Internet towards internal hosts. For NATs, an additional complication is that the representation of the IP addresses and port number in the PORT command refer to the internal host's IP address and port, rather than the public IP address and port of the NAT.

There are two approaches to solve this problem. One is that the FTP client and FTP server use the PASV command, which causes the data connection to be established from the FTP client to the server. This is widely used by modern FTP clients. Another approach is for the NAT to alter the values of the PORT command, using an [application-level gateway](https://en.wikipedia.org/wiki/Application-level_gateway "Application-level gateway") for this purpose.

<img src=https://upload.wikimedia.org/wikipedia/commons/f/fb/FTP_model.png style="width: 1000px;" alt="A model chart of how FTP works">
##### Data types
While transferring data over the network, five data types are defined:
- **[ASCII](https://en.wikipedia.org/wiki/ASCII "ASCII") (TYPE A)**: <span style="font-weight:bold; color:#92d050">Used for text</span> Data is converted, if needed, from the sending host's character representation to ["8-bit ASCII"](https://en.wikipedia.org/wiki/Extended_ASCII "Extended ASCII") before transmission, and (again, if necessary) to the receiving host's character representation, including [newlines](https://en.wikipedia.org/wiki/Newline "Newline"). As a consequence, this mode is inappropriate for files that contain data other than ASCII.
- I**mage (TYPE I/Binary mode)**: The sending machine sends each file byte-by-byte, and the recipient stores the [bytestream](https://en.wikipedia.org/wiki/Bytestream "Bytestream") as it receives it.
- **[EBCDIC](https://en.wikipedia.org/wiki/EBCDIC "EBCDIC") (TYPE E)**: <span style="font-weight:bold; color:#92d050">Used for plain text between hosts using the EBCDIC character set.</span>
- **Local (TYPE L _n_)**: Designed to support file transfer between <span title="36-bit systems such as DEC PDP-10s"  style="border-bottom: 1px dashed #999; cursor: help;">machines which do not use 8-bit bytes</span>.
	- For example, "TYPE L 9" would be used to transfer data in 9-bit bytes, or "TYPE L 36" to transfer 36-bit words. 
		> Most contemporary FTP clients/servers only support L 8, which is equivalent to I.
- **[Unicode](https://en.wikipedia.org/wiki/Unicode "Unicode") text files using [UTF-8](https://en.wikipedia.org/wiki/UTF-8 "UTF-8") (TYPE U)**: defined in an expired [Internet Draft](https://en.wikipedia.org/wiki/Internet_Draft "Internet Draft")which never became an RFC, though it has been implemented by several FTP clients/servers.

For text files (TYPE A and TYPE E), three different format control options are provided, to control how the file would be printed:
- **Non-print (TYPE A N and TYPE E N)** – the file does not contain any carriage control characters intended for a printer
- **[Telnet]((Network Protocols and Services#[Telnet(Teletype network)(https //en.wikipedia.org/wiki/Telnet)) (TYPE A T and TYPE E T)** – the file contains Telnet (or in other words, ASCII C0) carriage control characters (CR, LF, etc)
- **[ASA](https://en.wikipedia.org/wiki/ASA_carriage_control_characters "ASA carriage control characters") (TYPE A A and TYPE E A)** – the file contains ASA carriage control characters

These formats were mainly relevant to [line printers](https://en.wikipedia.org/wiki/Line_printer "Line printer"); most contemporary FTP clients/servers only support the default format control of N.
##### File structures

File organization is specified using the STRU command. The following file structures are defined in section 3.1.1 of RFC959:
- **_F_ or FILE structure (stream-oriented)**. Files are viewed as an arbitrary sequence of bytes, characters or words. This is the usual file structure on <span title="CP/M, MS-DOS and Microsoft Windows" style="border-bottom: 1px dashed #999; cursor: help;color:#92d050;">Unix systems and other systems</span>. 
- **_R_ or RECORD structure (record-oriented)**: Files are viewed as divided into records, which may be fixed or variable length. This file organization is common on <span title=" MVS, VM/CMS, OS/400 and VMS, which support record-oriented filesystems" Record-oriented filesystem")." style="border-bottom: 1px dashed #999; cursor: help;color:#92d050;">mainframe and midrange systems</span>. This is only used in mainframe and microcomputer file transfer applications.
- **_P_ or PAGE structure (page-oriented)**: Files are divided into pages, which may either contain data or metadata; each page may also have a header giving various attributes. This file structure was specifically designed for [TENEX](https://en.wikipedia.org/wiki/TENEX_(operating_system) "TENEX (operating system)") systems, and is generally not supported on other platforms.

##### Data transfer modes
Data transfer can be done in any of three modes:
- **Stream mode (MODE S)**: Data is sent as a continuous stream, relieving FTP from doing any processing. Rather, all processing is left up to [TCP](https://en.wikipedia.org/wiki/Transmission_Control_Protocol "Transmission Control Protocol"). No End-of-file indicator is needed, unless the data is divided into [records](https://en.wikipedia.org/wiki/Record_(computer_science) "Record (computer science)").
- **Block mode (MODE B)**: Designed primarily for transferring record-oriented files (STRU R), although can also be used to transfer stream-oriented (STRU F) text files. FTP puts each record (or line) of data into several blocks (block header, byte count, and data field) and then passes it on to TCP.
- **Compressed mode (MODE C)**: Extends MODE B with data compression using [run-length encoding](https://en.wikipedia.org/wiki/Run-length_encoding "Run-length encoding").

Some FTP software also implements a [DEFLATE](https://en.wikipedia.org/wiki/DEFLATE "DEFLATE")-based compressed mode, sometimes called "Mode Z" after the command that enables it. This mode was described in an [Internet Draft](https://en.wikipedia.org/wiki/Internet_Draft "Internet Draft"), but not standardized.
[GridFTP](https://en.wikipedia.org/wiki/GridFTP "GridFTP") defines additional modes, MODE E as extensions of MODE B.

### [HTTP(Hypertext Transfer Protocol)](https://en.wikipedia.org/wiki/HTTP)
HTTP is an application layer protocol in the Internet protocol suite, serving as the foundation for the World Wide Web. It enables the transfer of hypermedia documents with hyperlinks.

- **HTTPS:** A secure variant, used by over 85% of websites.
- **HTTP/2 (2015):** Enhances efficiency with wider adoption (36% of websites) and support from major web browsers and servers over TLS.
- **HTTP/3 (2022):** Successor to HTTP/2, utilized by 28% of websites. It employs QUIC instead of TCP, resulting in lower latency and faster loading times compared to HTTP/2 and HTTP/1.1. Support is widespread, including in Cloudflare, Google Chrome, and Firefox.
#### Technical overview
It functions as a request–response protocol in the client–server model. A web browser acts as the client, while a process known as a web server, running on a hosting computer, serves one or more websites. The client submits an HTTP request message to the server, which, in turn, returns a response message containing completion status information and possibly requested content.

A web browser is an example of a user agent (UA), with other types including web crawlers, voice browsers, mobile apps, and other software accessing or displaying web content. It permits intermediate network elements like web cache servers to enhance communications between clients and servers. High-traffic websites benefit from web cache servers, and web browsers cache resources to reduce network traffic. HTTP proxy servers at private network boundaries can facilitate communication for clients without a globally routable address.
Some HTTP headers are managed hop-by-hop, while others are managed end-to-end. HTTP is an application layer protocol within the Internet protocol suite, designed presuming an underlying reliable transport layer protocol.

HTTP resources are identified by Uniform Resource Locators (URLs) using URI schemes http and https. 
- **HTTP/1.0**: a separate TCP connection is made for every resource request, whereas in HTTP/1.1, a TCP connection can be reused for multiple resource requests, reducing latency.
- **HTTP/2**: maintains the client–server model but introduces a compressed binary representation of metadata, a single encrypted TCP/IP connection per accessed server domain, bidirectional streams to address head-of-line blocking, and a push capability for servers to send data to clients.
- **HTTP/3**: is a revision of HTTP/2 using QUIC + UDP transport protocols instead of TCP, improving communication speed and avoiding occasional TCP connection congestion issues.
#### HTTP data exchange

HTTP is a stateless application-level protocol requiring a reliable network transport connection for data exchange between client and server. In HTTP implementations, TCP/IP connections are utilized, typically on well-known ports (port 80 for unencrypted connections, and port 443 for encrypted connections).

##### Request and Response Messages
Data exchange occurs through a sequence of request–response messages over a session layer transport connection. An HTTP client initiates a connection to a server, sends a request message, and receives a response message from the server. The connection may be closed by either party at any time.

```sheet
{
	classes:{
		class1:{
		}
	}
}
---
| Type |  HTTP Version | Information |
| ---- | -------- | ----------- |
| Persistent Connection | 0.9 | TCP/IP connection is always closed after the server response |
| ^ | 1.0 | TCP/IP connection should be closed by the server after a response |
| ^ | 1.1 | keep-alive mechanism for connection resuse, reducing request latency, and HTTP pipelining added temporarily |
| ^ | 2 | Extended connections by multiplexing many concurrent requests/responses through a single TCP/IP connection |
| ^ | 3 | Uses QUIC + UDP instead of TCP/IP connections |
| Content Retrieval Optimizations | 0.9 | Entire resource sent in response |
| ^ | 1.0 | header for managing client-cached resources, allowing conditional GET requests |
| ^ | 1.1 | new headers for conditional retrieval,chunked transfer encoding for streaming content in chunks, and byte range for requesting specific portions of a resource |
| ^ | 2 | Retained features of previous |
| ^ | 3 | Retained features of previous |
```
#### HTTP authentication

HTTP supports various authentication schemes, including basic access authentication and digest access authentication, utilizing a challenge–response mechanism. In this process, the server issues a challenge before providing the requested content. The framework for access control and authentication in HTTP is extensible, offering a set of challenge–response authentication schemes. Servers can use these schemes to challenge client requests, and clients can respond with authentication information.

Authentication mechanisms, such as basic and digest access authentication, are managed by client and server HTTP software. They are not directly controlled by web applications that use a web application session. Additionally, the HTTP Authentication specification introduces authentication realms, allowing for the division of resources under a root URI into separate authentication scopes. The realm value, if present, combines with the canonical root URI to define the protection space for the challenge.

#### HTTP application session

HTTP is a [stateless protocol](https://en.wikipedia.org/wiki/Stateless_protocol "Stateless protocol"). A stateless protocol does not require the web server to retain information or status about each user for the duration of multiple requests.

Some [web applications](https://en.wikipedia.org/wiki/Web_application "Web application") need to manage user sessions, so they implement states, or [server side sessions](https://en.wikipedia.org/wiki/Session_(computer_science) "Session (computer science)"), using for instance [HTTP cookies](https://en.wikipedia.org/wiki/HTTP_cookie "HTTP cookie")[[45]](https://en.wikipedia.org/wiki/HTTP#cite_note-48) or hidden [variables](https://en.wikipedia.org/wiki/Variable_(computer_science) "Variable (computer science)") within [web forms](https://en.wikipedia.org/wiki/Form_(web) "Form (web)").

To start an application user session, an interactive [authentication](https://en.wikipedia.org/wiki/Authentication "Authentication") via web application [login](https://en.wikipedia.org/wiki/Login "Login") must be performed. To stop a user session a [logout](https://en.wikipedia.org/wiki/Logout "Logout") operation must be requested by user. These kind of operations do not use [HTTP authentication](https://en.wikipedia.org/wiki/HTTP#HTTP_authentication) but a custom managed web application authentication.

HTTP is a stateless protocol, meaning the web server doesn't retain information about users between requests. Some web applications use server-side sessions, employing mechanisms like HTTP cookies or hidden variables in web forms to manage user sessions.

For starting and ending user sessions, web applications perform custom-managed authentication, distinct from HTTP authentication.

#### HTTP/1.1 Request Messages
##### Request Syntax
A client sends request messages to the server, consisting of:
- A request line indicating the method, requested URL, and protocol version.
  Example: `GET /images/logo.png HTTP/1.1`
- Zero or more request header fields, each with a field name and value.
  **Example**:
  ```http
  Host: www.example.com
  Accept-Language: en
```
- An empty line
- An optional message body

In HTTP/1.1, all header fields except `Host: hostname` are optional.

##### Request Methods
HTTP defines methods like GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, and PATCH to indicate actions on resources. GET retrieves data, HEAD retrieves metadata, POST processes representation, PUT creates or updates state, DELETE deletes state, CONNECT establishes a TCP/IP tunnel, OPTIONS checks supported methods, TRACE echoes the received request, and PATCH partially updates a resource.

Method names are case-sensitive. GET is used for data retrieval, HEAD for metadata retrieval, POST for processing representations, PUT for creating or updating state, DELETE for deleting state, CONNECT for tunneling, OPTIONS for checking supported methods, TRACE for echoing the received request, and PATCH for partial updates.

The PATCH method is used for partial updates, saving bandwidth by modifying part of a file or document without transferring it entirely.

##### Request methods
<img src=https://upload.wikimedia.org/wikipedia/commons/c/c6/Http_request_telnet_ubuntu.png style="width:900px;" alt="An HTTP/1.1 request made using telnet. The request message, response header section, and response body are highlighted.">
HTTP methods, also known as verbs, indicate the desired action on the identified resource. The HTTP/1.0 specification included GET, HEAD, and POST. HTTP/1.1 added PUT, DELETE, CONNECT, OPTIONS, and TRACE. Servers can be configured to support any combination of methods. Unknown methods are treated as unsafe and non-idempotent. There is no limit to defining methods, allowing for future additions without breaking existing infrastructure. For instance, WebDAV introduced seven methods, and RFC 5789 specified the PATCH method.

Method names are case sensitive. This is in contrast to HTTP header field names which are case-insensitive.

| Method  | Description  |
|---------|--------------|
| GET     | Requests the target resource to transfer a representation of its state. It should only retrieve data with no other side effects. Preferred for retrieving resources without making changes, making it suitable for bookmarking and sharing. GET responses are eligible for caching.  |
| HEAD    | Similar to GET, but requests only the representation metadata in the response header without transferring the entire representation. Useful for checking the availability of a page through status codes and quickly finding the size of a file (Content-Length). |
| POST    | Requests that the target resource processes the representation enclosed in the request. Used for actions such as posting a message to an internet forum, subscribing to a mailing list, or completing an online shopping transaction. |
| PUT     | Requests that the target resource creates or updates its state with the representation enclosed in the request. The client specifies the target location on the server. |
| DELETE  | Requests that the target resource deletes its state. |
| CONNECT | Requests that the intermediary establishes a TCP/IP tunnel to the origin server identified by the request target. Often used to secure connections through HTTP proxies with TLS. |
| OPTIONS | Requests that the target resource transfers the HTTP methods it supports. Useful for checking a web server's functionality by requesting '*' instead of a specific resource. |
| TRACE   | Requests that the target resource transfers the received request in the response body. Allows a client to see changes or additions made by intermediaries. |
| PATCH   | Requests that the target resource modifies its state according to the partial update defined in the enclosed representation. Saves bandwidth by updating part of a file or document without transferring it entirely. |
All general-purpose web servers are required to implement at least the GET and HEAD methods, and all other methods are considered optional by the specification.

| Request method | RFC | Request has payload body | Response has payload body | Safe | Idempotent | Cacheable |
| --- | --- | --- | --- | --- | --- | --- |
| GET | [RFC 9110](https://datatracker.ietf.org/doc/html/rfc9110) | Optional | Yes | Yes | Yes | Yes |
| HEAD | [RFC 9110](https://datatracker.ietf.org/doc/html/rfc9110) | Optional | No | Yes | Yes | Yes |
| POST | [RFC 9110](https://datatracker.ietf.org/doc/html/rfc9110) | Yes | Yes | No | No | Yes |
| PUT | [RFC 9110](https://datatracker.ietf.org/doc/html/rfc9110) | Yes | Yes | No | Yes | No |
| DELETE | [RFC 9110](https://datatracker.ietf.org/doc/html/rfc9110) | Optional | Yes | No | Yes | No |
| CONNECT | [RFC 9110](https://datatracker.ietf.org/doc/html/rfc9110) | Optional | Yes | No | No | No |
| OPTIONS | [RFC 9110](https://datatracker.ietf.org/doc/html/rfc9110) | Optional | Yes | Yes | Yes | No |
| TRACE | [RFC 9110](https://datatracker.ietf.org/doc/html/rfc9110) | No | Yes | Yes | Yes | No |
| PATCH | [RFC 5789](https://datatracker.ietf.org/doc/html/rfc5789) | Yes | Yes | No | No | No |
###### Safe methods
Safe methods in HTTP have no intended effect on the server, making them essentially read-only. GET, HEAD, OPTIONS, and TRACE are defined as safe. These methods don't exclude side effects like appending request information to a log file or charging an advertising account, but these side effects are not client-requested.

On the other hand, POST, PUT, DELETE, CONNECT, and PATCH are not safe and may modify the server's state or have other effects, such as sending an email. Conforming web robots or crawlers typically avoid using these methods due to their potential impact.

While GET requests are technically considered safe, improper programming can allow them to cause unintended changes on the server. For example, a website allowing deletion through a GET request is discouraged; a properly coded website would require DELETE or POST for such actions. Instances of unintended changes have occurred, emphasizing the importance of proper handling to avoid issues like those experienced during the Google Web Accelerator beta.
###### Idempotent methods
An HTTP request method is considered idempotent if multiple identical requests yield the same effect as a single request. PUT and DELETE, along with safe methods, are idempotent. Safe methods are inherently idempotent, while PUT and DELETE disregard successive identical requests.

On the contrary, POST, CONNECT, and PATCH are not necessarily idempotent. Sending identical POST requests multiple times may have further effects. This behavior could be intentional but might occur accidentally, like when a user clicks a button multiple times. The idempotent nature is not enforced by the protocol or server, and a web application can violate this principle, potentially leading to undesirable consequences if a user agent assumes that repeating the same request is safe when it's not.
###### Cacheable methods
A request method is _cacheable_ if responses to requests with that method may be stored for future reuse. The methods GET, HEAD, and POST are defined as cacheable.
In contrast, the methods PUT, DELETE, CONNECT, OPTIONS, TRACE, and PATCH are not cacheable.
##### Request header fields
See also: [List of HTTP header fields § Request fields](https://en.wikipedia.org/wiki/List_of_HTTP_header_fields#Request_fields "List of HTTP header fields")
Request header fields allow the client to pass additional information beyond the request line, acting as request modifiers (similarly to the parameters of a procedure). They give information about the client, about the target resource, or about the expected handling of the request.

#### HTTP/1.1 response messages
A response message is sent by a server to a client as a reply to its former request message.
##### Response syntax
A server sends response messages to the client, which consist of:

- A status line, including the protocol version, a space, the response status code, another space, a possibly empty reason phrase, a carriage return, and a line feed. 
  **Example**: 
```http
  HTTP/1.1 200 OK
```
- Zero or more response header fields, each consisting of the case-insensitive field name, a colon, optional leading whitespace, the field value, optional trailing whitespace, and ending with a carriage return and a line feed. 
  
  **Example**: 
```http
  Content-Type: text/html
```
- An empty line, consisting of a carriage return and a line feed.
- An optional message body.

##### Response status codes
See also: [List of HTTP status codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes "List of HTTP status codes")
In HTTP responses (since version 1.0), the first line is called the status line, containing a numeric status code (e.g., "404") and a textual reason phrase (e.g., "Not Found"). The status code, a three-digit integer, represents the server's response to the client's request. The client interprets the response primarily based on the status code and secondarily on other header fields. Clients must understand the class of the status code (first digit) and can treat an unrecognized code as equivalent to the x00 status code of that class. The reason phrases are recommendations and can be replaced with "local equivalents" at the developer's discretion. If there's a problem, the user agent might display the reason phrase for user information, but interpreting it is optional, as status codes are machine-readable, and reason phrases are human-readable.

The first digit of the status code defines its class:
**`1XX` (informational)**
The request was received, continuing process.

**`2XX` (successful)**
The request was successfully received, understood, and accepted.

**`3XX` (redirection)**
Further action needs to be taken in order to complete the request.

**`4XX` (client error)**
The request contains bad syntax or cannot be fulfilled.

**`5XX` (server error)**
The server failed to fulfill an apparently valid request.

##### Response header fields

See also: [List of HTTP header fields § Response fields](https://en.wikipedia.org/wiki/List_of_HTTP_header_fields#Response_fields "List of HTTP header fields")

The response header fields allow the server to pass additional information beyond the status line, acting as response modifiers. They give information about the server or about further access to the target resource or related resources.

Each response header field has a defined meaning which can be further refined by the semantics of the request method or response status code.

#### HTTP/1.1 example of request / response transaction
Below is a sample HTTP transaction between an HTTP/1.1 client and an HTTP/1.1 server running on [www.example.com](https://en.wikipedia.org/wiki/Example.com "Example.com"), port 80.
##### Client request
```http
GET / HTTP/1.1
	Host: www.example.com
	User-Agent: Mozilla/5.0
	Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
	Accept-Language: en-GB,en;q=0.5
	Accept-Encoding: gzip, deflate, br
	Connection: keep-alive
```

A client request (consisting in this case of the request line and a few headers that can be reduced to only the `"Host: hostname"` header) is followed by a blank line, so that the request ends with a double end of line, each in the form of a [carriage return](https://en.wikipedia.org/wiki/Carriage_return "Carriage return") followed by a [line feed](https://en.wikipedia.org/wiki/Line_feed "Line feed"). The `"Host: hostname"` header value distinguishes between various [DNS](https://en.wikipedia.org/wiki/Domain_Name_System "Domain Name System") names sharing a single [IP address](https://en.wikipedia.org/wiki/IP_address "IP address"), allowing name-based [virtual hosting](https://en.wikipedia.org/wiki/Virtual_hosting "Virtual hosting"). While optional in HTTP/1.0, it is mandatory in HTTP/1.1. (A "/" (slash) will usually fetch a [/index.html](https://en.wikipedia.org/wiki/Webserver_directory_index "Webserver directory index") file if there is one.)
##### Server response
```http
HTTP/1.1 200 OK
	Date: Mon, 23 May 2005 22:38:34 GMT
	Content-Type: text/html; charset=UTF-8
	Content-Length: 155
	Last-Modified: Wed, 08 Jan 2003 23:11:55 GMT
	Server: Apache/1.3.3.7 (Unix) (Red-Hat/Linux)
	ETag: "3f80f-1b6-3e1cb03b"
	Accept-Ranges: bytes
	Connection: close
```

```html
<html>
  <head>
    <title>An Example Page</title>
  </head>
  <body>
    <p>Hello World, this is a very simple HTML document.</p>
  </body>
</html>
```

The ETag header field checks if a cached resource matches the current server version. "Content-Type" indicates the data type, "Content-Length" its length, and "Accept-Ranges: bytes" allows byte serving. "Connection: close" signals immediate TCP connection closure after the response. 

Content-Length omission in HTTP/1.0 is an error; in HTTP/1.1, it's acceptable with "Transfer-Encoding: chunked." Chunked encoding denotes content end with a 0-sized chunk. Some HTTP/1.0 servers omitted "Content-Length," extending data until socket closure. 

"Content-Encoding: gzip" informs compression by gzip algorithm.
#### Encrypted connections
The most popular way of establishing an encrypted HTTP connection is [HTTPS](https://en.wikipedia.org/wiki/HTTPS "HTTPS"). Two other methods for establishing an encrypted HTTP connection also exist: [Secure Hypertext Transfer Protocol](https://en.wikipedia.org/wiki/Secure_Hypertext_Transfer_Protocol "Secure Hypertext Transfer Protocol"), and using the [HTTP/1.1 Upgrade header](https://en.wikipedia.org/wiki/HTTP/1.1_Upgrade_header "HTTP/1.1 Upgrade header") to specify an upgrade to TLS. Browser support for these two is, however, nearly non-existent.

### [SMTP(Simple mail Transfer Protocol)](https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol)
It is a standard communication protocol for electronic mail transmission.
- It facilitates the exchange of mail messages between mail servers and message transfer agents.
- It is primarily used for sending emails, with user-level email clients using it to relay messages to mail servers.
- Outgoing emails are typically submitted to the mail server on ports 587 or 465 as per RFC 8314.
- For message retrieval, IMAP is standard, but proprietary protocols like Exchange ActiveSync are also common.
- Originating in 1980, SMTP evolved from ARPANET concepts since 1971 and has undergone multiple updates.
- The current version is extensible, incorporating various extensions for authentication, encryption, binary data transfer, and internationalized email addresses.
- SMTP servers commonly operate over TCP on port 25 for plaintext and 587 for encrypted communications.
#### Mail processing model
Email communication involves a sender's mail client (MUA) submitting a message to a mail server (MSA) using SMTP on ports 587 or 25. The MSA delivers the email to a mail transfer agent (MTA), often the same software with different configurations. Processing can be local or split across machines, and MTAs communicate via SMTP.

The boundary MTA uses DNS to find the recipient's MX record, determining the target MTA. Message transfer occurs through hops, with each hop formally handing off responsibility. The final hop hands the message to a mail delivery agent (MDA) for local storage. Retrieval by email clients is done using IMAP or POP protocols.

SMTP defines message transport, not content, covering the mail envelope, while RFC 5322 defines the message (header and body), known as the Internet Message Format.
#### Protocol overview

SSMTP is a connection-oriented, text-based protocol for email communication. An SMTP session involves commands from a client (sender) and responses from a server (receiver). Transactions include the MAIL command for the return address, RCPT for recipients, and DATA to begin message content. Replies can be positive (2xx codes) or negative (4xx, 5xx codes). The client can be an email client (MUA) or a relay server (MTA). Servers use DNS to find recipient servers, and a relay server may connect to an MSA with SMTP Authentication.
##### SMTP vs mail retrieval
SMTP is a delivery protocol for pushing mail to a destination server. It routes mail based on server destinations, not individual users. POP and IMAP are designed for users to retrieve and manage messages. SMTP has a feature for intermittently-connected servers to initiate mail queue processing on a remote server. POP and IMAP are unsuitable for relaying mail by intermittently-connected machines as they operate after final delivery, without critical mail relay information.
##### Remote Message Queue Starting
Remote Message Queue Starting enables a remote host to start processing of the mail queue on a server so it may receive messages destined to it by sending a corresponding command. The original TURN command was deemed insecure and was extended in RFC 1985 with the ETRN command which operates more securely using an authentication method based on Domain Name System information.[19]
#### Outgoing mail SMTP server

An email client needs to know the IP address of its initial SMTP server and this has to be given as part of its configuration (usually given as a DNS name). This server will deliver outgoing messages on behalf of the user.
##### Outgoing mail server access restrictions
Server administrators need to impose some control on which clients can use the server. This enables them to deal with abuse, for example spam. Two solutions have been in common use:

	In the past, many systems imposed usage restrictions by the location of the client, only permitting usage by clients whose IP address is one that the server administrators control. Usage from any other client IP address is disallowed.
	Modern SMTP servers typically offer an alternative system that requires authentication of clients by credentials before allowing access.

###### **Restricting Access by Location:**
- **Method:** ISP's SMTP server restricts access based on the user's IP address, allowing only those within the ISP's network.
- **Implementation:** Firewalling or IP range checks are common techniques to enforce this restriction.
- **Use Case:** Commonly used by corporations and institutions for internal email services.
- **Drawbacks:** Mobile users face issues when connected to different networks, leading to email delivery failures. Changing SMTP server configurations is impractical.
###### **Client Authentication:**
- **Method:** Modern SMTP servers require client authentication using credentials before granting access.
- **Implementation:** SMTP Authentication (SMTP AUTH) is the extension allowing login with an authentication mechanism.
- **Advantages:** Offers flexibility for mobile users, allowing a fixed choice of configured outbound SMTP server.
- **Use Case:** Suited for users who may connect through various ISPs, providing a seamless email experience.
- **Evolution:** Preferred over location-based restrictions due to its adaptability and user-friendly nature.

In summary, while location-based restrictions were common in the past, modern SMTP servers opt for client authentication, providing a more versatile and convenient approach, especially for users on the move.
##### Ports
Communication between mail servers generally uses the standard TCP port 25 designated for SMTP.

Mail clients however generally don't use this, instead using specific "submission" ports. Mail services generally accept email submission from clients on one of:

    587 (Submission), as formalized in RFC 6409 (previously RFC 2476)
    465 This port was deprecated after RFC 2487, until the issue of RFC 8314.

Port 2525 and others may be used by some individual providers, but have never been officially supported.

Many Internet service providers now block all outgoing port 25 traffic from their customers. Mainly as an anti-spam measure,[20] but also to cure for the higher cost they have when leaving it open, perhaps by charging more from the few customers that require it open.

#### SMTP transport example
A typical example of sending a message via SMTP to two mailboxes (alice and theboss) located in the same mail domain (example.com) is reproduced in the following session exchange. (In this example, the conversation parts are prefixed with S: and C:, for server and client, respectively; these labels are not part of the exchange.)

After the message sender (SMTP client) establishes a reliable communications channel to the message receiver (SMTP server), the session is opened with a greeting by the server, usually containing its fully qualified domain name (FQDN), in this case smtp.example.com. The client initiates its dialog by responding with a HELO command identifying itself in the command's parameter with its FQDN (or an address literal if none is available).

```c
S: 220 smtp.example.com ESMTP Postfix
C: HELO relay.example.org
S: 250 Hello relay.example.org, I am glad to meet you
C: MAIL FROM:<bob@example.org>
S: 250 Ok
C: RCPT TO:<alice@example.com>
S: 250 Ok
C: RCPT TO:<theboss@example.com>
S: 250 Ok
C: DATA
S: 354 End data with <CR><LF>.<CR><LF>
C: From: "Bob Example" <bob@example.org>
C: To: "Alice Example" <alice@example.com>
C: Cc: theboss@example.com
C: Date: Tue, 15 Jan 2008 16:02:43 -0500
C: Subject: Test message
C:
C: Hello Alice.
C: This is a test message with 5 header fields and 4 lines in the message body.
C: Your friend,
C: Bob
C: .
S: 250 Ok: queued as 12345
C: QUIT
S: 221 Bye
{The server closes the connection}
```

In the SMTP (Simple Mail Transfer Protocol) communication process, the client informs the server about the sender's email address using the MAIL FROM command. This address is used for bounce notifications if the message can't be delivered. Recipient addresses are specified with the RCPT TO command.

The transmission of the email body begins with the DATA command. The body is sent line by line and terminated with a new-line, a single period, and another new-line. This method, called dot-stuffing, involves sending two periods at the start of a line to represent a single period in the text. The server acknowledges successful commands with a result code and response message (e.g., 250 Ok). A positive reply to the end-of-data indicates the server's responsibility for message delivery. A communication failure at this stage could result in message duplication until the sender receives the 250 Ok reply.

The QUIT command ends the session. If there are additional recipients elsewhere, the client can QUIT and connect to another SMTP server. Information from HELO and MAIL FROM commands becomes additional header fields in the message, such as Received and Return-Path. Some clients close the connection after receiving a 250 Ok, leading to omission of the last two lines. However, this can cause an error on the server when attempting to send the 221 Bye reply.
#### SMTP Extensions
Extension discovery mechanism
Clients learn a server's supported options by using the EHLO greeting, as exemplified below, instead of the original HELO. Clients fall back to HELO only if the server does not support EHLO greeting.
Modern clients may use the ESMTP extension keyword SIZE to query the server for the maximum message size that will be accepted. Older clients and servers may try to transfer excessively sized messages that will be rejected after consuming network resources, including connect time to network links that is paid by the minute.[25]

Users can manually determine in advance the maximum size accepted by ESMTP servers. The client replaces the HELO command with the EHLO command.

```C
S: 220 smtp2.example.com ESMTP Postfix
C: EHLO bob.example.org
S: 250-smtp2.example.com Hello bob.example.org [192.0.2.201]
S: 250-SIZE 14680064
S: 250-PIPELINING
S: 250 HELP
```

Thus smtp2.example.com declares that it can accept a fixed maximum message size no larger than 14,680,064 octets (8-bit bytes).

In the simplest case, an ESMTP server declares a maximum SIZE immediately after receiving an EHLO. According to RFC 1870, however, the numeric parameter to the SIZE extension in the EHLO response is optional. Clients may instead, when issuing a MAIL FROM command, include a numeric estimate of the size of the message they are transferring, so that the server can refuse receipt of overly-large messages.
##### Binary data transfer
Original SMTP supports only a single body of ASCII text, therefore any binary data needs to be encoded as text into that body of the message before transfer, and then decoded by the recipient. Binary-to-text encodings, such as uuencode and BinHex were typically used.

The 8BITMIME command was developed to address this. It was standardized in 1994 as RFC 1652[26] It facilitates the transparent exchange of e-mail messages containing octets outside the seven-bit ASCII character set by encoding them as MIME content parts, typically encoded with Base64.
Mail delivery mechanism extensions
##### On-Demand Mail Relay
On-Demand Mail Relay (ODMR) is an SMTP extension standardized in RFC 2645 that allows an intermittently-connected SMTP server to receive email queued for it when it is connected.
##### Internationalization extension
SMTP originally had limitations with ASCII-only email addresses, posing inconvenience for non-Latin scripts or those using diacritics. Extensions like RFC 6531 introduced the SMTPUTF8 command, allowing multi-byte and non-ASCII characters, addressing issues for scripts like Greek and Chinese.

Current support is limited, but there is strong interest in broad adoption of RFC 6531 and the related RFCs in countries like China that have a large user base where Latin (ASCII) is a foreign script.
##### Extensions
ESMTP, an evolution of SMTP for Internet mail transport, serves as both an inter-server transport and mail submission protocol. ESMTP clients initiate communication with the EHLO command. The server responds with a success (code 250), failure (code 550), or error (code 500, 501, 502, 504, or 421) based on its configuration. A successful ESMTP response (code 250 OK) includes the server's domain and supported extensions. Extensions follow approved formats in subsequent RFCs, registered with IANA. Initial RFC 821 optional services and defined SMTP verbs paved the way for extensions and new parameters in MAIL and RCPT.

Some relatively common keywords (not all of them corresponding to commands) used today are:
    8BITMIME – 8 bit data transmission, RFC 6152
    ATRN – Authenticated TURN for On-Demand Mail Relay, RFC 2645
    AUTH – Authenticated SMTP, RFC 4954
    CHUNKING – Chunking, RFC 3030
    DSN – Delivery status notification, RFC 3461 (See Variable envelope return path)
    ETRN – Extended version of remote message queue starting command TURN, RFC 1985
    HELP – Supply helpful information, RFC 821
    PIPELINING – Command pipelining, RFC 2920
    SIZE – Message size declaration, RFC 1870
    STARTTLS – Transport Layer Security, RFC 3207 (2002)
    SMTPUTF8 – Allow UTF-8 encoding in mailbox names and header fields, RFC 6531
    UTF8SMTP – Allow UTF-8 encoding in mailbox names and header fields, RFC 5336 (deprecated)

The ESMTP format, defined in RFC 5321, supersedes RFC 821 and mandates support for the EHLO command in servers. HELO serves as a required fallback. Non-standard extensions, marked by "X" in EHLO messages, can be used by bilateral agreement. SMTP commands are case-insensitive; specific capitalization requirements violate the standard.
###### 8BITMIME
Servers Advertising 8BITMIME Extension:

	- Apache James (since 2.3.0a1)
	- Citadel (since 7.30)
	- Courier Mail Server
	- Gmail
	- IceWarp
	- IIS SMTP Service
	- Kerio Connect
	- Lotus Domino
	- Microsoft Exchange Server (as of Exchange Server 2000)
	- Novell GroupWise
	- OpenSMTPD
	- Oracle Communications Messaging Server
	- Postfix
	- Sendmail (since 6.57)

Servers Configurable for 8BITMIME, but no Conversion for Non-8BITMIME Relays:
- Exim and qmail do not translate eight-bit messages to seven-bit for non-8BITMIME peers, as required by RFC. However, this doesn't typically cause issues due to widespread 8-bit clean mail relays.
- Microsoft Exchange Server 2003 advertises 8BITMIME by default but results in a bounce when relaying to a non-8BITMIME peer, permitted by RFC 6152 section 3.

###### SMTP-AUTH
SMTP-AUTH is an access control mechanism in SMTP, allowing clients to log into the mail server during mail transmission. It verifies the sender's identity and can be enforced by servers, denying relay service to unauthorized users. While it doesn't guarantee the authenticity of the sender's address, it helps prevent unauthorized relay and spam. Spoofing is still possible unless servers are configured to limit message from-addresses to those authorized for the authenticated user. The extension also enables authenticated relay indications between mail servers, but this is rarely used due to trust considerations on the Internet..
###### SMTPUTF8
Supporting servers include:

    Postfix (version 3.0 and later)[33]
    Momentum (versions 4.1[34] and 3.6.5, and later)
    Sendmail (experimental support in 8.17.1)
    Exim (experimental as of the 4.86 release, quite mature in 4.96)
    CommuniGate Pro as of version 6.2.2[35]
    Courier-MTA as of version 1.0[36]
    Halon as of version 4.0[37]
    Microsoft Exchange Server as of protocol revision 14.0[38]
    Haraka and other servers.[39]
    Oracle Communications Messaging Server as of release 8.0.2.[40]

#### Security extensions
Mail delivery can occur both over plain text and encrypted connections, however the communicating parties might not know in advance of other party's ability to use secure channel.
##### STARTTLS or "Opportunistic TLS"
The STARTTLS extension in SMTP allows servers to signal TLS support to clients, enabling encrypted communication. It's termed opportunistic TLS because security depends on the client opting for encryption. STARTTLS defends against passive observation attacks, but an active attacker can strip STARTTLS commands, termed STRIPTLS.

In 2014, the EFF initiated "STARTTLS Everywhere" for discovering secure communication support. However, as of April 29, 2021, the project closed, recommending DANE and MTA-STS. RFC 8314 declares plain text obsolete, advocating TLS for mail submission and access, including ports with implicit TLS.
###### SMTP MTA Strict Transport Security
RFC 8461 (2018) introduces "SMTP MTA Strict Transport Security (MTA-STS)" to combat active adversaries. It allows mail servers to declare secure channel capabilities through files and DNS TXT records. Relying parties check and cache these records, refraining from insecure channel communication until expiration. MTA-STS applies to SMTP traffic between servers, while user-client and server communication is secured by Transport Layer Security. MTA-STS extends this security policy to third parties.
###### SMTP TLS Reporting
Protocols designed to securely deliver messages can fail due to misconfigurations or deliberate active interference, leading to undelivered messages or delivery over unencrypted or unauthenticated channels. RFC 8460 "**SMTP TLS Reporting**" describes a reporting mechanism and format for sharing statistics and specific information about potential failures with recipient domains. Recipient domains can then use this information to both detect potential attacks and diagnose unintentional misconfigurations.
#### Spoofing and spamming
The original design of SMTP had no facility to authenticate senders, or check that servers were authorized to send on their behalf, with the result that email spoofing is possible, and commonly used in email spam and phishing.

Occasional proposals are made to modify SMTP extensively or replace it completely. One example of this is Internet Mail 2000, but neither it, nor any other has made much headway in the face of the network effect of the huge installed base of classic SMTP. Instead, mail servers now use a range of techniques, such as stricter enforcement of standards such as RFC 5322, DomainKeys Identified Mail, Sender Policy Framework and DMARC, DNSBLs and greylisting to reject or quarantine suspicious emails.


### [Telnet(Teletype network)](https://en.wikipedia.org/wiki/Telnet)
- The main task of the internet is to provide services to users. For example, users want to run different application programs at the remote site and transfers a result to the local site. This requires a client-server program such as FTP, SMTP. But this would not allow us to create a specific program for each demand.
- The better solution is to provide a general client-server program that lets the user access any application program on a remote computer. Therefore, a program that allows a user to log on to a remote computer. A popular client-server program Telnet is used to meet such demands. Telnet is an abbreviation for **Terminal Network**.
- Telnet provides a connection to the remote computer in such a way that a local terminal appears to be at the remote side.

#### There are two types of login:
##### Local Login
 ![Computer Network Telnet](https://static.javatpoint.com/tutorial/computer-network/images/computer-network-telnet.png)

- When a user logs into a local computer, then it is known as local login.
- When the workstation running terminal emulator, the keystrokes entered by the user are accepted by the terminal driver. The terminal driver then passes these characters to the operating system which in turn, invokes the desired application program.
- However, the operating system has special meaning to special characters. For example, in UNIX some combination of characters have special meanings such as control character with "z" means suspend. Such situations do not create any problem as the terminal driver knows the meaning of such characters. But, it can cause the problems in remote login.

##### Remote login
![Computer Network Telnet](https://static.javatpoint.com/tutorial/computer-network/images/computer-network-telnet2.png)

- When the user wants to access an application program on a remote computer, then the user must perform remote login.

##### How remote login occurs
###### At the local site
The user sends the keystrokes to the terminal driver, the characters are then sent to the TELNET client. The TELNET client which in turn, transforms the characters to a universal character set known as network virtual terminal characters and delivers them to the local TCP/IP stack=
###### At the remote site
The commands in NVT forms are transmitted to the TCP/IP at the remote machine. Here, the characters are delivered to the operating system and then pass to the TELNET server. The TELNET server transforms the characters which can be understandable by a remote computer. However, the characters cannot be directly passed to the operating system as a remote operating system does not receive the characters from the TELNET server. Therefore it requires some piece of software that can accept the characters from the TELNET server. The operating system then passes these characters to the appropriate application program.
###### Network Virtual Terminal (NVT)

![Computer Network Telnet](https://static.javatpoint.com/tutorial/computer-network/images/computer-network-telnet3.png)

- The network virtual terminal is an interface that defines how data and commands are sent across the network.
- In today's world, systems are heterogeneous. For example, the operating system accepts a special combination of characters such as end-of-file token running a DOS operating system _ctrl+z_ while the token running a UNIX operating system is _ctrl+d_.
- TELNET solves this issue by defining a universal interface known as network virtual interface.
- The TELNET client translates the characters that come from the local terminal into NVT form and then delivers them to the network. The Telnet server then translates the data from NVT form into a form which can be understandable by a remote computer.





### [SSH(Secure Shell](https://en.wikipedia.org/wiki/Secure_Shell)
SSH (Secure Shell) is a cryptographic network protocol designed for secure operation of network services over unsecured networks, with primary applications in remote login and command-line execution. It follows a client-server architecture, comprising the transport layer for authentication, confidentiality, and integrity; the user authentication protocol for user validation; and the connection protocol for multiplexing encrypted tunnels into logical communication channels.

Developed in 1995 by Tatu Ylönen, SSH replaced insecure protocols like Telnet and unsecured Unix shell protocols (rsh, rlogin, rexec). Its layered protocol suite includes SSH-1 and SSH-2 versions, with OpenSSH being a widely adopted open-source implementation released in 1999 by OpenBSD developers. SSH is platform-agnostic, with implementations available for various operating systems, including embedded systems.

#### Definition
SSH employs public-key cryptography for remote computer authentication, allowing mutual user authentication. In one method, both ends use auto-generated public-private key pairs for encryption, supplemented by a password for user authentication.

When users manually generate key pairs, authentication occurs during key creation, enabling passwordless sessions. The public key is distributed to authorized computers, and while authentication relies on the private key, it is never transmitted over the network. SSH ensures the presenting public key corresponds to the owner's private key.

Verification of unknown public keys is crucial in all SSH versions. Accepting unvalidated public keys may authorize attackers, emphasizing the need to associate public keys with identities before acceptance.

#### Authentication: OpenSSH key management
On Unix-like systems, authorized public keys are typically stored in `~/.ssh/authorized_keys`. This file must be non-writable by others for SSH to recognize it. Passwords are bypassed when a matching private key is present. Private keys can have an optional passphrase for added security. The private key can be specified using `-i` in the SSH command. `ssh-keygen` creates key pairs. SSH also supports password-based authentication with auto-generated keys, but this poses a risk of man-in-the-middle attacks for the initial connection. Password authentication can be disabled on the server for enhanced security.
#### Use
SSH is a versatile protocol used for remote access, command execution, tunneling, and file transfer. It operates on a client-server model and is present on various operating systems, including macOS, Linux, OpenBSD, FreeBSD, NetBSD, Solaris, and OpenVMS. Notably absent from Windows until version 1709, SSH is available through proprietary, freeware, and open-source clients like PuTTY and OpenSSH. File managers like Konqueror and WinSCP provide graphical interfaces, while the Chrome browser supports SSH connections. In cloud computing, SSH addresses connectivity and security issues, allowing secure paths over the Internet to virtual machines. IANA has designated TCP, UDP, and SCTP port 22 for SSH. Originally a TCP-based protocol, SSH can also run on SCTP.
#### Uses
<img src=https://upload.wikimedia.org/wikipedia/commons/0/03/X11_ssh_tunnelling.png alt="Example of tunneling an X11 application over SSH: the user 'josh' has 'SSHed' from the local machine 'foofighter' to the remote machine 'tengwar' to run xeyes.">

<img src=https://upload.wikimedia.org/wikipedia/commons/1/1e/OpenWrtPuTTY.png alt="Logging into OpenWrt via SSH using PuTTY running on Windows.">
SSH is a versatile protocol used across various platforms, including Unix variants (Linux, BSDs, macOS, Solaris) and Windows. It serves multiple purposes:

1. **Remote Shell Access:**
   - Login to a remote shell (replacing Telnet and rlogin).
   - Execute a single command on a remote host (replacing rsh).

2. **Authentication and Security:**
   - Enable automatic, passwordless login to a remote server (e.g., using OpenSSH).
   - Securely transfer, copy, and mirror files using rsync.

3. **Network Operations:**
   - Port forwarding for network services.
   - Tunneling for secure data transfer.

4. **VPN and Proxy:**
   - Implement a full-fledged encrypted VPN (OpenSSH server/client).
   - Browse the web through an encrypted proxy connection using SSH with SOCKS support.

5. **X Window System and File Systems:**
   - Forward X from a remote host.
   - Mount a remote server directory as a local filesystem using SSHFS.

6. **Management and Development:**
   - Automate remote monitoring and server management.
   - Facilitate development on mobile or embedded devices supporting SSH.
   
1. **Security Measures:**
   - Secure file transfer protocols.

SSH's flexibility makes it a fundamental tool for secure communication, remote access, and network operations across diverse computing environments.
##### File transfer protocols
The Secure Shell protocols are used in several file transfer mechanisms.
- [Secure copy](https://en.wikipedia.org/wiki/Secure_copy "Secure copy") (SCP), which evolved from [RCP](https://en.wikipedia.org/wiki/Rcp_(Unix) "Rcp (Unix)") protocol over SSH
- [rsync](https://en.wikipedia.org/wiki/Rsync "Rsync"), intended to be more efficient than SCP. Generally runs over an SSH connection.
- [SSH File Transfer Protocol](https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol "SSH File Transfer Protocol") (SFTP), a secure alternative to [FTP](https://en.wikipedia.org/wiki/File_Transfer_Protocol "File Transfer Protocol") (not to be confused with [FTP over SSH](https://en.wikipedia.org/wiki/FTP_over_SSH "FTP over SSH") or [FTPS](https://en.wikipedia.org/wiki/FTPS "FTPS"))
- [Files transferred over shell protocol](https://en.wikipedia.org/wiki/Files_transferred_over_shell_protocol "Files transferred over shell protocol") (FISH), released in 1998, which evolved from [Unix shell](https://en.wikipedia.org/wiki/Unix_shell "Unix shell") commands over SSH
- [Fast and Secure Protocol](https://en.wikipedia.org/wiki/Fast_and_Secure_Protocol "Fast and Secure Protocol") (FASP), aka _Aspera_, uses SSH for control and UDP ports for data transfer.

#### Architecture
<img src=https://upload.wikimedia.org/wikipedia/commons/0/0f/Ssh_binary_packet_alt.svg style="width: 1000px;background-color;" alt="Diagram of the SSH-2 binary packet.">
The SSH protocol features a layered architecture with three components:

1. **Transport Layer (RFC 4253):**
   - Uses TCP on port 22 for initial key exchange, server authentication, encryption, compression, and integrity verification.
   - Handles key re-exchange and packet size up to 32,768 bytes.
   - Enables secure data transfer with optional re-exchange after 1 GB or one hour.

2. **User Authentication Layer (RFC 4252):**
   - Client-driven authentication with methods like password, public key, keyboard-interactive, and GSSAPI.
   - Supports password changes and diverse public-key algorithms.
   - Enables versatile authentication, including one-time passwords and external mechanisms like Kerberos.

3. **Connection Layer (RFC 4254):**
   - Introduces channels, channel requests, and global requests.
   - Allows multiplexing of bidirectional data on multiple logical channels.
   - Defines standard channel types for terminal shells, forwarded connections, and more.

Additional point:
- The SSHFP DNS record (RFC 4255) provides host key fingerprints for host authenticity verification.

The open architecture provides flexibility, comparable to TLS in transport, extensibility in user authentication, and multiplexing capabilities for various SSH services.

#### Algorithms
- [EdDSA](https://en.wikipedia.org/wiki/EdDSA "EdDSA"), [ECDSA](https://en.wikipedia.org/wiki/ECDSA "ECDSA"), [RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem) "RSA (cryptosystem)") and [DSA](https://en.wikipedia.org/wiki/Digital_Signature_Algorithm "Digital Signature Algorithm") for [public-key cryptography](https://en.wikipedia.org/wiki/Public-key_cryptography "Public-key cryptography").
- [ECDH](https://en.wikipedia.org/wiki/ECDH "ECDH") and [Diffie–Hellman](https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman "Diffie–Hellman") for [key exchange](https://en.wikipedia.org/wiki/Key_exchange "Key exchange").
- [HMAC](https://en.wikipedia.org/wiki/HMAC "HMAC"), [AEAD](https://en.wikipedia.org/wiki/AEAD "AEAD") and [UMAC](https://en.wikipedia.org/wiki/UMAC "UMAC") for [MAC](https://en.wikipedia.org/wiki/Message_authentication_code "Message authentication code").
- [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard "Advanced Encryption Standard") (and deprecated [RC4](https://en.wikipedia.org/wiki/RC4 "RC4"), [3DES](https://en.wikipedia.org/wiki/3DES "3DES"), [DES](https://en.wikipedia.org/wiki/Data_Encryption_Standard "Data Encryption Standard") for [symmetric encryption](https://en.wikipedia.org/wiki/Symmetric_encryption "Symmetric encryption").
- [AES-GCM](https://en.wikipedia.org/wiki/AES-GCM "AES-GCM") and [ChaCha20-Poly1305](https://en.wikipedia.org/wiki/ChaCha20-Poly1305 "ChaCha20-Poly1305") for [AEAD](https://en.wikipedia.org/wiki/AEAD "AEAD") encryption.
- [SHA](https://en.wikipedia.org/wiki/Secure_Hash_Algorithm "Secure Hash Algorithm") (and deprecated [MD5](https://en.wikipedia.org/wiki/MD5 "MD5")) for [key fingerprint](https://en.wikipedia.org/wiki/Key_fingerprint "Key fingerprint").

#### Vulnerabilities
##### SSH-1
In 1998, a vulnerability in SSH 1.5 allowed unauthorized content insertion due to weak data integrity protection using CRC-32. The SSH Compensation Attack Detector was introduced, but subsequent fixes led to an integer overflow flaw, enabling arbitrary code execution. In January 2001, vulnerabilities emerged allowing modification of the last block in an IDEA-encrypted session and malicious server forwarding of client authentication. SSH-1 is considered obsolete due to inherent design flaws, prompting explicit disabling of fallback to SSH-1. Most servers and clients now support the more secure SSH-2.
##### CBC plaintext recovery
In November 2008, a theoretical vulnerability was discovered for all versions of SSH which allowed recovery of up to 32 bits of plaintext from a block of ciphertext that was encrypted using what was then the standard default encryption mode, [CBC](https://en.wikipedia.org/wiki/Block_cipher_modes_of_operation#Cipher-block_chaining_(CBC) "Block cipher modes of operation"). The most straightforward solution is to use [CTR](https://en.wikipedia.org/wiki/Block_cipher_modes_of_operation#Counter_(CTR) "Block cipher modes of operation"), counter mode, instead of CBC mode, since this renders SSH resistant to the attack.
##### Suspected decryption by NSA
On December 28, 2014 _[Der Spiegel](https://en.wikipedia.org/wiki/Der_Spiegel "Der Spiegel")_ published classified information leaked by whistleblower [Edward Snowden](https://en.wikipedia.org/wiki/Edward_Snowden "Edward Snowden") which suggests that the [National Security Agency](https://en.wikipedia.org/wiki/National_Security_Agency "National Security Agency") may be able to decrypt some SSH traffic. The technical details associated with such a process were not disclosed. A 2017 analysis of the [CIA](https://en.wikipedia.org/wiki/CIA "CIA") hacking tools _BothanSpy_ and _Gyrfalcon_ suggested that the SSH protocol was not compromised.
##### Terrapin attack
In 2023, a novel man-in-the-middle attack, named the Terrapin attack, was discovered against most SSH implementations. The risk is mitigated by the need to intercept a genuine SSH session, resulting mainly in failed connections. The attack's major impact is the degradation of keystroke timing obfuscation in SSH. OpenSSH 9.6 fixed the vulnerability, requiring both client and server upgrades for full effectiveness.
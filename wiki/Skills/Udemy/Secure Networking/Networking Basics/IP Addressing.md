### What is IP Address
It is a numerical label assigned to each device participating in a [computer network](https://en.wikipedia.org/wiki/Computer_network "Computer network") that uses the [Internet Protocol](https://en.wikipedia.org/wiki/Internet_Protocol "Internet Protocol") for communication. It serves two main purposes: host or network interface identification and location addressing. IP addresses enable devices to communicate with each other within a network or over the internet.

IP addresses are fundamental for routing data packets across networks, allowing devices to send and receive information on the internet. They are essential for identifying and locating devices in a networked environment.
### IPv4 vs IPv6
| Feature         | IPv4                          | IPv6                               |
|-----------------|-------------------------------|------------------------------------|
| **Address Length** | 32 bits                     | 128 bits                           |
| **Notation**      | Dotted Decimal (e.g., 192.168.1.1) | Hexadecimal separated by colons (e.g., 2001:0db8:85a3:0000:0000:8a2e:0370:7334) |
| **Addresses**      | Limited (4.3 billion)         | Vast (3.4 × 10^38)                  |
| **Address Configuration** | Manual or DHCP             | Stateless Address Autoconfiguration (SLAAC), DHCPv6 |
| **Subnetting**    | Commonly used               | Simplified and often not required  |
| **Broadcast**     | Supported                   | Not used; replaced by multicast    |
| **Network Address Translation (NAT)** | Commonly used       | Generally not required             |
| **Header Length** | Fixed (20 bytes)             | Fixed (40 bytes)                   |
| **Checksum**      | Included                    | Removed                            |
| **ARP (Address Resolution Protocol)** | Required             | Not used; replaced by ICMPv6 Neighbor Discovery |
| **QoS (Quality of Service)** | Limited support       | Improved support                   |
| **Security Features** | Generally less secure    | Enhanced security features, IPsec support |
| **Deployment**    | Widely deployed              | Increasing adoption, coexisting with IPv4 |
### IP Address Classes
These are traditionally divided into five classes (A, B, C, D, and E), with each class having a different range of addresses for different purposes. However, due to the adoption of Classless Inter-Domain Routing (CIDR), the strict boundaries between classes are not as significant today. CIDR allows for more flexible allocation of IP addresses. Nevertheless, it's still helpful to understand the historical IP address classes:

1. **Class A (1.0.0.0 to 126.255.255.255):**
   - First octet is the network portion.
   - Supports a large number of hosts.
   - Example: 10.0.0.1

2. **Class B (128.0.0.0 to 191.255.255.255):**
   - First two octets are the network portion.
   - Suitable for medium-sized networks.
   - Example: 172.16.0.1

3. **Class C (192.0.0.0 to 223.255.255.255):**
   - First three octets are the network portion.
   - Suitable for small networks.
   - Example: 192.168.0.1

4. **Class D (224.0.0.0 to 239.255.255.255):**
   - Reserved for multicast groups.
   - Not used for traditional unicast communication.
   - Example: 233.0.0.1

5. **Class E (240.0.0.0 to 255.255.255.255):**
   - Reserved for experimental purposes.
   - Not used for standard IP communication.

CIDR has largely replaced the strict class-based system, allowing more efficient allocation of IP addresses by using variable-length subnet masks. This flexibility is crucial for accommodating the growth of the internet and optimizing address space utilization.
### Private Address Space
These are reserved ranges of IP addresses that are not routable on the public Internet. They are commonly used within private networks, like those within homes, businesses, or organizations. Private addresses are essential for internal communication, allowing devices to connect, communicate, and share data within a specific network while keeping these communications isolated from the global Internet.

The three commonly used private address ranges specified in RFC 1918 are:

1. **Class A Private Addresses:**
   - Range: 10.0.0.0 to 10.255.255.255
   - Subnet: 10.0.0.0/8
   - Example: 10.1.1.1

2. **Class B Private Addresses:**
   - Range: 172.16.0.0 to 172.31.255.255
   - Subnet: 172.16.0.0/12
   - Example: 172.16.1.1

3. **Class C Private Addresses:**
   - Range: 192.168.0.0 to 192.168.255.255
   - Subnet: 192.168.0.0/16
   - Example: 192.168.1.1

Devices within a private network use these addresses, and routers or firewalls in the network perform Network Address Translation (NAT) when these devices communicate with the outside world. NAT helps conceal the private addresses and allows multiple devices within the private network to share a single public IP address when accessing the Internet.

Using private address spaces helps mitigate the depletion of public IPv4 addresses, as it allows many devices to share a smaller set of publicly routable IP addresses. Additionally, with the adoption of IPv6, which has a vastly larger address space, the need for private address spaces may decrease in the long run.
### Default Subnet Masks
Default subnet masks are predefined values that accompany specific IP address classes, determining the default network size. These masks help in dividing an IP address into network and host portions. Here are the default subnet masks for each IP address class:

1. **Class A:**
   - Default Subnet Mask: 255.0.0.0
   - Example IP Range: 1.0.0.1 to 126.255.255.254

2. **Class B:**
   - Default Subnet Mask: 255.255.0.0
   - Example IP Range: 128.1.0.1 to 191.254.255.254

3. **Class C:**
   - Default Subnet Mask: 255.255.255.0
   - Example IP Range: 192.0.1.1 to 223.255.254.254

4. **Class D and E:**
   - Class D (multicast) and Class E (experimental) addresses do not use default subnet masks as they are not typically used for traditional unicast communication.

These default subnet masks are a part of the traditional Classful IP addressing system. However, in modern networking, Classless Inter-Domain Routing (CIDR) is commonly used, allowing for more flexible subnetting and not strictly following the class-based subnetting rules. In CIDR notation, the subnet mask is expressed as a prefix length, indicating the number of bits set to 1 in the mask. For example, instead of using the default subnet mask 255.255.255.0, CIDR notation might represent it as /24.
### IPv4 Address Types:

#### Unicast Address: (one-to-one)
- **Definition:** Unicast addresses identify a single network interface among a group of devices. 
- **Example:** 192.168.1.1
- **Usage:** Used for one-to-one communication, where data is sent from one sender to one receiver.
![imgbb](https://i.ibb.co/bv8zvwt/image.png)

#### Broadcast Address: (one-to-one)
- **Definition:** Broadcast addresses are used to send data to all devices in a specific network.
- **Example:** 192.168.1.255 (for a subnet 192.168.1.0/24)
- **Usage:** Historically used for one-to-all communication within a network, but less common due to security considerations.
![imgbb](https://i.ibb.co/1f4d0Tn/image.png)

#### Multicast Addresses: (one-to-several)
- **Definition:** Multicast addresses identify a group of devices interested in receiving the same data.
- **Example:** 224.0.0.1 (IPv4 All Hosts multicast group)
- **Usage:** Efficient for one-to-many or many-to-many communication, where data is sent to a selected group of recipients. Commonly used in multimedia streaming and certain network protocols.
![imgbb](https://i.ibb.co/BKW6WcQ/image.png)

These address types play crucial roles in defining the scope and purpose of communication within IPv4 networks. Unicast addresses are for point-to-point communication, broadcast addresses are for one-to-all communication within a network, and multicast addresses facilitate efficient one-to-many or many-to-many communication within specific groups.

### Showing concepts at use
![imgbb](https://i.ibb.co/gwjH6f9/Screenshot-from-2024-01-29-06-07-41.png)
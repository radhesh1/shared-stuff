## Modules
1. Do the following:

   ```sh
   cargo new modules
   cd modules
   emacs src/main.rs
   ```

2. Write the following code in `src/main.rs`:

   ```rust fold title:modules
	mod player; //player.rs as module

	fn main() {
		player::play_movie("play.mp4");
		//player::play_audio("play.mp3"); // Won't work
		clean::perform_clean(); // calling the module function
		clean::files::clean_files(); // calling submodule function
	}

	mod clean {
		pub fn perform_clean() {
			println!("CLeaning HDD");
		}
		pub mod files {
			pub fn clean_files() {
				println!("Removing Used files");
			}
		}
	}
   ```
3. Write the following code in `player.rs`:
	```rust fold title:module_code
	// pub keyword makes function publicly accessible
	pub fn play_movie(name: &str){
		println!("Playing Movie {}",name);
	}

	fn play_audio(name: &str){
		println!("Playing audio {}",name);
	}
	```
4. Execute it by:

   ```sh
   cargo run
   ```

## Crates
1. Do the following:

   ```sh
   cargo new crates
   cd crates
   emacs src/main.rs
   ```

2. Write the following code in `src/main.rs`:

   ```rust fold title:crates
   use rand::Rng;
   use crate::archive::arch::arch_file as arc;
   mod archive;

   fn main() {
      arc("file.txt");
      let mut rng = rand::thread_rng();
      let a: i32  = rng.gen();
      println!("{}",a);
   }
   ```
3. Write the following code in `src/archive.rs`:
   ```rust fold title:custom_crate
   pub mod arch {
      pub fn arch_file(name: &str){
         println!("Archiving file {}",name);
      }
   }
   ```
4. Execute it by:

   ```sh
   cargo run
   ```

## Generating random numbers
1. Do the following:

   ```sh
   cargo new randn
   cd randn
   emacs src/main.rs
   ```

2. Write the following code in `src/main.rs`:

   ```rust fold title:random_number_gen
	use rand::{Rng, thread_rng};
	use rand::distributions::Alphanumeric; //alphanumeric

	fn main() {
		let mut rng = rand::thread_rng(); // used to thread-local random number generator
		let i:i32 = rng.gen(); // generate string
		print!("{}\n",i);

		println!("Bounded int: {}", rng.gen_range(8,100)); // bounded integer range
		println!("Bounded float: {}", rng.gen_range(0.8,100.0)); // bounded float range

		let rand_string: String = thread_rng().sample_iter(&Alphanumeric).take(30).collect(); // Random String
		println!("Gen string is {}",rand_string);
	}
   ```

3. Execute it by:

   ```sh
   cargo run
   ```

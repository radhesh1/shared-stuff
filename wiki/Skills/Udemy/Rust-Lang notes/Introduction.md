---
dg-publish: true
---
## Install Rust

1. Go to [[https://www.rust-lang.org/|Rust Site]].
    
2. Run the following:
    
    - Linux: Open terminal and execute:
        
        ```sh title:installing_rust
        curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
        ```
        
    - Windows: Download [[https://static.rust-lang.org/rustup/dist/i686-pc-windows-gnu/rustup-init.exe|Rust for Windows]].
3. Create a new project:
    
    ```sh
    cargo new hello_world
    cd hello_world
    emacs src/main.rs
    ```
    
4. Enter the following code in `src/main.rs`:
    
    ```rust title:hello_world
    fn main() {
        println!("Hello, World");
    }
    ```
    
5. Execute it by:
    
    ```sh
    cargo run
    ```
    

## Cargo Package Manager

|Command|Description|
|---|---|
|`cargo new`|Creates a new Rust project|
|`cargo build`|Compiles the Rust project|
|`cargo run`|Runs the compiled Rust project|
|`cargo clean`|Cleans up built artifacts|
|`cargo check`|Checks code without making an executable|
|`cargo doc`|Generates documentation|

## User Input

1. Create a new project:
    
    ```sh 
    cargo new user_input
    cd user_input
    emacs src/main.rs
    ```
    
2. Write the following code in `src/main.rs`:
    
    ```rust title:taking_input
    use std::io;
    
    fn main() {
        let mut input = String::new();
        println!("Say Something: ");
        match io::stdin().read_line(&mut input) {
            Ok(_) => {}
            Err(e) => {
                println!("Something went wrong: {}", e);
            }
        }
    }
    ```
    
3. Execute it by:
    
    ```sh
    cargo run
    ```
    

## Commenting

1. Create a new project:
    
    ```sh
    cargo new commenting
    cd commenting
    emacs src/main.rs
    ```
    
2. Write the following code in `src/main.rs`:
    
    ```rust title:commenting
    use std::io;
    
    /// Crate Comment.
    /// What is this Module trying to do?
    
    fn main() {
        //! # Main Function
        //!
        //! ```rust
        //! fn main()
        //! ```
        //!
        //! Reads User input and prints it to the console
    
        let mut input = String::new();
        // Print a message to the user
        println!("Say Something: ");
        /*
        Takes an input and
        Acts accordingly
        */
        match io::stdin().read_line(&mut input) {
            Ok(_) => {}
            Err(e) => {
                println!("Something went wrong: {}", e);
            }
        }
    }
    ```
    
3. Execute it by:
    
    ```sh
    cargo run
    ```
    

## Printing Values

1. Create a new project:
    
    ```sh
    cargo new printing_values
    cd printing_values
    emacs src/main.rs
    ```
    
2. Write the following code in `src/main.rs`:
    
    ```rust title:printing
    fn main() {
    	println!("Hello, world!");
    	println!("My name is {} and I'm {} years old", "Alex", 29);
    	println!("a + b =  {}", 3 + 9);
    	println!("{0} has a {2} and  {0} has a {1}", "Alex", "Cat", "Dog");
    	println!("{name} {surname} ", surname = "Smith", name = "Alex");
    	println!("binary: {:b},hex: {:x},octal: {:o} ", 50, 50, 50);
    	println!("Array {:?}",[1, 2, 3]);
    }
    ```
    
3. Execute it by:
    
    ```sh
    cargo run
    ```
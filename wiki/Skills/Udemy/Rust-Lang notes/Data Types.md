## Arrays
Array rules
* Static - cannot be resized ( stack allocated )
* Elements can be modified but not deleted
* Indexed

1. Do the following:

   ```sh
   cargo new arrays
   cd arrays
   emacs src/main.rs
   ```

2. Write the following code in `src/main.rs`:

   ```rust fold title:testing_arrays
   #[allow(unused_variables)]
   #[allow(unused_assignments)]

   fn main() {
      let primes = [2,3,5,7,11];
      let doubles: [f64; 4] = [2.0, 4.0, 6.0, 8.0];

      // Array with default values
      let mut numbers = [0; 15];
      println!("{:?}",numbers);

      // Using Default value as a static
      const DEFAULT: i32 = 3;
      let numbers1 = [DEFAULT; 15];
      println!("{:?}",numbers1);

      // iterin `for` loop
      for number in numbers1.iter(){
         println!("{}",number);
      }
   }
   ```

3. Execute it by:

   ```sh
   cargo run
   ```

## Vectors
Vectors are Dynamic Array 

Vector rules
* Dynamic - can be resized ( heap allocated )
* Indexed

1. Do the following:

   ```sh
   cargo new vectors
   cd vectors
   emacs src/main.rs
   ```

2. Write the following code in `src/main.rs`:

   ```rust fold title:testing_vectors
   #[allow(unused_variables)]

   fn main() {
      let primes1: Vec<i32> = Vec::new();
      let mut primes: Vec<i32> = vec![2,3,5];
      println!("{:?}",primes);

      // Addition and removal on a mutable vector
      primes.push(7);
      println!("{:?}",primes);
      primes.remove(0);
      println!("{:?}",primes);

      // Vector with Default values
      let numbers = vec![2; 10];
      println!("{:?}",numbers);

      const DEFAULT:i32 = 6;
      let mut values = [DEFAULT; 8];
      println!("{:?}",values);

      // Adding number at a index
      values[2] = 9;
      println!("{:?}",values);

      // Using `for` loop
      for number in values.iter(){
         println!("{}",number);
      }
   }
   ```

3. Execute it by:

   ```sh
   cargo run
   ```

## Slices
Slice is pointer to a block of memory

Slice rules
* size determined on runtime
* can be used on arrays, vectors and strings
* Indexed
- Mutable slices can change values

1. Do the following:

   ```sh
   cargo new slices
   cd slices
   emacs src/main.rs
   ```

2. Write the following code in `src/main.rs`:

   ```rust fold title:testing_slices
   #[allow(unused_variables)]

   fn main() {
      let primes1: Vec<i32> = Vec::new();
      let mut primes: Vec<i32> = vec![2,3,5];
      println!("{:?}",primes);

      // Addition and removal on a mutable vector
      primes.push(7);
      println!("{:?}",primes);
      primes.remove(0);
      println!("{:?}",primes);

      // Vector with Default values
      let numbers = vec![2; 10];
      println!("{:?}",numbers);

      const DEFAULT:i32 = 6;
      let mut values = [DEFAULT; 8];
      println!("{:?}",values);

      // Adding number at a index
      values[2] = 9;
      println!("{:?}",values);

      // Using `for` loop
      for number in values.iter(){
         println!("{}",number);
      }
   }
   ```
3. Execute it by:

   ```sh
   cargo run
   ```

## Tuples
## Structure
## Enums
## Generics
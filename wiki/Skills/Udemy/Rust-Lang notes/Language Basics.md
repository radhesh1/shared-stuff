
Rust is a strongly typed language with explicit data type definitions, ensuring type safety to prevent type-related errors.

## Variables
1. Create a new project:
   ```sh
   cargo new variables
   cd variables
   emacs src/main.rs
   ```

2. Write the following code in `src/main.rs`:
   ```rust fold title:rust_variables
	#[allow(unused_variables)]
	#[allow(unused_assignments)]
	fn main() {
		// Demonstration of variable types
		let name = "Alex";
		let age = 32;
		let amount:i64 = 8473926758472;

		// How to reassign variables
		let mut age1 =42;
		println!("age is {}",age1);
		age1 = 43;
		println!("age is {}",age1);

		// Shadowing Variables
		let color = "blue";
		let color = 86;
		println!("color is {}", color);

		// Assigning multiple values simultaneously
		let (a,b,c) = (46,85,"red");
	}
   ```

3. Execute it by:
   ```sh
   cargo run
   ```

## Scalar Data Types
| Data Type        | Size     | Signed      | Unsigned        |
|------------------|----------|-------------|-----------------|
| **Integer** | 
| 8-bit integer    |8 bit     | `i8`        |`u8`             |
| 16-bit integer   |16 bit    | `i16`       |`u16`            |
| __32-bit integer__|__32 bit__| __`i32`__  |`u32`            |
| 64-bit integer   |64 bit    | `i64`       |`u64`            |
| 128-bit integer  |128 bit   | `i128`      |`u128`           |
| integer          |arch      | `isize`     |`usize`          |
| **Float** | 
| 32-bit float     |32 bit    | `f32`       | -               |
| __64-bit float__ |__64 bit__| __`f64`__   | -               |
| **Character and bool** | 
| Single Unicode character|32 bit    | -    |`char`           |
| Boolean (true/false) |8 bit        | `bool`| -              |

1. Create a new project:
   ```sh
   cargo new scalar_data_types
   cd scalar_data_types
   emacs src/main.rs
   ```

2. Write the following code in `src/main.rs`:
   ```rust fold title:data_types
	#[allow(unused_variables)]
	#[allow(unused_assignments)]
	fn main(){
		//Different Data types
		let pi: f32 = 4.0;
		let million = 1_000_000;
		println!("{}",million);

		// Boolean Data type
		let is_day = true;
		let is_night = false;
		println!("{}",is_day);

		// Character and emoji Data type
		let char1 = "A";
	

	let smiley_face = '\u{1F601}'; // 😁
		println!("{0} is very {1}",char1,smiley_face);
	}
   ```

3. Execute it by:
   ```sh
   cargo run
   ```

## String
1. Create a new project:
   ```sh
   cargo new string
   cd string
   emacs src/main.rs
   ```

2. Write the following code in `src/main.rs`:
   ```rust fold title:strings
	#[allow(unused_variables)]
	#[allow(unused_mut)]
	fn main() {
		//String slices
		let cat="Fluffy";
		let cat : &'static str="Fluffy";
		println!("{}",cat); // string slices are immutable

		// String Objects
		let dog = String::new(); // Directly
		let mut dog = String::from("Max"); // using String slices
		println!("{}",dog) ;

		let owner = format!("Hi I'm {} the owner of {}","Mark",dog); //usage of format macro
		println!("{} whose length is {}00cm",owner, dog.len()); //use of string.len() to find its length

		// adding to the existing string (it should be mutable)
		dog.push(' '); //scalar data types
		dog.push_str("the dog"); //String data type
		println!("{}",dog);

		// Replace part of a string
		let new_dog = dog.replace("the","is the");
		println!("{}",new_dog);

		//Usage of split
		let mut cat = String::from("Wiz,Woz,Piz");
		let cats: Vec<&str> = cat.split(',').collect(); // Splitting using delimiter
		let max_parts: Vec<&str> = dog.split_whitespace().collect(); // Splitting using Whitespaces
		println!("cats are {:?} and max parts are {:?}",cats,max_parts);

		// Usage of chars
		for c in dog.chars(){
			println!("{}",c);
		}
	}
   ```

3. Execute it by:
   ```sh
   cargo run
   ```

## Constant
```rust fold title:CONST
const URL:&str = "google.com"; // need to specify data type and can be used as a global variable

fn main() {
    println!("{}",URL);
}
```
Execute it by:
```sh
cargo run
```

## Operators
| Operator      | Description                | Example                    |
|---------------|----------------------------|----------------------------|
| **Arithmetic**|                            |                            |
| `+`           | Addition                   | 10 + 3 = 13                |
| `-`           | Subtraction                | 10 - 3 = 7                 |
| `*`           | Multiplication             | 10 * 3 =30                 |
| `/`           | Division                   | 10 / 3 = 3                 |
| `%`           | Remainder (Modulus)        | 10 % 3 = 1                 |
| **Logical**   |                            |                            |
| `&&`          | Logical AND                | true && true = true        |
| `\|\|`        | Logical OR                 | true \|\| false = true     |
| `!`           | Logical NOT                | !true = false			  |
| **Relational**|                            |                            |
| `==`          | Equal to                   | 1 == 3 = false             |
| `!=`          | Not equal to               | 1 != 3 = true              |
| `<`           | Less than                  | 3 < 1 = false              |
| `>`           | Greater than               | 2 > 1 = true               |
| `<=`          | Less than or equal to      | 3 <= 1 = false             |
| `>=`          | Greater than or equal to   | 2 >= 1 = true              |
| **Bitwise**   |                            |                            |
| `&`           | Bitwise AND                | 10 & 3 = 2                 |   
| `\|`           | Bitwise OR                | 10 \| 3 = 11               |
| `^`           | Bitwise XOR                | 10 ^ 3 = 9                 |
| `<<`          | Left shift                 | 10 << 3 = 40               |
| `>>`          | Right shift                | 10 >> 3 = 1                |
| **Assignment**                                                             |
| `=`           | Assignment                 | x = 5                      |
| `+=`          | Addition assignment        | x += 5                     |
| `-=`          | Subtraction assignment     | x -= 5                     |
| `*=`          | Multiplication assignment  | x *= 5                     |
| `/=`          | Division assignment        |x /= 5                     |
| `%=`          | Remainder assignment       | x %= 5                     |
| `&=`          | Bitwise AND assignment     | x &= 5                     |
| `\|=`         | Bitwise OR assignment      | x \|= 5                    |
| `^=`          | Bitwise XOR assignment     | x ^= 5                     |
| `<<=`         | Left shift assignment      | x <<= 5                    |
| `>>=`         | Right shift assignment     | x >>= 5                    |

1. Create a new project:
   ```sh
   cargo new operators
   cd operators
   emacs src/main.rs
   ```

2. Write the following code in `src/main.rs`:
   ```rust fold title:operators
   	​fn main() {
		// Arithmetic Operators
		let a = 4 + 8;
		let b = 10 / 3;
		let c = 10 % 3;
		println!("a={} b={} c={}",a,b,c);

		// Relational Operators
		println!("{}",a >= b);

		// Logical Operators
		println!("{}", a>=b && b>=c);

		// Bitwise Operators
		println!("{}",a^b);
	}
   ```

3. Execute it by:
   ```sh
   cargo run
   ```

## Functions
1. Create a new project:
   ```sh
   cargo new functions
   cd functions
   emacs src/main.rs
   ```

2. Write the following code in `src/main.rs`:
	```rust fold title:functions
	fn main() {
			// Looping a function
			for i in 1..3{
				say_hi(); // function call
			}
			let mut name = "John";
			say_hello(&mut name); // function called with a input
			println!("The new name is {}", name);
			let bye = say_bye(&mut name); // using a function to give input
			println!("{}",bye);
		}
	
		// basic function
		fn say_hi() {
			println!("Hello, there");
		}
	
		// mutable input to a function
		fn say_hello(name: &mut &str) { // function taking a mutable input
			println!("Hello {}", name);
			*name = "Alex"; // variable changing
		}
	
		// function with a return value
		fn say_bye(name: &mut &str) -> String{
			let greeting = format!("Bye {}", name);
			return greeting; //or greeting
		}
	```
3. 3. Execute it by:

   ```sh
   cargo run
   ```

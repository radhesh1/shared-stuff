# Skills
[[Skills/index.md]]

# Assignments
## COE
1. [[Assignments/COE/Probability/index.md]]
2. [[Assignments/COE/DBMS/index.md]]
3. [[Assignments/COE/AI/index.md]]
4. [[Assignments/COE/DAA/index.md]]

## ENC
1. [[Assignments/ENC/DSA/index.md]]
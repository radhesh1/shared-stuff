# Assignment 3
### Q1.	Write a function to perform following operations on the string:
> (Note: You can make single function for all operations/independent function for each problem)

i. Finding length of a string<br>
ii. Converting a string in lowercase<br>
iii. Counting number of words and vowels in a string<br>
iv. Validating a string (Note: Valid string does contain only alphabets)<br>
v. Reversing a string (Eg. Code  edoC)<br>
vi. Checking if a string is palindrome.<br>
vii. Finding duplicate characters in a string (Note: print the duplicate characters only once, irrespective of the number of times it occurred)

```cpp
#include <iostream>
#include <cstring>
#include <unordered_set>
using namespace std;

int leng(const char* str){
    int i=0;
    while(*str!='\0'){
        i++;
        str++;
    }
    return i;
}

void toLower(char* str){
    while(*str!='\0'){
        if(*str>='A' && *str<='Z'){
            *str+=32;
        }
        str++;
    }
}

void countWordandVowels(const char* str,int& wc,int vc){
    wc=0;
    vc=0;
    bool inWord=false;
    const char* Vowel="aeiouAEIOU";

    while(*str!='\0'){
        if(*str==' ' || *str=='\t'||*str=='\n'){inWord=false;}
        else{
            if(!inWord){wc++; inWord=true;}
            if(strchr(Vowel,*str)!=nullptr){vc++;}
        }
        str++;
    }
}

bool isValidStr(const char* str){
    while(*str!='\0'){
        if(!((*str>='a' && *str<='z')|| (*str>='A'||*str=='Z'))){ return false;}
        str++;
    }
    return true;
}

void reverseStr(char* str){
    int len=leng(str);
    char* i=str;
    char* n=str+len-1;
    while(i<n){
        char temp = *i;
        *i=*n;
        *n=temp;
        i++;
        n--;
    }
}

bool isPalindrome(const char* str){
    int len=leng(str);
    const char* i=str;
    const char* n=str+len-1;
    while (i < n) {
        if (*i != *n) {return false;}
        i++;
        n--;
    }
    return true;
}

void dupFind(const char* str){
    unordered_set<char> charS;
    unordered_set<char> dupS;

    while (*str != '\0') {
        if (charS.find(*str) != charS.end()) { dupS.insert(*str);}
        else {charS.insert(*str); }
        str++;
    }
    for (char c : dupS) {cout << c << " ";}
    cout << endl;
}

    int main(){
    const char* input = "Hello World";
    int wc, vc;

    cout << "Length of the string: " << leng(input) << endl;

    char lowercaseString[] = "Hello World";
    toLower(lowercaseString);
    cout << "String in lowercase: " << lowercaseString << endl;

    countWordandVowels(input, wc, vc);
    cout << "Number of words in the string: " << wc << endl;
    cout << "Number of vowels in the string: " << vc << endl;

    cout << "Is the string valid? " << (isValidStr(input) ? "Yes" : "No") << endl;

    char reversedString[] = "Hello World";
    reverseStr(reversedString);
    cout << "Reversed string: " << reversedString << endl;

    cout << "Is the string a palindrome? " << (isPalindrome(input) ? "Yes" : "No") << endl;

    cout << "Duplicate characters in the string: ";
    dupFind(input);

    return 0;
}
```

### Q2. Write a program for finding the factorial of a number recursively.
```cpp
#include<iostream>
unsigned long long fact(int n){
    if(n==0||n==1){return 1;}
    else{return n* fact(n-1);}
}

int main(){
    int n;
    std::cout <<"Enter a number: ";
    std::cin >>n;
    std::cout<<"Factorial of "<<n<<" is "<<fact(n)<<std::endl;
    return 0;
}
```

### Q3. Implement combination formula nCr using recursion and code it in C/C++/JAVA/Python language.
```cpp
#include<iostream>
unsigned long long fact(int n){
    if(n==0||n==1){return 1;}
    else{return n* fact(n-1);}
}

unsigned long long comb(int n,int r){return fact(n)/)(fact(r)*fact(n-r))}

int main() {
    int n, r;
    std::cout << "Enter the value of n: ";
    std::cin >> n;
    std::cout << "Enter the value of r: ";
    std::cin >> r;
    std::cout << "Combination C(" << n << ", " << r << ") = " << nCr(n, r) << std::endl;
    return 0;
}
```

### Q4. Implement the Tower of Hanoi Problem using recursion and code it in C/C++/JAVA/Python language.
```cpp
void toHanoi(int n,char source,char aux,char dest){
    if(n==1){ std::cout << "Move disk 1 from " << source << " to " << destination << std::endl; return;}
    toHanoi(n-1,source,dest,aux);
    std::cout << "Move disk " << n << " from " << source << " to " << dest << std::endl;
    toHanoi(n-1,aux,source,dest);
}
int main() {
    int n;
    std::cout << "Enter the number of disks: ";
    std::cin >> n;
    towerOfHanoi(n, 'A', 'B', 'C');
    return 0;
}
```

### Q5. Implement the Fibonacci series using recursion and code it in C/C++/JAVA/Python language. 
```cpp
#include <iostream>
int fib(int n){
    if(n<=1){return n;}
    return fib(n-1)+fib(n-2);
}
int main() {
    int n;
    std::cout << "Enter the number of terms in Fibonacci series: ";
    std::cin >> n;
    std::cout << "Fibonacci series: ";
    for (int i = 0; i < n; ++i) {
        std::cout << fibonacci(i) << " ";
    }
    std::cout << std::endl;
    return 0;
}
```

# Assignment 1
### Q1.	Write a program that 
1. computes and returns the sum of all the integers between "first" and "last" inclusive.
2. computes and returns the smallest positive integer n for which 1+2+3+...+n equals or exceeds the value of "goal".
3. computes and returns the greatest common divisor (g.c.d.) of the arguments passed to it.
4. Determines whether an integer is prime.
5. Prints the English name of an integer from 1 to 9.
6. Reverses the order of the objects in an array.
7. Finds the index of the largest number in an array.
8. Shifts the contents of array cells one cell to the right, with the last cell's contents moved to the left end.
9. Examines an array of integers and eliminates all duplication of values. The distinct integers are all moved to the left part of the array.
10. Copies numbers from two arrays into a third array. The numbers from the second array are placed to the right of the numbers copied from the first array.

```cpp
    #include <iostream>
    #include <vector>
    #include <algorithm>
    #include <numeric>

    using namespace std;

    // i. Computes and returns the sum of all the integers between "first" and "last" inclusive.
    int sumInRange(int first, int last) {
        return (last - first + 1) * (first + last) / 2;
    }

    // ii. Computes and returns the smallest positive integer n for which 1+2+3+...+n equals or exceeds the value of "goal".
    int smallestIntegerForSum(int goal) {
        return ceil((-1 + sqrt(1 + 8 * goal)) / 2);
    }

    // iii. Computes and returns the greatest common divisor (g.c.d.) of the arguments passed to it.
    int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    // iv. Determines whether an integer is prime.
    bool isPrime(int num) {
        if (num <= 1) return false;
        for (int i = 2; i * i <= num; i++) {
            if (num % i == 0) return false;
        }
        return true;
    }

    // v. Prints the English name of an integer from 1 to 9.
    string printIntegerName(int num) {
        vector<string> names = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
        if (num < 1 || num > 9) return "Invalid number";
        return names[num - 1];
    }

    // vi. Reverses the order of the objects in an array.
    void reverseArray(vector<int>& arr) {
        reverse(arr.begin(), arr.end());
    }

    // vii. Finds the index of the largest number in an array.
    int indexOfLargestNumber(const vector<int>& arr) {
        return distance(arr.begin(), max_element(arr.begin(), arr.end()));
    }

    // viii. Shifts the contents of array cells one cell to the right, with the last cell's contents moved to the left end.
    void shiftRight(vector<int>& arr) {
        if (arr.empty()) return;
        int last = arr.back();
        arr.pop_back();
        arr.insert(arr.begin(), last);
    }

    // ix. Examines an array of integers and eliminates all duplication of values.
    void eliminateDuplicates(vector<int>& arr) {
        sort(arr.begin(), arr.end());
        arr.erase(unique(arr.begin(), arr.end()), arr.end());
    }

    // x. Copies numbers from two arrays into a third array. The numbers from the second array are placed to the right of the numbers copied from the first array.
    vector<int> mergeArrays(const vector<int>& arr1, const vector<int>& arr2) {
        vector<int> merged(arr1);
        merged.insert(merged.end(), arr2.begin(), arr2.end());
        return merged;
    }

    int main() {
        // Example usage:
        cout << "Sum of integers from 1 to 10: " << sumInRange(1, 10) << endl;
        cout << "Smallest integer for sum >= 20: " << smallestIntegerForSum(20) << endl;

        vector<int> arr = {1, 3, 5, 7, 9};
        cout << "Array before reversal: ";
        for (int num : arr) cout << num << " ";
        cout << endl;
        reverseArray(arr);
        cout << "Array after reversal: ";
        for (int num : arr) cout << num << " ";
        cout << endl;

        cout << "Index of largest number: " << indexOfLargestNumber(arr) << endl;

        cout << "Array before shifting: ";
        for (int num : arr) cout << num << " ";
        cout << endl;
        shiftRight(arr);
        cout << "Array after shifting: ";
        for (int num : arr) cout << num << " ";
        cout << endl;

        vector<int> duplicates = {1, 2, 3, 2, 4, 1, 5, 6, 3};
        cout << "Array before eliminating duplicates: ";
        for (int num : duplicates) cout << num << " ";
        cout << endl;
        eliminateDuplicates(duplicates);
        cout << "Array after eliminating duplicates: ";
        for (int num : duplicates) cout << num << " ";
        cout << endl;

        vector<int> arr1 = {1, 2, 3};
        vector<int> arr2 = {4, 5, 6};
        vector<int> merged = mergeArrays(arr1, arr2);
        cout << "Merged array: ";
        for (int num : merged) cout << num << " ";
        cout << endl;

        return 0;
    }
```

### Q2.	Write a program to find sum of all elements of an array; write a program to find maximum of elements of an array; write a program for linear search of an array.
```cpp
#include <iostream>
#include <algorithm>

using namespace std;

// Function to calculate the sum of all elements in the array
int arraySum(int arr[], int size) {
    int sum = 0;
    for (int i = 0; i < size; ++i) {
        sum += arr[i];
    }
    return sum;
}

// Function to find the maximum element in the array
int arrayMax(int arr[], int size) {
    int maxElement = arr[0];
    for (int i = 1; i < size; ++i) {
        if (arr[i] > maxElement) {
            maxElement = arr[i];
        }
    }
    return maxElement;
}

// Function to perform linear search in the array
int linearSearch(int arr[], int size, int target) {
    for (int i = 0; i < size; ++i) {
        if (arr[i] == target) {
            return i; // Return the index if target is found
        }
    }
    return -1; // Return -1 if target is not found
}

int main() {
    // Example array
    int arr[] = {5, 2, 8, 10, 1, 7};
    int size = sizeof(arr) / sizeof(arr[0]);

    // Calculate the sum of all elements
    int sum = arraySum(arr, size);
    cout << "Sum of all elements: " << sum << endl;

    // Find the maximum element
    int maxElement = arrayMax(arr, size);
    cout << "Maximum element: " << maxElement << endl;

    // Perform linear search
    int target = 10;
    int index = linearSearch(arr, size, target);
    if (index != -1) {
        cout << "Linear search: Element " << target << " found at index " << index << endl;
    } else {
        cout << "Linear search: Element " << target << " not found in the array" << endl;
    }

    return 0;
}
```

### Q3.	Write a program for adding and subtracting Matrices using 2D Arrays.
```cpp
#include <iostream>
#include <vector>
#include <chrono> // For timing

using namespace std;
using namespace std::chrono;

// Function to add two matrices
void addMatrices(const vector<vector<int>>& mat1, const vector<vector<int>>& mat2, vector<vector<int>>& result) {
    int rows = mat1.size();
    int cols = mat1[0].size();
    
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            result[i][j] = mat1[i][j] + mat2[i][j];
        }
    }
}

// Function to subtract two matrices
void subtractMatrices(const vector<vector<int>>& mat1, const vector<vector<int>>& mat2, vector<vector<int>>& result) {
    int rows = mat1.size();
    int cols = mat1[0].size();
    
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            result[i][j] = mat1[i][j] - mat2[i][j];
        }
    }
}

int main() {
    // Define two matrices
    vector<vector<int>> mat1 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    vector<vector<int>> mat2 = {{9, 8, 7}, {6, 5, 4}, {3, 2, 1}};
    int rows = mat1.size();
    int cols = mat1[0].size();

    // Initialize result matrices
    vector<vector<int>> additionResult(rows, vector<int>(cols));
    vector<vector<int>> subtractionResult(rows, vector<int>(cols));

    // Add matrices and measure execution time
    auto start = high_resolution_clock::now(); // Start timer
    addMatrices(mat1, mat2, additionResult);
    auto stop = high_resolution_clock::now(); // Stop timer

    auto duration = duration_cast<microseconds>(stop - start); // Calculate duration

    // Print result matrix (for demonstration, can be omitted for large matrices)
    cout << "Result of Addition:" << endl;
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            cout << additionResult[i][j] << " ";
        }
        cout << endl;
    }

    // Print execution time
    cout << "Addition Execution Time: " << duration.count() << " microseconds" << endl;

    // Subtract matrices and measure execution time
    start = high_resolution_clock::now(); // Start timer
    subtractMatrices(mat1, mat2, subtractionResult);
    stop = high_resolution_clock::now(); // Stop timer

    duration = duration_cast<microseconds>(stop - start); // Calculate duration

    // Print result matrix (for demonstration, can be omitted for large matrices)
    cout << "Result of Subtraction:" << endl;
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            cout << subtractionResult[i][j] << " ";
        }
        cout << endl;
    }

    // Print execution time
    cout << "Subtraction Execution Time: " << duration.count() << " microseconds" << endl;

    return 0;
}
```

### Q4. Write a program to Multiply Matrices of 10X10.
```cpp
#include <iostream>
#include <chrono> // For timing

using namespace std;
using namespace std::chrono;

// Function to multiply two matrices
void multiplyMatrices(int mat1[][10], int mat2[][10], int result[][10]) {
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            result[i][j] = 0;
            for (int k = 0; k < 10; ++k) {
                result[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }
}

int main() {
    // Define two 10x10 matrices
    int mat1[10][10];
    int mat2[10][10];

    // Initialize matrices with random values (for demonstration)
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            mat1[i][j] = i + j; // Example initialization
            mat2[i][j] = i - j; // Example initialization
        }
    }

    // Initialize result matrix
    int result[10][10];

    // Multiply matrices and measure execution time
    auto start = high_resolution_clock::now(); // Start timer
    multiplyMatrices(mat1, mat2, result);
    auto stop = high_resolution_clock::now(); // Stop timer

    auto duration = duration_cast<microseconds>(stop - start); // Calculate duration

    // Print result matrix (for demonstration, can be omitted for large matrices)
    cout << "Resultant Matrix:" << endl;
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            cout << result[i][j] << " ";
        }
        cout << endl;
    }

    // Print execution time
    cout << "Execution Time: " << duration.count() << " microseconds" << endl;

    return 0;
}
```

### Q5. Write a program for Linear Search using Functions.
```cpp
#include <iostream>
#include <vector>
#include <chrono> // For timing

using namespace std;
using namespace std::chrono;

// Function for linear search
int linearSearch(const vector<int>& arr, int target) {
    for (int i = 0; i < arr.size(); ++i) {
        if (arr[i] == target) {
            return i; // Return index if element is found
        }
    }
    return -1; // Return -1 if element is not found
}

int main() {
    // Define the array
    vector<int> arr = {2, 4, 7, 1, 9, 6, 3, 8, 5};

    // Define the target element to search
    int target = 6;

    // Perform linear search and measure execution time
    auto start = high_resolution_clock::now(); // Start timer
    int index = linearSearch(arr, target);
    auto stop = high_resolution_clock::now(); // Stop timer

    auto duration = duration_cast<microseconds>(stop - start); // Calculate duration

    // Print the result
    if (index != -1) {
        cout << "Element " << target << " found at index " << index << endl;
    } else {
        cout << "Element " << target << " not found in the array" << endl;
    }

    // Print execution time
    cout << "Execution Time: " << duration.count() << " microseconds" << endl;

    return 0;
}
```

### Q6. Write a class for a rectangle. Create objects for this class and call the member functions to find the area and perimeter of the rectangle.
```cpp
#include <iostream>

using namespace std;

// Class definition for Rectangle
class Rectangle {
private:
    double length;
    double width;

public:
    // Constructor to initialize length and width
    Rectangle(double l, double w) : length(l), width(w) {}

    // Function to calculate the area of the rectangle
    double calculateArea() {
        return length * width;
    }

    // Function to calculate the perimeter of the rectangle
    double calculatePerimeter() {
        return 2 * (length + width);
    }
};

int main() {
    // Create objects of Rectangle class
    Rectangle rect1(5.0, 3.0);
    Rectangle rect2(7.0, 4.0);

    // Calculate and print area and perimeter for rect1
    cout << "Rectangle 1:" << endl;
    cout << "Area: " << rect1.calculateArea() << endl;
    cout << "Perimeter: " << rect1.calculatePerimeter() << endl;

    // Calculate and print area and perimeter for rect2
    cout << "\nRectangle 2:" << endl;
    cout << "Area: " << rect2.calculateArea() << endl;
    cout << "Perimeter: " << rect2.calculatePerimeter() << endl;

    return 0;
}
```
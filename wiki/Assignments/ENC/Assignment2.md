# Assignment 2
### Q1.	If a one dimensional integer type array with its size and length given, write the code in C++ language to create functions to  perform the following operations (Please assume whatever is necessary to examplify the results) :

i. Display()<br>
ii. Add/Append(x)<br>
iii. Insert(index,x)<br>
iv. Delete(x)<br>
v. LinearSearch(s)<br>
vi. BinarySearch(x)<br>
vii. Get(index)<br>
viii. Set(index,x)<br>
ix. Max()<br>
x. Min()<br>
xi. Reverse()<br>
xii. Shift()<br>
xiii. Rotate()<br>

```cpp
#include <iostream>
using namespace std;

class Array {
private:
    int *A;  // Pointer to the array stored in heap
    int size; // Maximum size of array
    int length; // Number of elements in array

public:
    Array(int size) { // Constructor
        this->size = size;
        A = new int[size];
        length = 0;
    }

    ~Array() { // Destructor
        delete[] A;
    }

    void Display();
    void Append(int x);
    bool Insert(int index, int x);
    bool Delete(int x);
    int LinearSearch(int key);
    int BinarySearch(int key);
    int Get(int index);
    bool Set(int index, int x);
    int Max();
    int Min();
    void Reverse();
    void Shift();
    void Rotate();

    // Helper functions
private:
    void swap(int *x, int *y);
};

void Array::Display() {
    for (int i = 0; i < length; i++)
        cout << A[i] << " ";
    cout << endl;
}

void Array::Append(int x) {
    if (length < size)
        A[length++] = x;
}

bool Array::Insert(int index, int x) {
    if (index >= 0 && index <= length && length < size) {
        for (int i = length; i > index; i--)
            A[i] = A[i - 1];
        A[index] = x;
        length++;
        return true;
    }
    return false;
}

bool Array::Delete(int x) {
    int index = LinearSearch(x);
    if (index != -1) {
        for (int i = index; i < length - 1; i++)
            A[i] = A[i + 1];
        length--;
        return true;
    }
    return false;
}

int Array::LinearSearch(int key) {
    for (int i = 0; i < length; i++)
        if (A[i] == key)
            return i;
    return -1;
}

int Array::BinarySearch(int key) {
    int low = 0, high = length - 1;
    while (low <= high) {
        int mid = (low + high) / 2;
        if (key == A[mid])
            return mid;
        else if (key < A[mid])
            high = mid - 1;
        else
            low = mid + 1;
    }
    return -1;
}

int Array::Get(int index) {
    if (index >= 0 && index < length)
        return A[index];
    return -1; // Assuming -1 is used to indicate error
}

bool Array::Set(int index, int x) {
    if (index >= 0 && index < length) {
        A[index] = x;
        return true;
    }
    return false;
}

int Array::Max() {
    int max = A[0];
    for (int i = 1; i < length; i++)
        if (A[i] > max)
            max = A[i];
    return max;
}

int Array::Min() {
    int min = A[0];
    for (int i = 1; i < length; i++)
        if (A[i] < min)
            min = A[i];
    return min;
}

void Array::Reverse() {
    for (int i = 0, j = length - 1; i < j; i++, j--) {
        swap(&A[i], &A[j]);
    }
}

void Array::Shift() {
    if (length == 0) return;
    for (int i = 0; i < length - 1; i++)
        A[i] = A[i + 1];
    A[length - 1] = 0; // Assuming 0 is the neutral element for shift
}

void Array::Rotate() {
    if (length == 0) return;
    int temp = A[0];
    for (int i = 0; i < length - 1; i++)
        A[i] = A[i + 1];
    A[length - 1] = temp;
}

void Array::swap(int *x, int *y) {
    int temp = *x;
    *x = *y;
    *y = temp;
}

int main() {
    Array arr(10); // Create an array of size 10
    arr.Append(3);
    arr.Append(7);
    arr.Append(9);
    arr.Append(15);
    arr.Append(20);
    
    cout << "Initial Array: ";
    arr.Display();

    arr.Insert(3, 10);
    cout << "After Insertion: ";
    arr.Display();

    arr.Delete(9);
    cout << "After Deletion: ";
    arr.Display();

    cout << "Linear Search for 15 found at index: " << arr.LinearSearch(15) << endl;
    
    // Sort the array before using Binary Search for correct results
    // Assume the array is already sorted for this demonstration
    cout << "Binary Search for 10 found at index: " << arr.BinarySearch(10) << endl;

    cout << "Maximum: " << arr.Max() << endl;
    cout << "Minimum: " << arr.Min() << endl;

    arr.Reverse();
    cout << "After Reverse: ";
    arr.Display();

    arr.Shift();
    cout << "After Shift: ";
    arr.Display();

    arr.Rotate();
    cout << "After Rotate: ";
    arr.Display();

    return 0;
}
```

### Q2. 	For a given array, write functions to perform the following:
i. Check if an array is sorted<br>
ii. Merge arrays<br>
iii. Set operations on array: Union, Intersection
```cpp
#include <iostream>
using namespace std;

// Function to check if an array is sorted
// Assuming ascending order
bool isSorted(int arr[], int n) {
    for (int i = 0; i < n - 1; i++) {
        if (arr[i] > arr[i + 1]) {
            return false;
        }
    }
    return true;
}

// Function to merge two sorted arrays
void mergeArrays(int arr1[], int n1, int arr2[], int n2, int arr3[]) {
    int i = 0, j = 0, k = 0;
    while (i < n1 && j < n2) {
        if (arr1[i] < arr2[j]) {
            arr3[k++] = arr1[i++];
        } else {
            arr3[k++] = arr2[j++];
        }
    }
    while (i < n1) {
        arr3[k++] = arr1[i++];
    }
    while (j < n2) {
        arr3[k++] = arr2[j++];
    }
}

// Function for Union of two sorted arrays
void unionArrays(int arr1[], int n1, int arr2[], int n2, int arr3[], int &k) {
    int i = 0, j = 0;
    k = 0; // Initialize output array length
    while (i < n1 && j < n2) {
        if (arr1[i] < arr2[j]) {
            arr3[k++] = arr1[i++];
        } else if (arr2[j] < arr1[i]) {
            arr3[k++] = arr2[j++];
        } else {
            arr3[k++] = arr1[i++];
            j++;
        }
    }
    while (i < n1) {
        arr3[k++] = arr1[i++];
    }
    while (j < n2) {
        arr3[k++] = arr2[j++];
    }
}

// Function for Intersection of two sorted arrays
void intersectionArrays(int arr1[], int n1, int arr2[], int n2, int arr3[], int &k) {
    int i = 0, j = 0;
    k = 0; // Initialize output array length
    while (i < n1 && j < n2) {
        if (arr1[i] < arr2[j]) {
            i++;
        } else if (arr2[j] < arr1[i]) {
            j++;
        } else {
            arr3[k++] = arr1[i++];
            j++;
        }
    }
}

// Utility function to print array
void printArray(int arr[], int n) {
    for (int i = 0; i < n; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

int main() {
    int arr1[] = {1, 3, 5, 7};
    int n1 = sizeof(arr1) / sizeof(arr1[0]);

    int arr2[] = {2, 3, 5, 6};
    int n2 = sizeof(arr2) / sizeof(arr2[0]);

    cout << "Array 1 is sorted: " << (isSorted(arr1, n1) ? "Yes" : "No") << endl;
    cout << "Array 2 is sorted: " << (isSorted(arr2, n2) ? "Yes" : "No") << endl;

    int arr3[n1 + n2];
    mergeArrays(arr1, n1, arr2, n2, arr3);
    cout << "Merged array: ";
    printArray(arr3, n1 + n2);

    int arrUnion[n1 + n2];
    int kUnion;
    unionArrays(arr1, n1, arr2, n2, arrUnion, kUnion);
    cout << "Union of arrays: ";
    printArray(arrUnion, kUnion);

    int arrIntersection[min(n1, n2)];
    int kIntersection;
    intersectionArrays(arr1, n1, arr2, n2, arrIntersection, kIntersection);
    cout << "Intersection of arrays: ";
    printArray(arrIntersection, kIntersection);

    return 0;
}
```

### Q3. 	For a given array, write functions to perform the following:
i. Finding single element in an array
ii. Finding multiple elements in an array<br>
iii. Finding duplicates in a sorted array<br>
iv. Finding duplicates in an unsorted array<br>
v. Finding a pair of elements with sum k<br>
vi. Finding a pair of elements with sum k in sorted array<br>
vii. Finding max and min in a single scan
```cpp
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <algorithm> // for sort function
using namespace std;

// i. Finding single element in an array
bool findElement(int arr[], int n, int key) {
    for (int i = 0; i < n; i++) {
        if (arr[i] == key) return true;
    }
    return false;
}

// ii. Finding multiple elements in an array
void findElements(int arr[], int n, int keys[], int k) {
    unordered_set<int> s(keys, keys + k);
    for (int i = 0; i < n; i++) {
        if (s.find(arr[i]) != s.end()) {
            cout << arr[i] << " ";
        }
    }
    cout << endl;
}

// iii. Finding duplicates in a sorted array
void findDuplicatesSorted(int arr[], int n) {
    for (int i = 0; i < n - 1; i++) {
        if (arr[i] == arr[i + 1]) {
            while (i < n - 1 && arr[i] == arr[i + 1]) i++;
            cout << arr[i] << " ";
        }
    }
    cout << endl;
}

// iv. Finding duplicates in an unsorted array
void findDuplicatesUnsorted(int arr[], int n) {
    unordered_map<int, int> freq;
    for (int i = 0; i < n; i++) {
        freq[arr[i]]++;
    }
    for (auto it : freq) {
        if (it.second > 1) {
            cout << it.first << " ";
        }
    }
    cout << endl;
}

// v. Finding a pair of elements with sum k
void findPairWithSum(int arr[], int n, int k) {
    unordered_set<int> s;
    for (int i = 0; i < n; i++) {
        int temp = k - arr[i];
        if (s.find(temp) != s.end()) {
            cout << temp << " + " << arr[i] << " = " << k << endl;
            return; // Remove return if you want to find all pairs
        }
        s.insert(arr[i]);
    }
}

// vi. Finding a pair of elements with sum k in sorted array
void findPairWithSumSorted(int arr[], int n, int k) {
    int low = 0, high = n - 1;
    while (low < high) {
        int sum = arr[low] + arr[high];
        if (sum == k) {
            cout << arr[low] << " + " << arr[high] << " = " << k << endl;
            return; // Remove return if you want to find all pairs
        } else if (sum < k) {
            low++;
        } else {
            high--;
        }
    }
}

// vii. Finding max and min in a single scan
pair<int, int> findMaxMin(int arr[], int n) {
    if (n == 1) return {arr[0], arr[0]};
    
    int max, min;
    if (arr[0] > arr[1]) {
        max = arr[0];
        min = arr[1];
    } else {
        max = arr[1];
        min = arr[0];
    }

    for (int i = 2; i < n; i++) {
        if (arr[i] > max) max = arr[i];
        else if (arr[i] < min) min = arr[i];
    }
    return {max, min};
}

int main() {
    int arr[] = {6, 3, 8, 10, 16, 7, 5, 2, 9, 14};
    int n = sizeof(arr) / sizeof(arr[0]);
    int key = 10;
    cout << "Element " << key << (findElement(arr, n, key) ? " is" : " is not") << " in the array" << endl;

    int keys[] = {10, 5, 2};
    int k = sizeof(keys) / sizeof(keys[0]);
    cout << "Finding multiple elements: ";
    findElements(arr, n, keys, k);

    int sortedArr[] = {1, 2, 2, 3, 4, 4, 4, 5, 5};
    int sortedN = sizeof(sortedArr) / sizeof(sortedArr[0]);
    cout << "Duplicates in sorted array: ";
    findDuplicatesSorted(sortedArr, sortedN);

    cout << "Duplicates in unsorted array: ";
    findDuplicatesUnsorted(arr, n);

    int sum = 19;
    cout << "Pair with sum " << sum << ": ";
    findPairWithSum(arr, n, sum);

    sort(arr, arr + n); // Sorting the array for the next operation
    cout << "Pair with sum " << sum << " in sorted array: ";
    findPairWithSumSorted(arr, n, sum);

    auto [max, min] = findMaxMin(arr, n);
    cout << "Max: " << max << ", Min: " << min << endl;

    return 0;
}
```
### Q4.	For a given Linked List (LL), write programs to perform the following functions
i. Display the elements of a LL<br>
ii. Count and sum the nodes of a LL<br>
iii. Search for a key element in a LL<br>
iv. Delete an element from a LL<br>
v. Check if a LL is sorted<br>
vi. Merge 2 LLs<br>
vii. Concatenate 2 LLs<br>
viii. Reverse the elements of a LL<br>
ix. Create and Display a circular Ll<br>
x. Create a doubly LL, insert in a doubly LL and reverse a doubly LL.
```cpp
#include <iostream>
class Node {
    public:
        int data;
        Node* next;
        Node(int data){
            this-> data=data;
            this-> next=nullptr;
        }
};

class LinkedList{
    private:
        Node* head;
    public:
        LinkedList(){head = nullptr;}
        ~LinkedList(){
            Node* current = head;
            while(current !=nullptr){
                Node* next = current-> next;
                delete current;
                current=next;                
            }
        }
        void append(int data){
            Node* newN = new Node(data);
            if(head == nullptr){head = newN;}
            else{
                Node* temp = head;
                while(temp->next != nullptr){
                    temp = temp -> next;
                }
                temp ->next =newN;
            }
        }
        void display() {
            Node* temp =head;
            while(temp != nullptr){
                std::cout <<  temp->data << "->";
                temp = temp -> next;
            }
            std::cout << "nullptr" << std::endl;
        }
        int CountandSum(){
            int i=0;
            int s=0;
            Node* temp = head;
            while(temp!=nullptr){
                i++;
                s += temp->data;
                temp = temp -> next;
            }
            return i,s;
        }
        int searchKey(int key){
            Node* temp =head;
            int i=0;
            while(temp!= nullptr){
                if(temp->data == key){ return i;}
                else{return false;} 
                temp = temp->next;
            }
            return true;
        }
        void deleteKey(int key){
            Node* temp=head;
            Node* prev=nullptr;
            while(temp!=nullptr && temp->data != key){
                prev = temp;
                temp = temp->next;
            }
            if (temp==nullptr){std::cout<<"not Found in this list"<<std::endl; return;}
            if(prev==nullptr){ head = temp -> next;} else {prev->next=temp->next;}
            delete temp;
        }
        bool checkSort(){
            Node* temp = head;
            while(temp!=nullptr && temp->next!=nullptr){
                if(temp->data>temp->next->data){return false;}
                temp=temp->next;
            }
            return true;
        }
        void merger(const LinkedList& otherList,int i){
            if(i<0){ std::cout<<"Invalid Index"<<std::endl; return;}
            if(i==0){
                Node* temp=otherList.head;
                while(temp->next!=nullptr){temp=temp->next;}
                temp->next=head;
                head=otherList.head;
            }
            else{
                Node* temp=head;
                int j=0;
                while(temp!=nullptr && j<i-1){temp=temp->next; j++;}
                if(temp==nullptr){
                    std::cout<<"Index out of range "<<std::endl; return;}
                Node* next = temp->next;
                temp->next=otherList.head;
                while(temp->next !=nullptr){temp=temp->next;}
                temp->next=next;
            }
        }
        void concat(const LinkedList& otherList){
            Node* temp=head;
            while(temp->next!=nullptr){temp=temp->next;}
            temp->next=otherList.head;
        }
        void reverseLL(){
            Node* current=head;
            Node *prev=nullptr,*next=nullptr;
            while(current!=nullptr){
                next=current->next;
                current->next=prev;
                prev=current;
                current=next;
            }
            head=prev;
        }
};
class CircularLL{
    private:
        Node* head;
    public:
        CircularLL(){head=nullptr;}
        ~CircularLL(){
            if(head==nullptr){return;}
            Node* temp = head->next;
            while(temp!=head){
                Node* next = temp->next;
                delete temp;
                temp=next;
            }
            delete head;
            head=nullptr;
        }
        void append(int data){
            Node* newN = new Node(data);
            if(head==nullptr){head=newN; newN->next=head;}
            else{
                Node* temp =head;
                while(temp->next!=head){temp=temp->next;}
                temp->next=newN;
                newN->next=head;
            }
        }
        void display(){
            if (head == nullptr) {
                std::cout << "Empty Circular Linked List" << std::endl;
                return;
            }

            Node* temp = head;
            do {
                std::cout << temp->data << " -> ";
                temp = temp->next;
            } while (temp != head);
            std::cout << " (Head)" << std::endl;   
        }
        void makeCircular(){
            if (head == nullptr) return;

            Node* temp = head;
            while (temp->next != nullptr) {
                temp = temp->next;
            }
            temp->next = head;
        }
};

class DNode{
    public:
        int data;
        DNode* prev;
        Dnode* next;
        Dnode(int data){
            this->data=data;
            this->prev=nullptr;
            this->next=nullptr;
        }
};
class doubleLL{
    private:
        DNode* head;
    public:
        doubleLL(){head=nullptr;}
        ~doubleLL(){
            DNode* temp=head;
            while(temp!=nullptr){
                DNode* next=temp->next;
                delete temp;
                temp=next;
            }
        }
        void insert(int data){
            DNode* newN = new DNode(data);
            if(head==nullptr){head=newN;}
            else{
                newN->next=head;
                head->prev=newN;
                head=newN;
            }
        }
        void display(){
            DoublyNode* temp = head;
            while (temp != nullptr) {
                std::cout << temp->data << " -> ";
                temp = temp->next;
            }
            std::cout << "nullptr" << std::endl;
        }
        void reverse(){
            DNode* current=head;
            DNode* temp=nullptr;
            while(temp!=nullptr){
                temp=current->prev;
                current->prev=current->next;
                current->next=temp;
                current=current->prev;
            }
            if (temp != nullptr) {
                head = temp->prev;
            }
        }
}
int main() {
    LinkedList list;
    list.append(1);
    list.append(2);
    list.append(3);

    std::cout << "Linked List: ";
    list.display();
    
    std::cout << "Sum of nodes: " << list.CountandSum() << std::endl;
    std::cout << "Is 2 in the list? " << (list.searchKey(2) ? "Yes" : "No") << std::endl;

    list.deleteKey(2);
    std::cout << "Linked List after deleting 2: ";
    list.display();

    std::cout << "Is the list sorted? " << (list.checkSort() ? "Yes" : "No") << std::endl;

    LinkedList anotherList;
    anotherList.append(4);
    anotherList.append(5);
    anotherList.append(6);

    list.merger(anotherList,1);
    std::cout << "Linked List after merging with another list: ";
    list.display();

    LinkedList yetAnotherList;
    yetAnotherList.append(7);
    yetAnotherList.append(8);
    yetAnotherList.append(9);

    list.concat(yetAnotherList);
    std::cout << "Linked List after concatenating with yet another list: ";
    list.display();

    list.reverseLL();
    std::cout << "Linked List after reversing: ";
    list.display();
    // Circular Linked List
        CircularLinkedList circularList;
        circularList.append(1);
        circularList.append(2);
        circularList.append(3);

        std::cout << "Circular Linked List: ";
        circularList.display();
        circularList.makeCircular();

        // Doubly Linked List
        DoublyLinkedList doublyList;
        doublyList.insert(1);
        doublyList.insert(2);
        doublyList.insert(3);

        std::cout << "Doubly Linked List: ";
        doublyList.display();

        std::cout << "Reversed Doubly Linked List: ";
        doublyList.reverse();
        doublyList.display();

    return 0;
}
```
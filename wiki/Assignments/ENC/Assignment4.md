### Q1. Implement the following stack operations using arrays:
- Push
- Pop
- IsFull
- isEmpty
- Peek
- stackTop
Create a stack structure and stack class for the above said implementation.
```cpp
#include <iostream>
using namespace std;
class Stack {
    private:
        int *A;
        int size;
        int length;

    public:
        Stack(int size){
            this -> size = size;
            A = new int[size];
            length =0;
        }
        ~Stack(){
            delete[] A;
        }
        void push(int x){
            if (length == size){cout<< "Stack Overflow" <<endl;}
            else {
                A[length++] = x;
            }
        }
        int pop(){
            if (length <= 0){cout<< "Stack Overflow" <<endl;return -1;}
            else {return A[--length];}
        }
        bool isFull(){
            if (length==size) {return true;}
            else {return false;}
        }
        bool isEmpty(){
            if (length==0) {return true;}
            else {return false;}
        }
        int peek(){
            if (length==0){cout <<" Stack is empty "<<endl;return -1;}
            else {return A[length-1];}
        }
        int stackTop(){
            if (length==0){cout <<" Stack is empty "<<endl;return -1;}
            else {return length-1;}
        }

};

int main(){
    Stack stack(5);
    cout << "is Stack Empty? "<<stack.isEmpty()<< endl << endl;    
    stack.push(1);
    stack.push(2);
    stack.push(3);    
    stack.push(4);
    stack.push(5);
    cout << "Stack is Full? " << stack.isFull() << endl << "Top element: " << stack.peek() << endl <<"with index: " << stack.stackTop()<< endl << endl;    

    stack.pop();
    cout << "After popping, " << endl << "Top element: " << stack.peek() << endl <<"with index: " << stack.stackTop()<< endl; 
    return 0;
}
```
### Q2. 	Implement the following stack operations using linked List:
- Push
- Pop
- IsFull
- isEmpty
- Peek
Create a stack structure and stack class for the above said implementation.

```cpp
#include <iostream>
using namespace std;

class Node {
    public:
        int data;
        Node* next;
        Node(int x){
            this->data = x;
            this->next = nullptr;
        }
};

class Stack {
    private:
        Node* head;
        int capacity;
        int size;
    public:
        Stack(int capacity){
            head = nullptr;
            this->capacity = capacity;
            size=0;    
        }
        ~Stack(){
            Node* current = head;
            while(current!=nullptr){
                Node* next = current->next;
                delete current;
                current=next;
            }
        }
        void push(int i){
            if(isFull()){cerr<< "Stack Overflow" <<endl; return;}
            Node* newN = new Node(i);
            if(head==nullptr){head = newN;}
            else{
                Node* temp=head;
                while(temp->next!=nullptr){
                   temp = temp->next; 
                }
                temp->next=newN;
            }
            size++;
        }
        int pop(){
            if(isEmpty()){cerr<<"Stack Underflow"<<endl; return -1;}
            Node* temp =head;
            Node* prev=nullptr;
            while(temp->next!=nullptr){
                prev=temp;
                temp = temp->next;            
            }
            if(prev==nullptr){ head = temp -> next;} else {prev->next=temp->next;}
            int tVal = temp->data;
            delete temp;
            size--;
            return tVal;
        }
        bool isFull(){return size==capacity;}
        bool isEmpty(){ return head == nullptr;}
        int peek(){
            if(isEmpty()){cout <<" Stack is empty "<<endl;return -1;}
            Node* temp = head;
            while(temp->next!=nullptr){temp=temp->next;}
            return temp->data;
        }
        int stackTop(){
            if (size==0){cout <<" Stack is empty "<<endl;return -1;}
            else {return size-1;}
        }
};
int main(){
    Stack stack(5);
    cout << "is Stack Full? "<<(stack.isFull() ? "true" : "false")<< endl << endl;    
    stack.push(1);
    stack.push(2);
    stack.push(3);    
    stack.push(4);
    stack.push(5);
    cout << "Stack is Empty? " << (stack.isEmpty() ? "true" : "false") << endl << "Top element: " << stack.peek() << endl <<"with index: " << stack.stackTop()<< endl << endl;    

    stack.pop();
    cout << "After popping, " << endl << "Top element: " << stack.peek() << endl <<"with index: " << stack.stackTop()<< endl; 
    return 0;
}
```
### Q3.	 Implement linear queue by writing a class and also implement the following fumctions:
    - Insert or enqueue 
    - Remove or dequeue
    - Isfull
    - Isempty
Write main function to exemplify the results. Also write a main function to make the implementation a “Menu-Driven”.
```cpp
#include <iostream>
using namespace std;
class Queue {
    private:
        int *A;
        int capacity;
        int front;
        int rear;
        int size;
    public:
        Queue(int capacity){
            this->capacity = capacity;
            A = new int[capacity];
            front=0;
            rear=-1;
            size=0;    
        }
        ~Queue(){delete[] A;}
        void enQueue(int value){
            if(isFull()){cerr<<"Queue is full. Can't Do it"<<endl;}
            else {
                rear=(rear+1)%capacity;
                A[rear]= value;
                size++;
            }
        }
        int deQueue(){
            if(isEmpty()){cerr <<"Queue is Empty. Can't Do it"<<endl;return -1;}
            else {
                int tVal = A[front];
                front = (front+1)%capacity;
                size--;
                return tVal;
            }
        }
        bool isFull(){
            if (size==capacity){return true;}
            else {return false;}
        }
        bool isEmpty(){
            if(size==0){return true;}
            else {return false;}
        }
};

void menu(Queue &queue){
    cout <<"=== Queue Menu ==="<<endl<<"1. Enqueue"<<endl<<"2. Dequeue"<<endl<<"3. Is Full?"<<endl<<"4. Is Empty?"<<endl<<"5. Exit"<<endl<<"=================="<<endl<<endl<<"Enter Your Choice: ";
    int choice;
    int value;
    cin >> choice;
    switch(choice){
        case 1:
            cout << "Enter value to add: ";
            cin >> value;
            queue.enQueue(value);
            break;
        case 2:
            cout << "Removing Value";
            queue.deQueue();
            break;
        case 3:
            cout << "is Queue Full? "<<(queue.isFull()?"true":"false")<<endl;
            break;
        case 4:
            cout << "is Queue Empty? "<<(queue.isEmpty()?"true":"false")<<endl;
            break;
        case 5:
            exit(0);
            break;
        default:
            cout << "Not a valid Choice. "<<endl;
            break;
    }
    menu(queue);
}
int main(){
    Queue queue(6);
    menu(queue);
    return 0;
}
```
### Q4.	Implement a queue using two stacks only. Name the two as forward and reverse stacks.  The resultant queue (comprised of two stacks) should perform the following functions with minimum time complexity:
    i. Enqueue()
    ii. Dequeue()
    iii. isFull()
    iv. isEmpty()
Also incorporate the code for exception handling.
```cpp
#include <iostream>
#include <stack>
#include <stdexcept>

using namespace std;
class Queue {
    private:
        stack<int> fStack;
        stack<int> rStack;
        int size;
    public:
        Queue(int size): size(size){}
        void enQueue(int value){
            if(isFull()){throw overflow_error("Queue is Full");}
            fStack.push(value);
        }
        int deQueue(){
            if(isEmpty()){throw underflow_error("Queue is Empty");}
            if(rStack.empty()){
                while(!fStack.empty()){rStack.push(fStack.top());fStack.pop();}
            }
            int fr= rStack.top();
            return fr;
        }
        bool isFull(){return fStack.size() + rStack.size() >= size;}
        bool isEmpty(){return fStack.empty() && rStack.empty();}
};
void menu(Queue &queue){
    cout <<"=== Queue Menu ==="<<endl<<"1. Enqueue"<<endl<<"2. Dequeue"<<endl<<"3. Is Full?"<<endl<<"4. Is Empty?"<<endl<<"5. Exit"<<endl<<"=================="<<endl<<endl<<"Enter Your Choice: ";
    int choice;
    int value;
    cin >> choice;
    switch(choice){
        case 1:
            cout << "Enter value to add: ";
            cin >> value;
            queue.enQueue(value);
            break;
        case 2:
            cout << "Removing Value";
            queue.deQueue();
            break;
        case 3:
            cout << "is Queue Full? "<<(queue.isFull()?"true":"false")<<endl;
            break;
        case 4:
            cout << "is Queue Empty? "<<(queue.isEmpty()?"true":"false")<<endl;
            break;
        case 5:
            exit(0);
            break;
        default:
            cout << "Not a valid Choice. "<<endl;
            break;
    }
    menu(queue);
}
int main(){
    Queue queue(6);
    menu(queue);
    return 0;
}
```
### Q5. Implement priority queue by writing a class and also implement the following functions:
    - Insert or enqueue 
    - Remove or dequeue
    - Isfull
    - Isempty
Write main function to exemplify the results. Also write a main function to make the implementation a “Menu-Driven”. 
```cpp
#include <iostream>
using namespace std;

class pQueue {
    private:
        int* A;
        int size;
        int capacity;

    public:
        pQueue(int capacity){
            this->capacity=capacity;
            A = new int[capacity];
            size =0;
        }
        ~pQueue(){delete[] A;}
        void enQueue(int val){
            if(isFull()){cout<<"Queue is Full.Can't Do it"<<endl;return;}
            int i = size -1;
            while(i>=0 && val>A[i]){
                A[i+1]= A[i];
                i--;
            }
            A[i+1]=val;
            size++;
        }
        int deQueue(){
            if(isEmpty()){cout<<"Queue is Empty.Can't Do it"<<endl;return -1;}
           int val = A[size-1];
           size--;
           return val;
        }
        bool isFull(){return size==capacity;}
        bool isEmpty(){return size==0;}  
};
void menu(pQueue &queue){
    cout <<"=== Priority Queue Menu ==="<<endl<<"1. Enqueue"<<endl<<"2. Dequeue"<<endl<<"3. Is Full?"<<endl<<"4. Is Empty?"<<endl<<"5. Exit"<<endl<<"==========================="<<endl<<endl<<"Enter Your Choice: ";
    int choice;
    int value;
    cin >> choice;
    switch(choice){
        case 1:
            cout << "Enter value to add: ";
            cin >> value;
            queue.enQueue(value);
            break;
        case 2:
            cout << "Removing Value";
            queue.deQueue();
            break;
        case 3:
            cout << "is Queue Full? "<<(queue.isFull()?"true":"false")<<endl;
            break;
        case 4:
            cout << "is Queue Empty? "<<(queue.isEmpty()?"true":"false")<<endl;
            break;
        case 5:
            exit(0);
            break;
        default:
            cout << "Not a valid Choice. "<<endl;
            break;
    }
    menu(queue);
}
int main(){
    pQueue queue(6);
    menu(queue);
    return 0;
}
```
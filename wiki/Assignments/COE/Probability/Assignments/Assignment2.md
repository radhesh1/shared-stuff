# Assignment 2

### Consider the following statistical distributions
i. Binomial
ii. Poisson
iii. Uniform
iv. Exponential distribution
v. Gamma distribution
vi. Normal distribution
vii. Log-normal distribution
viii. Weibull distribution
1 . Make use of inbuilt functions and also write your own functions to deal with PMF/ PDF,
CDF and quantile functions for the above mentioned statistical distributions.

```R
binom_pdf <- function(x, n, p) {
  return(choose(n, x) * p^x * (1 - p)^(n - x))
}

poisson_pdf <- function(x, lam) {
  return(lam^x * exp(-lam) / factorial(x))
}

uniform_pdf <- function(x, n) {
  return(rep(1/n, n))  # Return a constant value of 1
}

uniform_cdf <- function(x, n) {
  return(sum(uniform_pdf(n)[1:x]))  # Ensure the values are between 0 and 1
}

exponential_pdf <- function(x, lam) {
  return(lam * exp(-lam * x))
}

exponential_cdf <- function(x, lam) {
  ifelse(x >= 0, (1 - exp(-lam * x)), 0)
}

gamma_pdf <- function(x, alpha, beta) {
  return((beta^alpha) * (x^(alpha - 1)) * exp(-beta * x) / gamma(alpha))
}

normal_pdf <- function(x, mean, sd) {
  return((1 / (sqrt(2 * pi) * sd)) * exp(-(x - mean)^2 / (2 * sd^2)))
}

log_normal_pdf <- function(x, mean, sd) {
  exponent <- -(log(x) - mean)^2 / (2 * sd^2)
  pdf <- (1 / (sqrt(2 * pi) * x * sd)) * exp(exponent)
  pdf[!is.finite(pdf)] <- 0  # Replace non-finite values with 0
  return(pdf)
}

# Cumulative distribution function for log-normal distribution
log_normal_cdf <- function(x, mean, sd) {
  integrand <- function(t) {
    log_normal_pdf(t, mean, sd)
  }
  cdf <- sapply(x, function(x_val) {
    result <- integrate(integrand, 0, x_val)
    if (is.finite(result$value)) {
      return(result$value)
    } else {
      return(0)  # Replace non-finite results with 0
    }
  })
  return(cdf)
}

weibull_pdf <- function(x, k, lam) {
  return((k / lam) * (x / lam)^(k - 1) * exp(-(x / lam)^k))
}
params <- list(
  binom = list(n = 10, p = 0.5),
  poisson = list(lam = 2),
  uniform = list(n = 10),
  exponential = list(lam = 2),
  gamma = list(alpha = 2, beta = 1),
  normal = list(mean = 0, sd = 1),
  log_normal = list(mean = 0, sd = 1),
  weibull = list(k = 2, lam = 1)
)


cdf <- function(pdf_function,x) {
  params_list <- params[[pdf_function]]
  cdf_values <- numeric(length(x))  # Initialize vector to store CDF values
  
  for (i in seq_along(x)) {
    # Compute PDF values up to x[i]
    pdf_values <- switch(pdf_function,
                         "binom" = sapply(0:x[i], function(k) binom_pdf(k, params_list$n, params_list$p)),
                         "poisson" = sapply(0:x[i], function(k) poisson_pdf(k, params_list$lam)),
                         "uniform" = uniform_cdf(x[i], params_list$n),
                         "exponential" = exponential_cdf(x[i], params_list$lam),  # Pass the specific x value
                         "gamma" = sapply(0:x[i], function(k) gamma_pdf(k, params_list$alpha, params_list$beta)),
                         "normal" = sapply(0:x[i], function(k) normal_pdf(k, params_list$mean, params_list$sd)),
                         "log_normal" = sapply(0:x[i], function(k) log_normal_pdf(k, params_list$mean, params_list$sd)),
                         "weibull" = sapply(0:x[i], function(k) weibull_pdf(k, params_list$k, params_list$lam))
    )
    # Sum up the probabilities to get the CDF value
    cdf_values[i] <- sum(pdf_values)
  }
  return(cdf_values)
}
```

2 . Find mean and variance of the mentioned statistical distributions.

```R
binom_pdf <- function(x, n, p) {
  return(choose(n, x) * p^x * (1 - p)^(n - x))
}

poisson_pdf <- function(x, lam) {
  return(lam^x * exp(-lam) / factorial(x))
}

uniform_pdf <- function(x, n) {
  return(rep(1/n, n))  # Return a constant value of 1
}

uniform_cdf <- function(x, n) {
  return(sum(uniform_pdf(n)[1:x]))  # Ensure the values are between 0 and 1
}

exponential_pdf <- function(x, lam) {
  return(lam * exp(-lam * x))
}

exponential_cdf <- function(x, lam) {
  ifelse(x >= 0, (1 - exp(-lam * x)), 0)
}

gamma_pdf <- function(x, alpha, beta) {
  return((beta^alpha) * (x^(alpha - 1)) * exp(-beta * x) / gamma(alpha))
}

normal_pdf <- function(x, mean, sd) {
  return((1 / (sqrt(2 * pi) * sd)) * exp(-(x - mean)^2 / (2 * sd^2)))
}

log_normal_pdf <- function(x, mean, sd) {
  exponent <- -(log(x) - mean)^2 / (2 * sd^2)
  pdf <- (1 / (sqrt(2 * pi) * x * sd)) * exp(exponent)
  pdf[!is.finite(pdf)] <- 0  # Replace non-finite values with 0
  return(pdf)
}

# Cumulative distribution function for log-normal distribution
log_normal_cdf <- function(x, mean, sd) {
  integrand <- function(t) {
    log_normal_pdf(t, mean, sd)
  }
  cdf <- sapply(x, function(x_val) {
    result <- integrate(integrand, 0, x_val)
    if (is.finite(result$value)) {
      return(result$value)
    } else {
      return(0)  # Replace non-finite results with 0
    }
  })
  return(cdf)
}

weibull_pdf <- function(x, k, lam) {
  return((k / lam) * (x / lam)^(k - 1) * exp(-(x / lam)^k))
}

# Function to calculate mean using x * pdf(x)
calculate_mean <- function(pdf_function, params) {
  x_values <- 0:1000  # Assuming maximum value for discrete distributions
  pdf_values <- switch(pdf_function,
                       "binom" = sapply(x_values, function(x) x * binom_pdf(x, params$n, params$p)),
                       "poisson" = sapply(x_values, function(x) x * poisson_pdf(x, params$lam)),
                       "uniform" = sapply(x_values, function(x) x * uniform_pdf(x, params$n)),
                       "exponential" = sapply(x_values, function(x) x * exponential_pdf(x, params$lam)),
                       "gamma" = sapply(x_values, function(x) x * gamma_pdf(x, params$alpha, params$beta)),
                       "normal" = sapply(x_values, function(x) x * normal_pdf(x, params$mean, params$sd)),
                       "log_normal" = sapply(x_values, function(x) x * log_normal_pdf(x, params$mean, params$sd)),
                       "weibull" = sapply(x_values, function(x) x * weibull_pdf(x, params$k, params$lam)))
  return(sum(pdf_values))
}

# Function to calculate variance using x^2 * pdf(x)
calculate_variance <- function(pdf_function, params) {
  x_values <- 0:1000  # Assuming maximum value for discrete distributions
  pdf_values <- switch(pdf_function,
                       "binom" = sapply(x_values, function(x) x^2 * binom_pdf(x, params$n, params$p)),
                       "poisson" = sapply(x_values, function(x) x^2 * poisson_pdf(x, params$lam)),
                       "uniform" = sapply(x_values, function(x) x^2 * uniform_pdf(x, params$n)),
                       "exponential" = sapply(x_values, function(x) x^2 * exponential_pdf(x, params$lam)),
                       "gamma" = sapply(x_values, function(x) x^2 * gamma_pdf(x, params$alpha, params$beta)),
                       "normal" = sapply(x_values, function(x) x^2 * normal_pdf(x, params$mean, params$sd)),
                       "log_normal" = sapply(x_values, function(x) x^2 * log_normal_pdf(x, params$mean, params$sd)),
                       "weibull" = sapply(x_values, function(x) x^2 * weibull_pdf(x, params$k, params$lam)))
  return(sum(pdf_values))
}
```

3 . Generate random sample of size 1000 from the each mentioned statistical distribution,
and compare the results of sample mean and sample variance with the theoretical mean
and variance of the distributions.
```R
binom_pdf <- function(x, n, p) {
  return(choose(n, x) * p^x * (1 - p)^(n - x))
}

poisson_pdf <- function(x, lam) {
  return(lam^x * exp(-lam) / factorial(x))
}

uniform_pdf <- function(x, n) {
  return(rep(1/n, n))  # Return a constant value of 1
}

uniform_cdf <- function(x, n) {
  return(sum(uniform_pdf(n)[1:x]))  # Ensure the values are between 0 and 1
}

exponential_pdf <- function(x, lam) {
  return(lam * exp(-lam * x))
}

exponential_cdf <- function(x, lam) {
  ifelse(x >= 0, (1 - exp(-lam * x)), 0)
}

gamma_pdf <- function(x, alpha, beta) {
  return((beta^alpha) * (x^(alpha - 1)) * exp(-beta * x) / gamma(alpha))
}

normal_pdf <- function(x, mean, sd) {
  return((1 / (sqrt(2 * pi) * sd)) * exp(-(x - mean)^2 / (2 * sd^2)))
}

log_normal_pdf <- function(x, mean, sd) {
  exponent <- -(log(x) - mean)^2 / (2 * sd^2)
  pdf <- (1 / (sqrt(2 * pi) * x * sd)) * exp(exponent)
  pdf[!is.finite(pdf)] <- 0  # Replace non-finite values with 0
  return(pdf)
}

# Cumulative distribution function for log-normal distribution
log_normal_cdf <- function(x, mean, sd) {
  integrand <- function(t) {
    log_normal_pdf(t, mean, sd)
  }
  cdf <- sapply(x, function(x_val) {
    result <- integrate(integrand, 0, x_val)
    if (is.finite(result$value)) {
      return(result$value)
    } else {
      return(0)  # Replace non-finite results with 0
    }
  })
  return(cdf)
}

weibull_pdf <- function(x, k, lam) {
  return((k / lam) * (x / lam)^(k - 1) * exp(-(x / lam)^k))
}
# Set seed for reproducibility
set.seed(123)

# Function to generate random samples for each distribution
generate_random_sample <- function(pdf_function, params, sample_size) {
  samples <- switch(pdf_function,
                    "binom" = replicate(sample_size, {
                      # Generate binomial sample using inverse transform sampling
                      u <- runif(1)
                      q <- 0
                      cum_prob <- binom_pdf(q, params$n, params$p)
                      while (u > cum_prob) {
                        q <- q + 1
                        cum_prob <- cum_prob + binom_pdf(q, params$n, params$p)
                      }
                      return(q)
                    }),
                    "poisson" = replicate(sample_size, {
                      # Generate Poisson sample using inverse transform sampling
                      u <- runif(1)
                      q <- 0
                      cum_prob <- poisson_pdf(q, params$lam)
                      while (u > cum_prob) {
                        q <- q + 1
                        cum_prob <- cum_prob + poisson_pdf(q, params$lam)
                      }
                      return(q)
                    }),
                    "uniform" = sample(0:params$n, sample_size, replace = TRUE),
                    "exponential" = replicate(sample_size, {
                      # Generate exponential sample using inverse transform sampling
                      u <- runif(1)
                      q <- 0
                      cum_prob <- exponential_cdf(q, params$lam)
                      while (u > cum_prob) {
                        q <- q + 0.1  # Increasing by 0.1 for discretization
                        cum_prob <- exponential_cdf(q, params$lam)
                      }
                      return(q)
                    }),
                    "gamma" = replicate(sample_size, {
                      # Generate gamma sample using inverse transform sampling
                      u <- runif(1)
                      q <- 0
                      cum_prob <- gamma_pdf(q, params$alpha, params$beta)
                      while (u > cum_prob) {
                        q <- q + 0.1  # Increasing by 0.1 for discretization
                        cum_prob <- gamma_pdf(q, params$alpha, params$beta)
                      }
                      return(q)
                    }),
                    "normal" = rnorm(sample_size, params$mean, params$sd),
                    "log_normal" = {
                      # Generate log-normal sample using Box-Muller transformation
                      u1 <- runif(sample_size)
                      u2 <- runif(sample_size)
                      z <- sqrt(-2 * log(u1)) * cos(2 * pi * u2)
                      exp(params$mean + params$sd * z)
                    },
                    "weibull" = replicate(sample_size, {
                      # Generate Weibull sample using inverse transform sampling
                      u <- runif(1)
                      q <- 0
                      cum_prob <- weibull_pdf(q, params$k, params$lam)
                      while (u > cum_prob) {
                        q <- q + 0.1  # Increasing by 0.1 for discretization
                        cum_prob <- weibull_pdf(q, params$k, params$lam)
                      }
                      return(q)
                    })
  )
  return(samples)
}

# Function to calculate sample mean and variance
calculate_sample_mean_variance <- function(samples) {
  sample_mean <- mean(samples)
  sample_variance <- var(samples)
  return(c(sample_mean, sample_variance))
}

# Function to compare sample mean and variance with theoretical values
compare_sample_stats <- function(pdf_function, params, sample_size) {
  # Generate random sample
  samples <- generate_random_sample(pdf_function, params, sample_size)
  
  # Calculate sample mean and variance
  sample_stats <- calculate_sample_mean_variance(samples)
  
  # Calculate theoretical mean and variance
  theoretical_mean <- calculate_mean(pdf_function, params)
  theoretical_variance <- calculate_variance(pdf_function, params)
  
  # Print results
  cat("Distribution:", pdf_function, "\n")
  cat("Sample Mean:", sample_stats[1], "\nTheoretical Mean:", theoretical_mean, "\n")
  cat("Sample Variance:", sample_stats[2], "\nTheoretical Variance:", theoretical_variance, "\n\n")
}

# Perform comparison for each distribution
sample_size <- 1000  # Number of samples to generate
for (pdf_function in names(params)) {
  compare_sample_stats(pdf_function, params[[pdf_function]], sample_size)
}
```

4 . Consider an experiment of tossing a coin four times in which probability mass function
of getting a head is given by P(X = x) = kx, here random variable X denotes the total
number of heads. Write a program to find the value of k, E(X) and Var(X).
```R
# Define the probability mass function
probability_mass_function <- function(x) {
  k <- 0.5  # Probability of getting a head
  return(k * x)
}

# Calculate the expected value
expected_value <- function() {
  expected <- 0
  for (x in 0:4) {
    expected <- expected + x * probability_mass_function(x)
  }
  return(expected)
}

# Calculate the variance
variance <- function() {
  expected <- expected_value()
  variance <- 0
  for (x in 0:4) {
    variance <- variance + (x - expected) ^ 2 * probability_mass_function(x)
  }
  return(variance)
}

# Main function
main <- function() {
  k <- probability_mass_function(1)  # Calculate the value of k
  cat("Value of k:", k, "\n")
  
  expected <- expected_value()  # Calculate the expected value
  cat("Expected value E(X):", expected, "\n")
  
  var <- variance()  # Calculate the variance
  cat("Variance Var(X):", var, "\n")
}

# Call the main function
main()
```
5 . Suppose a random variable X follows a probability density function given by f(x; β) = β*e^(−x/2), x ≥ 0, β > 0. Write a program to find the value of β, E(X), and Var(X)

```R
# Define the probability density function
pdf_X <- function(x, beta) {
  ifelse(x >= 0, beta * exp(-x/2), 0)
}

# Function to integrate the PDF from 0 to infinity and solve for beta
find_beta <- function() {
  result <- integrate(function(x) pdf_X(x, beta), lower = 0, upper = Inf)
  return(1 / result$value) # Inverse of the integral is beta
}

# Find the value of beta
beta <- find_beta()

# Calculate E[X] and Var[X]
e_x <- integrate(function(x) x * pdf_X(x, beta), lower = 0, upper = Inf)$value
var_x <- integrate(function(x) (x - e_x)^2 * pdf_X(x, beta), lower = 0, upper = Inf)$value

# Print the results
cat("Value of β:", beta, "\n")
cat("Expected Value (E[X]):", e_x, "\n")
cat("Variance (Var[X]):", var_x, "\n")
```
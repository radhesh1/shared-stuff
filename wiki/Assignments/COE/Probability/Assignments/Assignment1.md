# Assignment 1
1 . Consider mtcars and iris (Motor Trend Car Road Tests and Iris) data sets available in
R-statistical software, and find
    i. Mean, median, mode, 1st quartile, 2nd quartile, 3rd quartile, variance, standard deviation, and covariance between any two variables using inbuilt     functions and by writing your own function with proper documentation.
    ii. Describe data using summary in R
    iii. Make use of histogram, bar chart, pie chart and boxplot to illustrate data on
    different variables.

```R
# Load necessary datasets
data(mtcars)
data(iris)

# i
custom_covariance <- function(x, y) {
  n <- length(x)
  mean_x <- mean(x)
  mean_y <- mean(y)
  covariance <- sum((x - mean_x) * (y - mean_y)) / (n - 1)
  return(covariance)
}

# Function to calculate statistics for a dataset
calculate_statistics <- function(dataset) {
  result <- list(
    mean = apply(dataset, 2, function(x) mean(x, na.rm = TRUE)),
    median = apply(dataset, 2, function(x) median(x, na.rm = TRUE)),
    mode = apply(dataset, 2, function(x) {
      ux <- unique(x)
      ux[which.max(tabulate(match(x, ux)))]
    }),
    Q1 = apply(dataset, 2, function(x) quantile(x, probs = 0.25, na.rm = TRUE)),
    Q2 = apply(dataset, 2, function(x) quantile(x, probs = 0.5, na.rm = TRUE)),
    Q3 = apply(dataset, 2, function(x) quantile(x, probs = 0.75, na.rm = TRUE)),
    variance = apply(dataset, 2, function(x) var(x, na.rm = TRUE)),
    standard_deviation = apply(dataset, 2, function(x) sd(x, na.rm = TRUE))
  )
  return(result)
}

sapply(iris, function(x) any(!is.numeric(x) | is.na(x)))
iris_numeric <- iris[sapply(iris, is.numeric)]

# Print the results
print(calculate_statistics(mtcars))
cat("Covariance (mtcars):", custom_covariance(mtcars$mpg, mtcars$wt), "\n")
print(calculate_statistics(iris_numeric))
cat("Covariance (iris):", custom_covariance(iris$Sepal.Length, iris$Sepal.Width), "\n")

# ii
summary(mtcars)
summary(iris)

# iii
barplot(table(mtcars$cyl), main = "Bar chart of Number of Cylinders", xlab = "Cylinders", ylab = "Frequency")
boxplot(mtcars$wt, main = "Boxplot of Car Weight", ylab = "Weight")

hist(iris$Sepal.Length, main = "Histogram of Sepal Length", xlab = "Sepal Length")
pie(table(iris$Species), main = "Pie chart of Species")
```

2 . Make use of plot, lines and legend functions in R to plot the graph of PMF/PDFs and
CDFs of following statistical distributions corresponding to various parameter values
on the same x-axis.
    i. Binomial
    ii. Poisson
    iii. Uniform
    iv. Exponential distribution
    v. Gamma distribution
    vi. Normal distribution
    vii. Log-normal distribution
    viii. Weibull distribution

```R
# Define the function f with lam pre-set
binom_pdf <- function(x, n, p) {
  return(choose(n, x) * p^x * (1 - p)^(n - x))
}
poisson_pdf <- function(x, lam) {
  return(lam^x * exp(-lam) / factorial(x))
}

uniform_pdf <- function(x) {
  return(rep(1 / (max(x) - min(x)), length(x)))
}


exponential_pdf <- function(x, lam) {
  return(lam * exp(-lam * x))
}

gamma_pdf <- function(x, alpha, beta) {
  return((beta^alpha) * (x^(alpha - 1)) * exp(-beta * x) / gamma(alpha))
}

normal_pdf <- function(x, mean, sd) {
  return((1 / (sqrt(2 * pi) * sd)) * exp(-(x - mean)^2 / (2 * sd^2)))
}

log_normal_pdf <- function(x, mean, sd) {
  exponent <- -(log(x) - mean)^2 / (2 * sd^2)
  pdf <- (1 / (sqrt(2 * pi) * x * sd)) * exp(exponent)
  pdf[!is.finite(pdf)] <- 0  # Replace non-finite values with 0
  return(pdf)
}

# Cumulative distribution function for log-normal distribution
log_normal_cdf <- function(x, mean, sd) {
  integrand <- function(t) {
    log_normal_pdf(t, mean, sd)
  }
  cdf <- sapply(x, function(x_val) {
    result <- integrate(integrand, 0, x_val)
    if (is.finite(result$value)) {
      return(result$value)
    } else {
      return(0)  # Replace non-finite results with 0
    }
  })
  return(cdf)
}

weibull_pdf <- function(x, k, lam) {
  return((k / lam) * (x / lam)^(k - 1) * exp(-(x / lam)^k))
}

x <- seq(0, 10)

# Parameters for distributions
params <- list(
  binom = list(n = 10, p = 0.5),
  poisson = list(lam = 2),
  uniform = list(),
  exponential = list(lam = 2),
  gamma = list(alpha = 2, beta = 1),
  normal = list(mean = 0, sd = 1),
  log_normal = list(mean = 0, sd = 1),
  weibull = list(k = 2, lam = 1)
)


ploty <- function(pdf_function, xlim = NULL) {
  params_list <- params[[pdf_function]]
  
  pdf <- switch(pdf_function,
                "binom" = binom_pdf(x, params_list$n, params_list$p),
                "poisson" = poisson_pdf(x, params_list$lam),
                "uniform" = uniform_pdf(x),
                "exponential" = exponential_pdf(x, params_list$lam),
                "gamma" = gamma_pdf(x, params_list$alpha, params_list$beta),
                "normal" = normal_pdf(x, params_list$mean, params_list$sd),
                "log_normal" = log_normal_pdf(x, params_list$mean, params_list$sd),
                "weibull" = weibull_pdf(x, params_list$k, params_list$lam)
  )
  
  plot(x, pdf, type = "l", xlab = "x", ylab = "Probability",
       main = paste(sub("^([a-z])", "\\U\\1", pdf_function, perl=TRUE), "Distribution"), col = "blue",
       xlim = c(min(x), max(x)), ylim = c(0, 1))
  
  if (pdf_function != "uniform" && pdf_function != "exponential" && pdf_function !="log_normal"&& pdf_function !="binom") {
    lines(x, cumsum(pdf), type = "l", col = "red")
    legend("topleft", legend = c("PDF", "CDF"), col = c("blue", "red"), lty = 1)
  }else if(pdf_function == "exponential"){
    lines(x, 1 - exp(-1*(params_list$lam * x)), type = "l", col = "red")
    legend("topleft", legend = c("PDF", "CDF"), col = c("blue", "red"), lty = 1)
  } else if (pdf_function == "log_normal") {
    cdf <- log_normal_cdf(x, params_list$mean, params_list$sd)
    lines(x, cdf, type = "l", col = "red")
    legend("topleft", legend = c("PDF", "CDF"), col = c("blue", "red"), lty = 1)
  } else if (pdf_function == "binom") {
    cdf <- sapply(x, function(i) sum(binom_pdf(0:i, params_list$n, params_list$p)))
    lines(x, cdf, type = "l", col = "red")
    legend("topleft", legend = c("PDF", "CDF"), col = c("blue", "red"), lty = 1)
  } else {
    lines(x, cumsum(pdf), type = "s", col = "red")
    legend("topleft", legend = c("PDF","CDF"), col = c("blue", "red"), lty = 1)
  }
}


ploty("binom",xlim=x)
ploty("poisson",xlim=x)
ploty("uniform",xlim=x)
ploty("exponential",xlim=x)
ploty("gamma",xlim=x)
ploty("normal",xlim=x)
ploty("log_normal",xlim=x)
ploty("weibull",xlim=x)

```
### 1. Create table employees which has the following attributes (employee table)
(@empno, ename, job, sal, deptno)
Where empno is primary key, ename is unique, job in (Prof, AP,
and Lect), sal is not NULL, and deptno default is 10.
Insert appropriate records, check error messages in case of
violation and list all the constraint names for given table.
```plsql
DROP TABLE employees;
CREATE TABLE employees (
    empno INT PRIMARY KEY,
    ename VARCHAR2(40) UNIQUE,
    job VARCHAR2(10) CHECK (job IN ('Prof', 'AP', 'Lect')),
    sal NUMBER NOT NULL,
    deptno INT DEFAULT 10
);

-- Insert records
INSERT INTO employees (empno, ename, job, sal, deptno) VALUES (100, 'John', 'Prof', 50000.00, 20);
INSERT INTO employees (empno, ename, job, sal, deptno) VALUES (2, 'Jane', 'AP', 45000.00, 10);
INSERT INTO employees (empno, ename, job, sal, deptno) VALUES (3, 'Alice', 'Lect', 40000.00, 30);

-- Try to insert a record with a duplicate ename (should fail)
INSERT INTO employees (empno, ename, job, sal, deptno) VALUES (4, 'Jane', 'Prof', 55000.00, 20);

-- Try to insert a record with a null salary (should fail)
INSERT INTO employees (empno, ename, job, sal, deptno) VALUES (5, 'Bob', 'AP', NULL, 10);

-- Displaying structure
DESC employees;
SELECT * FROM employees;
```
#### Output:
![imgbb](https://i.ibb.co/L9RdN86/image.png)
![imgbb](https://i.ibb.co/9gmSwjh/image.png)
![imgbb](https://i.ibb.co/sstZTmx/image.png)

### 2. Create table book:
Rno number—PK
DOI-date
DOR-date
DOR>DOI
Insert appropriate records, check error messages in case of
violation and list all the constraint names for given table.
```plsql
CREATE TABLE book (
    Rno INT PRIMARY KEY,
    DOI DATE,
    DOR DATE,
    CONSTRAINT check_dor_gt_doi CHECK (DOR > DOI)
);

-- Insert records
INSERT INTO book (Rno, DOI, DOR) VALUES (1, TO_DATE('2022-01-01', 'YYYY-MM-DD'), TO_DATE('2022-01-10', 'YYYY-MM-DD'));
INSERT INTO book (Rno, DOI, DOR) VALUES (2, TO_DATE('2022-02-15', 'YYYY-MM-DD'), TO_DATE('2022-03-01', 'YYYY-MM-DD'));
INSERT INTO book (Rno, DOI, DOR) VALUES (3, TO_DATE('2022-03-10', 'YYYY-MM-DD'), TO_DATE('2022-03-15', 'YYYY-MM-DD'));

-- Try to insert a record with DOR <= DOI (should fail)
INSERT INTO book (Rno, DOI, DOR) VALUES (4, TO_DATE('2022-04-01', 'YYYY-MM-DD'), TO_DATE('2022-03-01', 'YYYY-MM-DD'));

-- Displaying structure
DESC book;
SELECT * FROM book;
```
#### Output:
![imgbb](https://i.ibb.co/vLffXxw/image.png)
![imgbb](https://i.ibb.co/Mpb2nrH/image.png)
![imgbb](https://i.ibb.co/KhLRLX1/image.png)


### 3. Create table st
Rno-Number
Class-Char
Marks-Number
Primary key(rno,class)
Marks>0
Insert appropriate records, check error messages in case of
violation and list all the constraint names for given table.
```plsql
CREATE TABLE st (
    Rno INT,
    Class CHAR(1),
    Marks INT,
    CONSTRAINT pk_st PRIMARY KEY (Rno, Class),
    CONSTRAINT check_marks_gt_zero CHECK (Marks > 0)
);

-- insert records
INSERT INTO st (Rno, Class, Marks) VALUES (1, 'A', 98);
INSERT INTO st (Rno, Class, Marks) VALUES (2, 'B', 10);

-- Try to insert a faulty record
-- This will fail due to the CHECK constraint
INSERT INTO st (Rno, Class, Marks) VALUES (3, 'C', 0);

-- insert records
INSERT INTO st (Rno, Class, Marks) VALUES (1, 'A', 98);
INSERT INTO st (Rno, Class, Marks) VALUES (2, 'B', 10);

-- Try to insert a faulty record
INSERT INTO st (Rno, Class, Marks) VALUES (3, 'C', 0);

-- Displaying structure
DESC st;
SELECT * FROM st;
```
#### Output:
![imgbb](https://i.ibb.co/Q7B9NkQ/image.png)
![imgbb](https://i.ibb.co/F4Pwv2D/image.png)
![imgbb](https://i.ibb.co/8zHHZmx/image.png)

### 4. Create table S which has the following attributes (Salesperson table)
(sno, sname, city)
Where sno is primary key
```plsql
CREATE TABLE S (
    sno INT PRIMARY KEY,
    sname VARCHAR2(40),
    city VARCHAR2(15)
);

-- Displaying structure
DESC S;
```
#### Output:
![imgbb](https://i.ibb.co/09FMW1P/image.png)

### 5. Create table P which has the following attributes (Part table)
(pno, pname, color)
Where pno is primary key
```plsql
CREATE TABLE P (
    pno INT PRIMARY KEY,
    pname VARCHAR2(40),
    color VARCHAR2(15)
);

-- Displaying structure
DESC P;
```
#### Output:
![imgbb](https://i.ibb.co/NrrHdSz/image.png)

### 6. Create table SP which has the following attributes
(sno, pno qty)
Where combination of (sno, pno) is primary key, also sno and
pno are foreign keys
```plsql
CREATE TABLE SP (
    sno INT,
    pno INT,
    qty INT,
    PRIMARY KEY (sno, pno),
    FOREIGN KEY (sno) REFERENCES S(sno),
    FOREIGN KEY (pno) REFERENCES P(pno)
); 

-- Displaying structure
DESC SP;
```
#### Output:
![imgbb](https://i.ibb.co/58DcqkG/image.png)

### 7. Create table dept which has the following attributes
(department table)
(deptno, dname)
Where deptno is primary key, dname in (Acc, comp, elect)
```plsql
CREATE TABLE dept (
    deptno INT PRIMARY KEY,
    dname VARCHAR2(10) CHECK (dname IN ('Acc', 'comp', 'elect'))
);

-- Displaying structure
DESC dept;
```
#### Output:
![imgbb](https://i.ibb.co/pWZ2vDR/image.png)

### 8. Create table emp which has the following attributes (employeetable)
(@empno, ename, job, sal, deptno)
Where empno is primary key, ename is unique, job in (Prof, AP,
and Lect), sal is not NULL, and deptno is foreign key
```plsql
CREATE TABLE emp (
    empno INT PRIMARY KEY,
    ename VARCHAR2(50) UNIQUE,
    job VARCHAR2(10) CHECK (job IN ('Prof', 'AP', 'Lect')),
    sal INT NOT NULL,
    deptno INT,
    FOREIGN KEY (deptno) REFERENCES dept(deptno)
);

-- Displaying structure
DESC emp;
```
#### Output:
![imgbb](https://i.ibb.co/16hFpP3/image.png)
### 1. Create the following schema and insert some tuples in these tables shown below.
.
	Author         (ID, Name, Birth_Year, Death_Year (NULL in case of Author is alive))
	Book            (ID, Author_ID, Title, Publish_Year, Publishing_House, Rating))
	Adaptation  (Book_ID, Type, Title, Release_Year, Rating)
```mysql
-- Table creation
CREATE TABLE Author (
ID int PRIMARY KEY,
Name varchar(30),
Birth_Year int NOT NULL,
Death_Year int
);

CREATE TABLE Book (
ID int,
Author_ID int PRIMARY KEY,
Title varchar(55),
Publish_Year int,
Publishing_House varchar(25),
Rating int
);
  
CREATE TABLE Adaptation (
Book_ID int PRIMARY KEY,
Type varchar(20),
Title varchar(55),
Release_Year int,
Rating int
);

-- Addition of Data
INSERT INTO Author(ID, Name, Birth_Year, Death_Year) VALUES
(1, 'Chetan Bhagat', 1974, NULL),
(2, 'Rabindranath Tagore', 1861, 1941),
(3, 'Arundhati Roy', 1961, NULL);
  
INSERT INTO Book(ID, Author_ID, Title, Publish_Year, Publishing_House, Rating) VALUES
(101, 1, 'Five Point Someone', 2004, 'Rupa & Co.', 4),
(102, 2, '2 States', 2009, 'Bhagat Publications', 3),
(103, 3, 'Gitanjali', 1910, 'Macmillan', 5),
(104, 4, 'The God of Small Things', 1997, 'IndiaInk', 4);
  
INSERT INTO Adaptation(Book_ID, Type, Title, Release_Year, Rating) VALUES
(101, 'Movie', '3 Idiots', 2009, 4),
(102, 'Movie', '2 States', 2014, 3),
(103, 'TV Series', 'Gitanjali', 1989, 4),
(104, 'Movie', 'The God of Small Things', 2021, 4)
```
##### Output:
![[dbms8-1.1.png]]
![[dbms8-1.2.png]]
![[dbms8-1.3.png]]

### 2. Write SQL command for the following using Join:
#### i) Display the title of each book and the name of its author.
```mysql
SELECT b.Title,a.Name FROM Book b JOIN Author a ON b.Author_ID=a.ID; 
```
##### Output:
![[dbms8-2.1.png]]
#### ii) Display the name of each author together with the title of the book they wrote and the year in which that book was published (Show only books published after 2005).
```mysql
SELECT a.name, b.Title, b.Publish_Year FROM Author a JOIN Book b ON a.ID=b.Author_ID WHERE b.Publish_Year>2005; 
```
##### Output:
![[dbms8-2.2.png]]
#### iii) For each book, show its title, adaptation title, adaptation year, and publication year. Consider only books with a rating lower than the rating of their corresponding adaptation.
```mysql
SELECT b.Title AS 'Book Title', ad.Title AS 'Adaptation Title', ad.Release_Year AS 'Adaptation Year', b.Publish_Year AS 'Publication Year' FROM Book b JOIN Adaptation ad ON b.ID=ad.Book_ID WHERE b.Rating<ad.Rating;
```
##### Output:
![[dbms8-2.3.png]]
#### iv) Display the title of each book together with its rating. Consider only those books that were published by authors who are still alive. (Use Inner Join).
```mysql
SELECT b.Title, b.Rating FROM Book b INNER JOIN Author a ON b.Author_ID=a.ID WHERE a.Death_Year IS NULL;
```
##### Output:
![[dbms8-2.4.png]]
#### v) Display the title of each book along with the name of its author. Show all books, even those without an author. Show all authors, even those who haven't published a book yet. (Use Full JOIN).
```mysql
SELECT b.Title, a.Name FROM Book b LEFT JOIN Author a ON b.Author_ID = a.ID UNION SELECT b.Title, a.Name FROM Book b RIGHT JOIN Author a ON b.Author_ID = a.ID;
```
##### Output:
![[dbms8-2.5.png]]
#### vi) Generate all possible pairs of book titles and author names. Consider only books whose author's name is ‘Chetan Bhagat’ (Use Cross JOIN).
```mysql
SELECT b.Title,a.name FROM Book b CROSS JOIN Author a WHERE a.Name='Chetan Bhagat';
```
##### Output:
![[dbms8-2.6.png]]
#### vii) Select each book's title, the name of its publishing house and the title of its adaptation on the type of the adaptation (‘Movie’). (Use Right JOIN).
```mysql
SELECT  b.Title,b.Publishing_House,ad.Title FROM Book b RIGHT JOIN Adaptation ON b.ID = ad.Book_ID WHERE ad.Type='Movie';
```
##### Output:
![[dbms8-2.7.png]]
#### viii) Show the title of each book and the name of its author — but only if the author was born in the 20th century. Otherwise, the author's name field should be NULL (Use Left JOIN).
```mysql
SELECT b.Title,a.Name FROM Book b, LEFT JOIN Author a ON b.Author_ID=a.ID WHERE 
```
##### Output:
![[dbms8-2.8.png]]
### 3. Consider the following relation and execute the given queries(Aggregate/Group By/Having):
```mysql
-- create structure
CREATE TABLE Nobel(
	YEAR int,
	SUBJECT varchar(13),
	WINNER varchar(30),
	COUNTRY varchar(10),
	CATEGORY varchar(15)
);

-- addition of data
INSERT INTO Nobel(YEAR,SUBJECT,WINNER,COUNTRY,CATEGORY) VALUES
(1970, 'Physics', 'Hannes Alfven', 'Sweden', 'Scientist'),
(1970, 'Physics', 'Louis Neel', 'France', 'Scientist'),
(1970, 'Chemistry', 'Luis Frederico Leloir', 'France', 'Scientist'),
(1970, 'Physiology', 'Ulf von Euler', 'Sweden', 'Scientist'),
(1970, 'Physiology', 'Bernard Katz', 'Germany', 'Scientist'),
(1970, 'Literature', 'Hannes Alfven', 'Russia', 'Linguist'),
(1970, 'Economics', 'Paul Samuelson', 'USA', 'Economist'),
(1970, 'Physiology', 'Julius Axelrod', 'USA', 'Scientist'),
(1971, 'Physics', 'Dennis Gabor', 'Hungary', 'Scientist'),
(1971, 'Chemistry', 'Gerhard Herzberg', 'Germany', 'Scientist'),
(1971, 'Peace', 'Willy Brandt', 'Germany', 'Chancellor'),
(1971, 'Literature', 'Pablo Neruda', 'Chile', 'Linguist'),
(1971, 'Economics', 'Simon Kuznets', 'Russia', 'Economist'),
(1978, 'Peace', 'Anwar al-Sadat', 'Egypt', 'President'),
(1978, 'Peace', 'Menachem Begin', 'Israel', 'Prime Minister'),
(1987, 'Chemistry', 'Donald J. Cram', 'USA', 'Scientist'),
(1987, 'Chemistry', 'Jean-Marie Lehn', 'France', 'Scientist'),
(1987, 'Physiology', 'Susuma Tonegawa', 'Japan', 'Scientist'),
(1994, 'Economics', 'Reinhard Selten', 'Germany', 'Economist'),
(1994, 'Peace', 'Yitzhak Rabin', 'Israel', 'Prime Minister'),
(1987, 'Physics', 'Johannes Georg Bednorz', 'Germany', 'Scientist'),
(1987, 'Literature', 'Joseph Brodsky', 'Russia', 'Linguist'),
(1987, 'Economics', 'Robert Solow', 'USA', 'Economoist'),
(1994, 'Literature', 'Kenzaburo Oe', 'Japan', 'Linguist');
```
![[dbms8-3.0.png]]
#### i) Show the total number of prizes awarded.
```mysql
SELECT COUNT(*) AS 'Number of Prizes' FROM Nobel;
```
##### Output:
![[dbms8-3.1.png]]

#### ii) List each subject - just once
```mysql
SELECT DISTINCT(SUBJECT) AS 'Subject' FROM Nobel;
```
##### Output:
![[dbms8-3.2.png]]

#### iii) Show the total number of prizes awarded for Physics
```mysql
SELECT COUNT(*) AS 'Number of Prizes' FROM Nobel WHERE SUBJECT='Physics';
```
##### Output:
![[dbms8-3.3.png]]

#### iv) For each subject show the subject and the number of prizes.
```mysql
SELECT SUBJECT, COUNT(*) AS PrizeCount FROM Nobel GROUP BY SUBJECT ORDER BY PrizeCount;
```
##### Output:
![[dbms8-3.4.png]]

#### v) For each subject show the first year that the prize was awarded.
```mysql
SELECT SUBJECT, MIN(YEAR) AS FirstYear FROM Nobel GROUP BY SUBJECT;
```
##### Output:
![[dbms8-3.5.png]]

#### vi) For each subject show the number of prizes awarded in the year 2000.
```mysql
SELECT SUBJECT,COUNT(*) AS PrizeCount FROM Nobel WHERE YEAR=2000 GROUP BY SUBJECT;
```
##### Output:
![[dbms8-3.6.png]]

#### vii) Show the number of different winners for each subject.
```mysql
SELECT COUNT(DISTINCT WINNER) AS 'Number of Different Winner for Each Subject' FROM Nobel GROUP BY SUBJECT;
```
##### Output:
![[dbms8-3.7.png]]

#### viii) For each subject show how many years have had prizes awarded.
```mysql
SELECT SUBJECT, COUNT(DISTINCT YEAR) AS 'Awards Given Distinctly' FROM Nobel GROUP BY SUBJECT;
```
##### Output:
![[dbms8-3.8.png]]

#### ix) Show the years in which three prizes were given for Physics.
```mysql
SELECT YEAR FROM Nobel WHERE SUBJECT='Physics' GROUP BY YEAR HAVING COUNT(*)=3;
```
##### Output:
![[dbms8-3.9.png]]

#### x) Show winners who have won more than once.
```mysql
SELECT WINNER FROM Nobel GROUP BY WINNER HAVING COUNT(*)>1;
```
##### Output:
![[dbms8-3.10.png]]

#### xi) Show winners who have won more than one subject.
```mysql
SELECT WINNER FROM Nobel GROUP BY SUBJECT HAVING COUNT(DISTINCT SUBJECT)>1;
```
##### Output:
![[dbms8-3.11.png]]

#### xii) Show the year and subject where 3 prizes were given. Show only years 2000 onwards.
```mysql
SELECT YEAR,SUBJECT FROM Nobel WHERE YEAR>=2000 GROUP BY YEAR,SUBJECT HAVING COUNT(*)=3;
```
##### Output:
![[dbms8-3.12.png]]
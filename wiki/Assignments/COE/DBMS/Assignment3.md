### Q1) Use the following functions:
#### 1. char (n):
```plsql
SELECT CHR(65) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/Yh3H7PL/image.png)

#### 2. concat(char1,char2):
```plsql
SELECT 'Hello' || 'World' FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/Fswk62M/image.png)

#### 3. instr(string,char):
```plsql
SELECT INSTR('Hello World', 'o') FROM dual;
```
##### Output:
![[dbms3.1.3.png]]

#### 4. length(n):
```plsql
SELECT LENGTH('Hello'); -- Returns 5
```
##### Output:
![[dbms3.1.4.png]]

#### 5. lpad(char1 ,n [,char2]):
```plsql
SELECT LPAD('abc', 5, '*') FROM dual;
```
##### Output:
![[dbms3.1.5.png]]

#### 6. ltrim(string [,char(s)]):
```plsql
SELECT LTRIM('   Hello   ') FROM dual;
```
##### Output:
![[dbms3.1.6.png]]

#### 7. rpad(char1 ,n [,char2]):
```plsql
SELECT RPAD('abc', 5, '*') FROM dual;
```
##### Output:
![[dbms3.1.7.png]]

#### 8. rtrim(string [,char(s)]):
```plsql
SELECT RTRIM('   Hello   ') FROM dual;
```

##### Output:
![[dbms3.1.8.png]]

#### 9. replace(char ,search_string , replacement_string):
```plsql
SELECT REPLACE('apple', 'p', 'b') "Changes" FROM dual;
```
Output:
![[dbms3.1.9.png]]

#### 10. substr(string ,position ,substring length):
```plsql
SELECT SUBSTR('Hello World', 1, 5) FROM dual; -- Returns 'Hello'
```
##### Output:
![imgbb](https://i.ibb.co/28Dd4QF/image.png)


#### 11. initcap(char):
```plsql
SELECT INITCAP('john doe') FROM dual;
```
##### Output:
![[dbms3.1.11.png]]


#### 12. lower(string):
```plsql
SELECT LOWER('Hello') FROM dual;
```
##### Output:
![[dbms3.1.12.png]]

#### 13. upper(string):
```plsql
SELECT UPPER('Hello') FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/Y7zmw40/image.png)

#### 14. translate(char ,from string ,to string):
```plsql
SELECT TRANSLATE('hello', 'el', 'XY') FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/pd5jNMG/image.png)

#### 15. abs(n):
```plsql
SELECT ABS(-10) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/n7Bw3gz/image.png)


#### 16. ceil(n):
```plsql
SELECT CEIL(4.3) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/SyFWPWJ/image.png)

#### 17. cos(n):
```plsql
SELECT COS(0) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/TB0CTpB/image.png)

#### 18. exp(n):
```plsql
SELECT EXP(2) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/Z1PJH14/image.png)

#### 19. floor(n):
```plsql
SELECT FLOOR(4.7) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/WPM0RcF/image.png)

#### 20. mod(m ,n):
```plsql
SELECT MOD(10, 3) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/vvjnTZG/image.png)

#### 21. power(x ,y):
```plsql
SELECT POW(2, 3) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/sPQ7Cdy/image.png)

#### 22. round(x ,y):
```plsql
SELECT ROUND(4.567, 2) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/H2Hpzqj/image.png)

#### 23. sign(n):
```plsql
SELECT SIGN(-7) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/XCmT6Rw/image.png)

#### 24. sqrt(n);
```plsql
SELECT SQRT(25) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/bs59hNq/image.png)

#### 25. trunc(x ,n):
```plsql
SELECT TRUNC(4.789, 2) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/k3s2K3F/image.png)

#### 26. sysdate:
```plsql
SELECT SYSDATE FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/NncvFyc/image.png)

#### 27. add_months(d ,n):
```plsql
SELECT ADD_MONTHS(SYSDATE, 3) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/y0HD7W1/image.png)

#### 28. last_day():
```plsql
SELECT LAST_DAY(SYSDATE) FROM dual; -- Returns last day of the current month
```
##### Output:
![imgbb](https://i.ibb.co/hs5kWpX/image.png)

#### 29. months_between(date1 ,date2):
```plsql
SELECT MONTHS_BETWEEN(SYSDATE, TO_DATE('2022-01-01', 'YYYY-MM-DD')) FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/HqKp42w/image.png)

#### 30. next_day(date ,char):
```plsql
SELECT NEXT_DAY(SYSDATE, 'FRIDAY') FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/z4RvS1P/image.png)

#### 31. greatest(expr):
```plsql
SELECT GREATEST(5, 8, 3) FROM dual;
```
#### Output:
![imgbb](https://i.ibb.co/JRJNyt0/image.png)

#### 32. least(expr):
```plsql
SELECT LEAST(5, 8, 3) FROM dual;
```
#### Output:
![imgbb](https://i.ibb.co/NVw456G/image.png)

### Q2) Display current time in hour: min: sec format:
```plsql
SELECT TO_CHAR(CURRENT_TIMESTAMP, 'HH MI SS') FROM dual;
```
#### Output:
![imgbb](https://i.ibb.co/jh3YY3W/image.png)

### Q3) Display salary + commission of emp table:
```plsql
SELECT Ename, Salary + NVL(Commission, 0) AS "Total Salary" FROM EmpTable;
```
#### Output:
![imgbb](https://i.ibb.co/3sCkM13/image.png)

### Q4) Store any date value in the hiredate column of the table:
```plsql
UPDATE EmpTable SET HireDate = SYSDATE WHERE EmpNo = 1;
```
#### Output:
![imgbb](https://i.ibb.co/WDkQXRY/image.png)

### Q5) Display names of employees who joined the company in 1985:
```plsql
SELECT Ename FROM EmpTable WHERE EXTRACT(YEAR FROM HireDate) = 1985;
```
#### Output:
![imgbb](https://i.ibb.co/QJHsqyH/image.png)

### Q6) Display names of employees who joined the company this year:
```plsql
SELECT Ename FROM EmpTable WHERE EXTRACT(YEAR FROM HireDate) = EXTRACT(YEAR FROM SYSDATE);
```
#### Output:
![imgbb](https://i.ibb.co/YbDXD5q/image.png)
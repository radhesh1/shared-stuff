
### Write queries to:
#### 1. Display the system date
```plsql
SELECT SYSDATE FROM dual;
```
###### Output:
![imgbb](https://i.ibb.co/5M8Dwxg/image.png)

#### 2. Display current day
```plsql 
SELECT TO_CHAR(SYSDATE, 'D') "DAY OF WEEK" FROM dual;
```
###### Output:
![imgbb](https://i.ibb.co/LxDVrfx/image.png)

#### 3. Display current month and spell out year
```plsql
SELECT TO_CHAR(SYSDATE,'MONTH YYYY') "Date Mark" FROM dual;
```
###### Output:
![imgbb](https://i.ibb.co/7KXwpWc/image.png)

#### 4. Display spell out current date
```plsql
SELECT TO_CHAR(SYSDATE, 'DAY, MONTH D, YYYY') FROM dual;
```
###### Output:
![imgbb](https://i.ibb.co/Y77Nkrn/image.png)

#### 5. Check whether it is AM or PM right now
```plsql
SELECT CASE WHEN TO_NUMBER(TO_CHAR(SYSDATE, 'HH24')) < 12 THEN 'AM' ELSE 'PM' END "AM OR PM" FROM dual;
```
###### Output:
![imgbb](https://i.ibb.co/L6mfykn/image.png)

#### 6. Display the date of next Friday
```plsql
SELECT NEXT_DAY(TRUNC(SYSDATE),'Friday') "Next Friday At"  FROM dual;
```
###### Output:
![imgbb](https://i.ibb.co/xF9sMFj/image.png)

#### 7. Round the system date on month
```plsql
SELECT ROUND(SYSDATE, 'MONTH') "Rounded Date to month" FROM dual;
```
###### Output:
![imgbb](https://i.ibb.co/nCGQMQy/image.png)

#### 8. Truncate the system date on month
```plsql
SELECT ROUND(SYSDATE, 'MONTH') "Rounded Date to month" FROM dual;
```
###### Output:
![imgbb](https://i.ibb.co/vDKzR4B/image.png)

#### 9. Round the system date on year
```plsql
SELECT ROUND(SYSDATE, 'YEAR') "Rounded Date to year" FROM dual;
```
###### Output:
![imgbb](https://i.ibb.co/QnFn2yt/image.png)

#### 10. Truncate the system date on year
```plsql
SELECT TRUNC(SYSDATE, 'YEAR') "Truncated Date to year" FROM dual;
```
###### Output:
![imgbb](https://i.ibb.co/MGT37RS/image.png)

#### 11. Find the day after three days
```plsql
SELECT TO_CHAR(SYSDATE+3,'DAY') "Day after 3 Days" FROM dual;
```
###### Output:
![imgbb](https://i.ibb.co/hYPg3yv/image.png)

### Queries Based on EMP table (Assignment 2)

#### 12. Display day of date of joining column
```plsql
SELECT EmpNo,TO_CHAR(DateofJoining, 'DAY') "Day Of Joining" FROM EmpTable;
```
###### Output:
![imgbb](https://i.ibb.co/m9tqxQh/image.png)

#### 13. Display those employees who join the company on Monday
```plsql
SELECT * FROM EmpTable WHERE TO_CHAR(DateofJoining,'DAY')='MONDAY';
```
###### Output:
![imgbb](https://i.ibb.co/gdCBBqP/image.png)

#### 14. Display those employees who join the company this month
```plsql
SELECT * FROM EmpTable WHERE TO_NUMBER(TO_CHAR(DateofJoining,'MM'))=TO_NUMBER(TO_CHAR(SYSDATE,'MM'));
```

###### Output:
![imgbb](https://i.ibb.co/PxXYGWX/image.png)

##### 15. Display those employees who join the company in last 30 days
```plsql
SELECT * FROM EmpTable WHERE DateofJoining >= SYSDATE - INTERVAL '30' DAY AND DateofJoining <= SYSDATE;
```
###### Output:
![imgbb](https://i.ibb.co/ZBpV03p/image.png)

### Create a table train having three four columns
##### 16. Train Number, date of Departure, time of departure, time of arrival
```plsql
DROP TABLE train;
CREATE TABLE train (
	train_number NUMBER,
	date_of_departure DATE,
	time_of_departure TIMESTAMP,
	time_of_arrival TIMESTAMP
);
DESC train;
```
###### Output:
![imgbb](https://i.ibb.co/BLTns8q/image.png)

##### 17. Insert five columns in train table
```plsql
INSERT INTO train (train_number, date_of_departure, time_of_departure, time_of_arrival) VALUES (101, TO_DATE('2024-01-22','YYYY-MM-DD'), TO_TIMESTAMP('08:00:00','HH24:MI:SS'), TO_TIMESTAMP('12:00:00','HH24:MI:SS'));
INSERT INTO train (train_number, date_of_departure, time_of_departure, time_of_arrival) VALUES (102, TO_DATE('2024-01-23','YYYY-MM-DD'), TO_TIMESTAMP('14:00:00','HH24:MI:SS'), TO_TIMESTAMP('18:00:00','HH24:MI:SS'));
INSERT INTO train (train_number, date_of_departure, time_of_departure, time_of_arrival) VALUES (103, TO_DATE('2024-01-24','YYYY-MM-DD'), TO_TIMESTAMP('10:00:00','HH24:MI:SS'), TO_TIMESTAMP('15:00:00','HH24:MI:SS'));
INSERT INTO train (train_number, date_of_departure, time_of_departure, time_of_arrival) VALUES (104, TO_DATE('2024-01-25','YYYY-MM-DD'), TO_TIMESTAMP('12:00:00','HH24:MI:SS'), TO_TIMESTAMP('17:00:00','HH24:MI:SS'));
INSERT INTO train (train_number, date_of_departure, time_of_departure, time_of_arrival) VALUES (105, TO_DATE('2024-01-26','YYYY-MM-DD'), TO_TIMESTAMP('16:00:00','HH24:MI:SS'), TO_TIMESTAMP('20:00:00','HH24:MI:SS'));
```
###### Output:
![imgbb](https://i.ibb.co/Hq88M0j/image.png)

##### 18. Display all the records
```plsql
SELECT * FROM train;
```
###### Output:
![imgbb](https://i.ibb.co/HNX48Xx/image.png)

##### 19. Display the time values inserted in the columns
```plsql
SELECT train_number, TO_CHAR(time_of_departure, 'HH12:MI:SS AM') "Departure time", TO_CHAR(time_of_arrival, 'HH12:MI:SS AM') "Arrival time" FROM train;
```
###### Output:
![imgbb](https://i.ibb.co/F3BJ0qm/image.png)

##### 20. Display those trains which arrived on PM
```plsql
SELECT * FROM train WHERE TO_NUMBER(TO_CHAR(time_of_arrival,'HH24')) > 12;
```
###### Output:
![imgbb](https://i.ibb.co/SPD7Hh5/image.png)

##### 21. Display train number who are going to depart in next on hou
```plsql
SELECT train_number FROM train WHERE time_of_departure BETWEEN SYSTIMESTAMP AND SYSTIMESTAMP + INTERVAL '1' HOUR;
```
###### Output:
![imgbb](https://i.ibb.co/MN2thJN/image.png)

Name: Radhesh Goel
Group: 2QN1
Roll No: 102203952
### Pre-Requisite
Based on EmpTable
Columns are EmpNo, Ename, Job, Salary, Commission, DeptNo Insert 5 records by storing Null value in some records for commission column.
```plsql
-- Creating the EmpTable table
DROP TABLE EmpTable;
CREATE TABLE EmpTable (
	EmpNo varchar(4) NOT NULL PRIMARY KEY,
	Ename varchar(30),
	Job varchar(20),
	Salary int,
	Commission int,
	DeptNo varchar(3),
	HireDate date,
	DateofJoining date
);

-- Addition of data
INSERT INTO EmpTable (EmpNo, Ename, Job, Salary, Commission, DeptNo, HireDate, DateofJoining) VALUES ('1', 'John Doe', 'Manager', 5000, NULL, '10', TO_DATE('1985-10-10', 'YYYY-MM-DD'), TO_DATE('1985-10-11', 'YYYY-MM-DD'));
    INSERT INTO EmpTable (EmpNo, Ename, Job, Salary, Commission, DeptNo, HireDate, DateofJoining) VALUES ('2', 'Raj Smith', 'Clerk', 3000, 200, '20', TO_DATE('1984-12-24', 'YYYY-MM-DD'), TO_DATE('1984-12-29', 'YYYY-MM-DD'));
    INSERT INTO EmpTable (EmpNo, Ename, Job, Salary, Commission, DeptNo, HireDate, DateofJoining) VALUES ('3', 'Abigail Johnson', 'Salesperson', 400, NULL, '20', TO_DATE('2024-01-21', 'YYYY-MM-DD'), TO_DATE('2024-01-24', 'YYYY-MM-DD'));
    INSERT INTO EmpTable (EmpNo, Ename, Job, Salary, Commission, DeptNo, HireDate, DateofJoining) VALUES ('4', 'Alice Brown', 'Manager', 5500, NULL, '10', TO_DATE('2023-06-15', 'YYYY-MM-DD'), TO_DATE('2023-06-18', 'YYYY-MM-DD'));
    INSERT INTO EmpTable (EmpNo, Ename, Job, Salary, Commission, DeptNo, HireDate, DateofJoining) VALUES ('5', 'Charlie Green', 'Analyst', 4500, 150, '30', TO_DATE('2023-12-23', 'YYYY-MM-DD'), TO_DATE('2023-12-25', 'YYYY-MM-DD'));
```
##### Output:
![imgbb](https://i.ibb.co/NnQRFq3/image.png)

### Q1) Get employee no and employee name who works in dept no 10 ?
```plsql
SELECT EmpNo,Ename FROM EmpTable WHERE DeptNo=10;
```
##### Output:
![imgbb](https://i.ibb.co/HDjMSPY/image.png)

### Q2) Display the employee names of those clerks whose salary> 2000 ?
```plsql
SELECT Ename FROM EmpTable WHERE Salary>2000;
```
##### Output:
![imgbb](https://i.ibb.co/9g6R0Bd/image.png)

### Q3) Display name and job of Salesperson & Clerks ?
```plsql
SELECT Ename,Job FROM EmpTable WHERE Job IN ('Salesperson','Clerk');
```
##### Output:
![imgbb](https://i.ibb.co/NSjqCdg/image.png)

### Q4) Display all details of employees whose salary between 2000 and 3000 ?
```plsql
SELECT * FROM EmpTable WHERE Salary BETWEEN 2000 AND 3000;
```
##### Output:
![imgbb](https://i.ibb.co/Fn7nFCq/image.png)

### Q5) Display all details of employees whose dept no is 10, 20, or 30 ?
```plsql
SELECT * FROM EmpTable WHERE DeptNo IN (10,20,30);
```
##### Output:
![imgbb](https://i.ibb.co/JCmGXZR/image.png)

### Q6) Display name of those employees whose commission is NULL ?
```plsql
SELECT Ename FROM EmpTable WHERE Commission IS NULL;
```
##### Output:
![imgbb](https://i.ibb.co/25BYm5y/image.png)

### Q7) Display dept no & salary in ascending order of dept no and with in each dept no salary should be in descending order ?
```plsql
SELECT DeptNo,Salary FROM EmpTable ORDER BY DeptNo ASC,Salary DESC;
```
##### Output:
![imgbb](https://i.ibb.co/gd3gQJL/image.png)

### Q8) Display name of employees having two ‘a’ or ‘A’ chars in the name 
```plsql
SELECT Ename FROM EmpTable WHERE LOWER(Ename) LIKE '%a%a%';
```
##### Output:
![imgbb](https://i.ibb.co/dcf5p8G/image.png)

### Q9) Display the name of the employees whose second char is ‘b’ or ‘B’ ?
```plsql
SELECT Ename FROM EmpTable WHERE SUBSTR(Ename, 2, 1) IN ('b', 'B');
```
##### Output:
![imgbb](https://i.ibb.co/dcf5p8G/image.png)

### Q10) Display the name of the employees whose first or last char is ‘a’ or ‘A’ ?
```plsql
SELECT Ename FROM EmpTable WHERE SUBSTR(Ename,-1,1) IN ('a','A') OR SUBSTR(Ename,1,1) IN ('a','A');
```
##### Output:
![imgbb](https://i.ibb.co/LY7b9bg/image.png)

### Q11) Display maximum, minimum, average salary of deptno 10 employees.
```plsql
SELECT MAX(Salary) AS "Maximum Salary",MIN(Salary) AS "Minimum Salary",AVG(Salary) AS "Average Salary" FROM EmpTable WHERE DeptNo=10;
```
##### Output:
![imgbb](https://i.ibb.co/cvBP4n1/image.png)


### Q12) Display total number of employees working in deptno 20 
```plsql
SELECT COUNT(*) AS "Total Employees" FROM EmpTable WHERE DeptNo=20;
```
##### Output:
![imgbb](https://i.ibb.co/5LyhS2y/image.png)


### Q13) Display total salary paid to clerks
```plsql
SELECT SUM(SALARY) AS "Salary Paid to Clerks" FROM EmpTable WHERE Job='Clerk';
```
##### Output:
![imgbb](https://i.ibb.co/vkx1tfj/image.png)


### Q14) Display system date
```plsql
SELECT CURDATE() AS "Current Date" FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/wcVDJTx/image.png)


### Q15) Display the result of (1212)/13
```plsql
SELECT 1212 / 13 AS result FROM dual;
```
##### Output:
![imgbb](https://i.ibb.co/gRJy8Dv/image.png)


### Q16) Display info of ‘raj’ irrespective to the case in which the data is stored.
```plsql
SELECT * FROM EmpTable WHERE LOWER(Ename)='raj%';
```
##### Output:
![imgbb](https://i.ibb.co/mFm3qmh/image.png)

Name: Radhesh Goel
Group: 2QN1
Roll No: 102203952
### 1. Create table Student (Rno, Name, DOB, Gender, Class, College, City, Marks)
```plsql
DROP TABLE Student;
CREATE TABLE Student(
    Rno number NOT NULL PRIMARY KEY,
    Name varchar2(30),
    DOB date,
    Gender varchar2(1),
    Class varchar2(4),
    College varchar2(30),
	City varchar2(40),
    Marks number(5,2)
);
```
##### Output
![imgbb](https://i.ibb.co/SBN00ZN/image.png)

### 2. Insert 5 records in student table
```plsql 
INSERT INTO Student (Rno,Name,DOB,Gender,Class,College,City,Marks) VALUES(1,'Manish', TO_DATE('2004-3-28', 'YYYY-MM-DD'),'M','1C01','Thapar University','Patiala','35');
INSERT INTO Student (Rno,Name,DOB,Gender,Class,College,City,Marks) VALUES(2,'Rachel', TO_DATE('2004-8-21', 'YYYY-MM-DD'),'F','1C01','GNDU','Amritsar','82');
INSERT INTO Student (Rno,Name,DOB,Gender,Class,College,City,Marks) VALUES(3,'John', TO_DATE('2004-4-26', 'YYYY-MM-DD'),'M','1C02','University of Cambridge','Cambridge','99');
INSERT INTO Student (Rno,Name,DOB,Gender,Class,College,City,Marks) VALUES(5,'Yogesh', TO_DATE('2004-6-23', 'YYYY-MM-DD'),'F','1C02','IIT','Delhi','2');
INSERT INTO Student (Rno,Name,DOB,Gender,Class,College,City,Marks) VALUES(9,'Rohan', TO_DATE('2004-2-19', 'YYYY-MM-DD'),'M','1C01','Thapar University','Patiala','56');
```
##### Output
![imgbb](https://i.ibb.co/qk0VXtL/image.png)

### 3. Display the information of all the students
```plsql
SELECT * FROM Student;
```
##### Output:
![imgbb](https://i.ibb.co/HhGSq0k/image.png)


### 4. Display the detail structure of student table
```plsql
DESC Student;
```
##### Output:
![imgbb](https://i.ibb.co/RSqkxXy/image.png)

### 5. Display Rno, Name and Class information of ‘Patiala’
```plsql
SELECT Rno,Name,Class FROM Student WHERE Class='Patiala';
```
##### Output:
![imgbb](https://i.ibb.co/Fn57T3y/image.png)


### 6. Display information on ascending order of marks
```plsql
SELECT * FROM Student ORDER BY Marks ASC;
```
##### Output:
![imgbb](https://i.ibb.co/y4YN3y4/image.png)


### 7. Change the marks of Rno 5 to 89.
```plsql
UPDATE Student SET Marks=89 WHERE Rno=5;
```
##### Output:
![imgbb](https://i.ibb.co/bBzsS90/image.png)

### 8. Change the name and city of Rno 9.
```plsql
UPDATE Student SET Name='Raghav',City='Delhi' WHERE Rno=9;
```
##### Output:
![imgbb](https://i.ibb.co/sQDp5L9/image.png)

### 9. Delete the information of ‘Amritsar’ city records
```mysql
DELETE Student WHERE City='Amritsar';
```
##### Output:
![imgbb](https://i.ibb.co/31YsbPf/image.png)

### 10. Delete the records of student where marks<3
```plsql
DELETE Student WHERE Marks>3;
```
##### Output:
![imgbb](https://i.ibb.co/YTVN2yG/image.png)
### 1. Create the following tables and insert some tuples in these tables the primary key for the Sailors bid are the foreign keys for the respectively.
with following tables
     Sailors     (sid: integer
    Boats        (bid: integer
    Reserves  (sid: integer
After inserting the records in these table,the instances should look like as follows:
![[dbms5-1.0.png]]
```plsql
	DROP TABLE Sailors CASCADE CONSTRAINTS;
	DROP TABLE Boats CASCADE CONSTRAINTS;
	DROP TABLE Reserves CASCADE CONSTRAINTS;
	
    -- creation of structure
    CREATE TABLE Sailors(
        sid int PRIMARY KEY,
        sname varchar2(10),
        rating int,
        age decimal(10,2)
    );

    CREATE TABLE Boats(
    bid int PRIMARY KEY,
    bname varchar2(10),
    color varchar2(10)
    );

    CREATE TABLE Reserves(
    sid int,
    bid int,
    day date,
    FOREIGN KEY (sid) REFERENCES Sailors(sid),
    FOREIGN KEY (bid) REFERENCES Boats(bid)
    );

    -- addition of data
    INSERT INTO Sailors(sid,sname,rating,age) VALUES (22,'Dustin',7,45.0);
    INSERT INTO Sailors(sid,sname,rating,age) VALUES (29,'Brutus',1,33.0);
    INSERT INTO Sailors(sid,sname,rating,age) VALUES (31,'Lubber',8,55.5);
    INSERT INTO Sailors(sid,sname,rating,age) VALUES (32,'Andy',8,25.5);
    INSERT INTO Sailors(sid,sname,rating,age) VALUES (58,'Rusty',10,35.0);
    INSERT INTO Sailors(sid,sname,rating,age) VALUES (64,'Horatio',7,35.0);
    INSERT INTO Sailors(sid,sname,rating,age) VALUES (71,'Zorba',10,16.0);
    INSERT INTO Sailors(sid,sname,rating,age) VALUES (74,'Horatio',9,35.0);
    INSERT INTO Sailors(sid,sname,rating,age) VALUES (85,'Art',3,25.5);
    INSERT INTO Sailors(sid,sname,rating,age) VALUES (95,'Bob',3,63.5);

    INSERT INTO Boats(bid, bname, color) VALUES (101, 'Interlake', 'blue');
    INSERT INTO Boats(bid, bname, color) VALUES (102, 'Interlake', 'red');
    INSERT INTO Boats(bid, bname, color) VALUES (103, 'Clipper', 'green');
    INSERT INTO Boats(bid, bname, color) VALUES (104, 'Marine', 'red');

     INSERT INTO Reserves(sid,bid,day) VALUES (22,101,TO_DATE('1998-10-10','YYYY-MM-DD'));
     INSERT INTO Reserves(sid,bid,day) VALUES (22,102,TO_DATE('1998-10-10','YYYY-MM-DD'));
     INSERT INTO Reserves(sid,bid,day) VALUES (22,103,TO_DATE('1998-08-10','YYYY-MM-DD'));
     INSERT INTO Reserves(sid,bid,day) VALUES (22,104,TO_DATE('1998-07-10','YYYY-MM-DD'));
     INSERT INTO Reserves(sid,bid,day) VALUES (31,102,TO_DATE('1998-10-11','YYYY-MM-DD'));
     INSERT INTO Reserves(sid,bid,day) VALUES (31,103,TO_DATE('1998-06-11','YYYY-MM-DD'));
     INSERT INTO Reserves(sid,bid,day) VALUES (31,104,TO_DATE('1998-12-11','YYYY-MM-DD'));
     INSERT INTO Reserves(sid,bid,day) VALUES (64,101,TO_DATE('1998-05-09','YYYY-MM-DD'));
     INSERT INTO Reserves(sid,bid,day) VALUES (64,102,TO_DATE('1998-08-09','YYYY-MM-DD'));
     INSERT INTO Reserves(sid,bid,day) VALUES (74,103,TO_DATE('1998-08-09','YYYY-MM-DD'));

    SELECT * FROM Boats;
    SELECT * FROM Reserves;
    SELECT * FROM Sailors;
```
#### Output:
![imgbb](https://i.ibb.co/LPrZF4H/image.png)
![imgbb](https://i.ibb.co/6YvjDjx/image.png)
![imgbb](https://i.ibb.co/c1tzXDb/image.png)

### 2. Write SQL command for the following:
####  i) Show the names and ages of all sailors.
```plsql
SELECT sname,age FROM Sailors;
```
#### Output:
![imgbb](https://i.ibb.co/d5ykhvs/image.png)

#### ii) Show the details of the boats which are red and blue in color.
```plsql
SELECT * FROM Sailors WHERE color IN ('red','blue');
```
##### Output:
![imgbb](https://i.ibb.co/NKgm9xT/image.png)

#### iii) Find the oldest and youngest sailors' age.
```plsql
SELECT MAX(age) AS "Oldest Sailor",MAX(age) AS "Youngest Sailor" FROM Sailors;
```
##### Output:
![imgbb](https://i.ibb.co/HhLnGnq/image.png)

#### iv) Find the ages of sailors whose name begins and ends with B and has at least three characters.
```plsql
SELECT age FROM Sailors WHERE LENGTH(sname)>=3 AND LOWER(SUBSTR(sname,-1,1))='b'  AND LOWER(SUBSTR(sname,1,1))='b' ;
```
##### Output:
![imgbb](https://i.ibb.co/fGzPbfM/image.png)

#### v) Show the average rating of the sailors. 
```plsql
SELECT AVG(rating) AS "Average Rating" FROM Sailors;
```
##### Output:
![imgbb](https://i.ibb.co/rt805cD/image.png)

#### vi) Find all the sailors with a rating above 7.
```plsql
SELECT * FROM Sailors WHERE rating>7;
```
##### Output:
![imgbb](https://i.ibb.co/3mf2gVg/image.png)

#### vii) Find the number of boats reserved by  sailor named Horatio.
```plsql
SELECT COUNT(*) AS "Number of Boats" FROM Sailors WHERE sname='Horatio';
```
##### Output:
![imgbb](https://i.ibb.co/T8PwqNb/image.png)

#### viii) Find the colors of boats reserved by Lubber.
```plsql
SELECT color FROM Boats b JOIN Reserves r ON b.bid=r.bid JOIN Sailors s ON r.sid=s.sid WHERE s.sname='Lubber'; 
```
##### Output:
![imgbb](https://i.ibb.co/B3XFNgS/image.png)

#### ix) Show the details of the sailors who have reserved the boat with bid 102.
```plsql
SELECT * FROM Sailors s JOIN Reserves r ON s.sid=r.sid WHERE r.bid=102;
```
##### Output:
![imgbb](https://i.ibb.co/LPYpf97/image.png)

#### x) Find the sid of sailors who have reserved green boats.
```plsql
SELECT sid FROM Sailors s JOIN Reserves r ON r.sid=s.sid JOIN Boats b ON r.bid=b.bid WHERE color='green';
```
##### Output:
![imgbb](https://i.ibb.co/41dzR3V/image.png)

#### xi) Find the names of sailors who have reserved boat number 103.
```plsql
SELECT sname FROM Sailors s JOIN Reserves r ON s.sid=r.sid WHERE r.bid=103;
```
##### Output:
![imgbb](https://i.ibb.co/NtDVNd8/image.png)
#### xii) Find the names of sailors who have reserved a red boat.
```plsql
SELECT sname FROM Sailors s JOIN Reserves r ON s.sid=r.sid JOIN Boats b ON r.bid=b.bid WHERE b.color='red';
```
##### Output:
![imgbb](https://i.ibb.co/NtDVNd8/image.png)
#### xiii) Find the names of sailors who have reserved a green or a blue boat.
```plsql
SELECT sname FROM Sailors s JOIN Reserves r ON s.sid=r.sid JOIN Reserves r ON r.bid=b.bid WHERE b.color IN ('green','blue');
```
##### Output:
![imgbb](https://i.ibb.co/NtDVNd8/image.png)

#### xiv) Find the names of sailors who have reserved both a red and a green boat.
```plsql
SELECT sname FROM Sailors s JOIN Reserves r ON s.sid=r.sid JOIN Reserves r ON r.bid=b.bid WHERE b.color IN ('red','green'); GROUP BY s.sid,s.sname HAVING COUNT(DISTINCT(b.color))=2;
```
##### Output:
![imgbb](https://i.ibb.co/h2MFP5h/image.png)

#### xv) Find the names of sailors who have reserved at least one boat.
```plsql
SELECT DISTINCT s.sname FROM Sailors s JOIN Reserves r ON r.sid=s.sid;
```
##### Output:
![imgbb](https://i.ibb.co/WWXkHPP/image.png)

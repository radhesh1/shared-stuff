(Continued from Lab Assignment-4)
### Q1) Check the structure of tables.
```plsql
DESC employees;
DESC book;
DESC ST;
DESC S;
DESC P;
DESC SP;
DESC dept;
DESC emp;
```
#### Output:
![imgbb](https://i.ibb.co/mhY7DKc/image.png)
![imgbb](https://i.ibb.co/r29ngfy/image.png)
![imgbb](https://i.ibb.co/ZSHMcbh/image.png)
![imgbb](https://i.ibb.co/pJF2c3D/image.png)
![imgbb](https://i.ibb.co/wMdV6sc/image.png)
![imgbb](https://i.ibb.co/X23KH1H/image.png)
![imgbb](https://i.ibb.co/8PFmfhp/image.png)
![imgbb](https://i.ibb.co/BztXgzw/image.png)
### Q2) Check the constraint name for applied constraints?
```plsql
SELECT * FROM user_cons_columns WHERE table_name = 'EMPLOYEES';
SELECT * FROM user_cons_columns WHERE table_name = 'BOOK';
SELECT * FROM user_cons_columns WHERE table_name = 'ST';
SELECT * FROM user_cons_columns WHERE table_name = 'S';
SELECT * FROM user_cons_columns WHERE table_name = 'P';
SELECT * FROM user_cons_columns WHERE table_name = 'SP';
SELECT * FROM user_cons_columns WHERE table_name = 'EMP';
```
#### Output:
![imgbb](https://i.ibb.co/HxvKvYD/image.png)
![imgbb](https://i.ibb.co/gMPFG5j/image.png)
![imgbb](https://i.ibb.co/jvbYk6F/image.png)
![imgbb](https://i.ibb.co/Jmkn6qQ/image.png)
![imgbb](https://i.ibb.co/NxDPhCx/image.png)
![imgbb](https://i.ibb.co/JFq6SSL/image.png)
![imgbb](https://i.ibb.co/gzbXqD7/image.png)

### Q3) Drop the unique constraint on ENAME
```plsql
ALTER TABLE EMP1 DROP CONSTRAINT SYS_C00147978439;
```
#### Output:
![imgbb](https://i.ibb.co/xFkDPZS/image.png)

### Q4) Drop the Foreign Key constraint on DEPTNO
```plsql
ALTER TABLE emp1 DROP CONSTRAINT SYS_C00147978440;
```
#### Output:
![imgbb](https://i.ibb.co/LSXtPF9/image.png)


### Q5) Add Foreign Key constraint on DEPTNO
```plsql
ALTER TABLE emp ADD CONSTRAINT fk_emp_deptno FOREIGN KEY (deptno) REFERENCES dept(deptno);
```
#### Output:
![imgbb](https://i.ibb.co/QbDCqvZ/image.png)

### Q6) Change Data type of ENAME
```plsql
ALTER TABLE emp1 MODIFY ename varchar(35);
```
#### Output:
![imgbb](https://i.ibb.co/xGswD1B/image.png)

### Q7) Change width of DNAME
```plsql
ALTER TABLE dept MODIFY dname varchar(50);
```
#### Output:
![imgbb](https://i.ibb.co/mtpfyM2/image.png)

### Q8) Add COMM column in EMP table
```plsql
ALTER TABLE emp1 ADD comm int;
```
#### Output:
![[dbms7-8.png]]

### Q9) Drop CITY column from J table
```plsql
ALTER TABLE J DROP city;
```
#### Output:
![[dbms7-9.png]]

### Q10) Create duplicate copy of EMP table
```mysql
CREATE TABLE emp_copy AS SELECT * FROM emp;
```
#### Output:
![[dbms7-10.png]]

### Q11) Copy structure of DEPT table in another table with different column names
```mysql
CREATE TABLE NEW_DEPT AS SELECT
    deptno AS DEPARTMENT_ID,
    dname AS DEPARTMENT_NAME
FROM dept;
```
#### Output:
![[dbms7-11.png]]

### Q12) Change the name and job of the employee whose EMPNO =100
```mysql
UPDATE SET ename='Jack',job='Lect' WHERE empno=100;
```
#### Output:
![[dbms7-12.png]]

### Q13) Delete the record of employee who belongs to computer department
```mysql
DELETE FROM emp WHERE deptno = (SELECT deptno FROM dept WHERE dname = 'comp');
```
#### Output:
![[dbms7-13.png]]

### Q14) Drop DEPT Table
```mysql
DROP TABLE NEW;
```
#### Output:
![[dbms7-14.png]]

### Q15) Drop duplicate table of EMP table
```mysql
DROP TABLE emp_copy;
```
#### Output:
![[dbms7-14.png]]
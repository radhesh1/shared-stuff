## Q1. Discuss the concept of Networking, advantages, disadvantages and applications.
It means the interconnection of computing devices that exchange data and share resources with each other. These networked devices use a system of rules, <span style="color: 0070c0;">communication protocols</span>, to transmit information over physical or wireless technologies.
#### Advantages 
1. Resource Sharing 
2. Communication
3. Remote Access
4. Data Security
5. Scalability
6. Cost Efficiency
#### Disadvantages
1.  Security Concerns
2. Complexity
3. Reliability Issues
4. Cost of Implementation
5. Compatibility
#### Application
1. Internet
2. Intranet
3. Social Networking
4. Cloud Computing
5. Usage based networks
## Q2. Discuss the peer-to-peer connections and multipoint connection.
```sheet
{
	classes:{
		class:{
		}
	}
}
---
| Peer-to-Peer Connection | Multipoint Connection |
| ----------------------- | --------------------- |
| Channel is shared between 2 devices | Channel is shared between multiple |
| Dedicated link between the two | A link is provided at all times which is shared among nodes |
| The entire capacity is reserved between the two with the possibility of wastage | The entire capacity is not r |
| One transmitter-reciever | Multiple transmitter-reciever|
| Smallest distance is most important | Smallest distance isn't important |
| Very secure and private as channel isn't shared | Not secure and private as channel is shared |
```
## Q3. Discuss the components required to make a computer network.
#### 1. **NIC(Network Interface Card/Network adapter/Network Interface Controller)**
It is a hardware component that allows a computer to connect to network. These are essential to communicate with <span title="other computers,  servers, routers, and switches" style="border-bottom: 1px dashed #999; cursor: help;color: #92d050;">other devices</span> on a network

These are wired and wireless, and support <span title="Ethernet, Wi-Fi, Bluetooth, and Fiber channels" style="border-bottom: 1px dashed #999; cursor: help;color: #92d050;">different protocols</span>. A wired NIC typically uses RJ45 connector ,whereas a wireless Nic uses antennas.

A NIC contains a network controller that manages the communication between the computer and the network. The controller uses software drivers to interpret the network protocols and to control the flow of data between the computer and the network. The drivers are typically installed automatically when the NIC is connected to the computer, but they can also be installed manually.

NICs can provide various features and capabilities, such as bandwidth management, quality of service (QoS), virtual LAN (VLAN) tagging, and network booting. Bandwidth management allows the NIC to prioritize network traffic and allocate bandwidth to different applications and users. QoS allows the NIC to ensure that certain types of traffic, such as voice and video, receive higher priority than other traffic. VLAN tagging allows the NIC to separate network traffic into different virtual networks, which can improve security and performance. Network booting allows the NIC to boot the computer from a network server, which can simplify system management and deployment.
#### 2) Hub

A hub is a basic networking device that allows multiple devices to communicate with each other. Hubs receive data packets from one device and broadcast them to all the other devices connected to the hub. This means that all devices on the network share the same bandwidth and receive all data, regardless of whether it is intended for them or not.

There are mainly three types of hubs: passive, active, and intelligent.

- **Passive hub:** A passive hub simply connects all devices together, without any signal amplification or regeneration. It has no power source and does not boost the signal, so it is not suitable for long-distance connections or large networks. A passive hub is mainly used to extend the number of ports available on a network.
- **Active hub/Powererd hub:**  It has a power source and can regenerate and amplify the signal. This means that it can transmit data over longer distances and support more devices than a passive hub. It also has built-in circuitry that helps to prevent collisions, which occur when two or more devices attempt to transmit data at the same time.
- **Intelligent hub/Managed hub:** It provides additional features and capabilities beyond those of a passive or active hub. It can monitor and manage network traffic, prioritize data transmission, and provide network security features. It can also support VLANs, which allow a single physical network to be divided into multiple logical networks.
#### 3) Switch 
It connects multiple devices in a network, allowing them to communicate with each other. Unlike a hub, which broadcasts data to all devices on the network, a switch forwards data only to the intended recipient, which reduces network congestion and improves performance. A switch can also provide additional security features and management capabilities, which makes it ideal for larger networks.

There are several types of switches, which include:
- **Unmanaged switch:** An unmanaged switch is a basic switch that does not have any management features. It is simply plug-and-play, which means it can be connected to the network and used immediately. An unmanaged switch is ideal for small networks where management features are not required.
- **Managed switch:** A managed switch is a more advanced switch that provides additional features and capabilities. It can be configured and managed using a web-based interface or a command-line interface. A managed switch can provide features such as VLANs, Quality of Service (QoS), and Spanning Tree Protocol (STP), which improve network management and security.
- **PoE switch:** A Power over Ethernet (PoE) switch is a switch that provides power to PoE-enabled devices, such as IP cameras, wireless access points, and VoIP phones. This eliminates the need for separate power supplies for these devices, which makes installation and management easier.

#### 4) Repeater

A repeater is a networking device and of the components of computer network that is used to regenerate and amplify signals in a network. When data is transmitted over a network, the signal weakens as it travels through the network cables. A repeater receives the weak signal and regenerates it, amplifying the signal to its original strength. This allows the signal to be transmitted over longer distances without loss of quality.

There are mainly two types of repeaters: analog and digital.

- **Analog repeater:** An analog repeater is used to regenerate and amplify analog signals. Analog signals are continuous signals that vary in amplitude or frequency over time. Examples of analog signals include voice, video, and radio signals. An analog repeater receives the weak analog signal and regenerates it by boosting the signal strength, which helps to extend the signal range.
- **Digital repeater:** A digital repeater is used to regenerate and amplify digital signals. Digital signals are discrete signals that represent data as a series of 0s and 1s. Examples of digital signals include Ethernet, Wi-Fi, and Bluetooth signals. A digital repeater receives the weak digital signal and regenerates it by decoding the digital signal, amplifying the signal strength, and re-encoding the signal. This helps to extend the signal range and prevent data loss.

There are also specialized types of repeaters that are used in specific applications:

- **Wireless repeater:** A wireless repeater is used to extend the range of a wireless network. It receives the wireless signal and rebroadcasts it, helping to improve coverage in areas where the signal is weak.
- **Ethernet repeater:** An Ethernet repeater, also known as a hub, is used to extend the range of an Ethernet network. It receives the Ethernet signal and broadcasts it to all devices on the network.
- **Fiber optic repeater:** A fiber optic repeater is used to extend the range of a fiber optic network. It receives the optical signal and regenerates it, amplifying the signal strength and extending the signal range.

#### 5) Router

A router is a networking device or Components of computer network that connects multiple networksmanaged together, allowing them to communicate with each other. A router operates at the Network layer of the OSI model and uses IP addresses to forward data packets between networks. Routers provide several benefits over other networking devices, such as better security, improved network performance, and scalability.

There are several types of routers, which include:

- **Residential router:** A residential router is a router that is designed for home use. It is typically provided by an Internet Service Provider (ISP) and is used to connect devices in a home network to the Internet. A residential router may provide features such as wireless connectivity, firewall protection, and Quality of Service (QoS).
- **Enterprise router:** An enterprise router is a router that is designed for use in large organizations. It provides advanced features and capabilities, such as multiple WAN ports, Virtual Private Network (VPN) connectivity, and traffic shaping. An enterprise router is typically managed by a network administrator and may be used to connect multiple locations together.
- **Core router:** A core router is a router that is used to connect multiple networks together at the core of a large network, such as an Internet Service Provider’s network. A core router is designed for high-speed data transfer and provides advanced features such as packet filtering, load balancing, and routing protocols.
- **Edge router:** An edge router is a router that is used to connect a local network to an external network, such as the Internet. It provides security features such as firewall protection, Network Address Translation (NAT), and Access Control Lists (ACLs).
- **Virtual router:** A virtual router is a software-based router that is used in virtualized environments, such as cloud computing. It provides the same routing functionality as a physical router but is hosted on a virtual machine.
- **Wireless router:** A wireless router is a router that provides wireless connectivity to devices in a network. It may use Wi-Fi or Bluetooth technology to provide wireless connectivity.

#### 6) Modem

A modem (short for modulator-demodulator) is a networking device or Components of computer network that converts digital data to analog signals for transmission over a telephone line or cable. It is used to connect a computer or network to the Internet through a service provider’s network. A modem converts digital signals from a computer or network into analog signals that can be transmitted over the telephone or cable line. When the analog signals reach the other end of the line, the modem on the other end converts the analog signals back into digital signals that can be understood by the receiving computer or network. There are different types of modems, including cable modems, DSL modems, and dial-up modems.

#### 7) Server

A server is a computer system or program that provides a service to other computers or programs on a network. Servers can be used to store files, manage resources, provide access to applications, and host websites. There are different types of servers, including file servers, print servers, web servers, mail servers, and database servers. Servers are typically more powerful and have more storage capacity than regular desktop computers. They are designed to handle multiple requests from client computers or other servers simultaneously, and they may also have features such as redundancy and failover capabilities for improved reliability.

#### 8) Bridge

A bridge is a networking device or Components of computer network that connects two or more network segments together, allowing devices on different segments to communicate with each other. A bridge operates at the Data Link layer of the OSI model and uses MAC addresses to forward data packets between network segments. A bridge can be used to segment a network, improve network performance, and improve network security. There are different types of bridges, including local bridges, remote bridges, and wireless bridges. A local bridge connects two network segments within the same physical location, while a remote bridge connects two network segments in different physical locations. A wireless bridge is a type of bridge that uses Wi-Fi technology to connect two wireless networks together.
## Q4. Discuss the types of networks as LAN, WAN and MAN.

####  [LAN (Local Area Networks)](https://en.wikipedia.org/wiki/Local_area_network)
A wired data network restricted to a limited geographic location such as a building.
- 📍typically a building wide network.
#### [MAN(Metropolitan Area Network)](https://en.wikipedia.org/wiki/Metropolitan_area_network)
A wired/wireless data network restricted to a city wide area
- 📍typically a city wide network.
## Q5. Differentiate between physical and logical topologies.
```sheet
{
	classes:{
		class1:{
		}
	}
}
---
| Physical Topologies | Logical Topologies |
| ------------------- | ------------------ |
|Depicts physical layout of network.|Depicts logistics of network concerned with transmission of data.|
|The layout can be modified based on needs.|There is no interference and manipulation involved here.|
|It can be arranged in star, ring, mesh and bus topologies.|It exists in bus and ring topologies.|
|This has major impact on cost, scalability and bandwidth capacity of network based on selection and availability of devices.|This has major impact on speed and delivery of data packets. It also handles flow control and ordered delivery of data packets.|
|It is actual route concerned with transmission.|It is a high level representation of data flow.|
|Physical connection of the network.|Data path followed of the network.|
```
## Q6. List the different types of networks from surroundings as client-server network, distributed networks,peer-to-peer networks and cloud based networks.
```sheet
{
	classes:{
		class1:{
		}
	}
}
---
| Parameter | Client-Server | Distributed | Peer-to-Peer | Cloud Based |
|||||
```

## Q7. Discuss the concept of Network Topologies.
## Q8. Protocols and their usage e.g. TCP/IP, http, https, ftp.







